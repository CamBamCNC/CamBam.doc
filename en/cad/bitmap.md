---
title: Bitmap Objects
---
# ![icon](../img/icon/bitmap32.png) Bitmap Objects

Use <span class="cmd">Draw - Bitmap</span>, then select a drawing file such as a jpeg or bitmap.
CamBam will detect edges in the bitmap, allowing you to scan a drawing, or draw a shape in an external application like Photoshop, 
then load it into CamBam and insert a machining operation based from it.
Changes to the drawing will be picked up automatically by the machining operation.

The bitmap file is linked to the CamBam drawing, not saved within it, so transferring CamBam drawings containing bitmaps 
will also require the bitmap file to be copied.

To break the link to a bitmap file and include the vectors within the CamBam drawing, select the drawing object then use 
<span class="cmd">Convert To Polylines</span> or <span class="cmd">CTRL+P</span>.

## Showing Bitmaps or Vectors

The <property>Display Bitmaps</property> and <property>Display Vectors</property> properties can be used to control what is
displayed in the drawing.

<table>
<tr>
<td>
	<img src="../images/cad/bitmap_bmp_only.png" alt="Show Bitmap Only"><br>
	showing bitmap only
</td>
<td>
	<img src="../images/cad/bitmap_both.png" alt="Show Bitmap and Vectors"><br>
	showing bitmap and vectors
</td>
</tr>
<tr>
<td>
	<img src="../images/cad/bitmap_edge_only.png" alt="Show Vectors Only"><br>
	showing vectors only (invert=false)
</td>
<td>
	<img src="../images/cad/bitmap_edge_invert.png" alt="Show Vectors Only Inverted"><br>
	showing vectors only (invert=true)
</td>
</tr>
</table>

## Smoothing

CamBam will automatically vectorize the bitmap using a quick and simple edge detect.
You can change the vectorization method in the bitmap properties.  Selecting <property>Smoothing</property> = <value>True</value> will generate a spline smoothed outline.  This can take a few seconds for the spline to calculate.

*Smoothing Pixels*{:.prop} can be used to control the spline fit. <property>Smoothing Pixels</property> = <value>2</value> usually provides good results.  Going lower than 1 can take a long time and will end up with a spline trying to follow each pixel.

> Small smoothing pixel values (around 1), can take a very long time to calculate (minutes!).  
<br>
We are working to make this faster and run in a background process so as not to lock up the UI,
but for now smoothing pixel settings of 2 or more are recommended.
{:.warning}

<table>
<tr>
<td>
	<img src="../images/cad/bitmap_nosmooth.png" alt="No Smoothing"><br>
	no smoothing
</td>
<td>
	<img src="../images/cad/bitmap_smooth_1.png" alt="Smooth Pixels = 1"><br>
	smooth pixels = 1
</td>
</tr>
<tr>
<td>
	<img src="../images/cad/bitmap_smooth_2.png" alt="Smooth Pixels = 2"><br>
	smooth pixels = 2
</td>
<td>
	<img src="../images/cad/bitmap_smooth_5.png" alt="Smooth Pixels = 5"><br>
	smooth pixels = 5
</td>
</tr>
</table>

## Properties

<table class="prop" cellspacing="0" width="100%">
<tr>
<td>
	<property>Display Bitmap</property>
</td>
<td>
	<p><value>True</value> | <value>False</value>.</p>
	<p>Controls whether the bitmap image is displayed.</p>
</td>
</tr>
<tr>
<td>
	<property>Display Vectors</property>
</td>
<td>
	<p><value>True</value> | <value>False</value>.</p>
	<p>Controls whether the detected edge vectors are displayed.</p>
</td>
</tr>
<tr>
<td>
	<property>Height</property>
</td>
<td>
	<p>The height of the image in drawing units.</p>
	<p>The pixel height and vertical resolution stored in the bitmap are used to calculate the default height value.</p>
</td>
</tr>
<tr>
<td>
	<property>Width</property>
</td>
<td>
	<p>The width of the image in drawing units.</p>
	<p>The pixel width and horizontal resolution stored in the bitmap are used to calculate the default width value.</p>
</td>
</tr>
<tr>
<td>
	<property>Invert</property>
</td>
<td>
	<p>If <value>True</value> the edges of lighter pixels are detected. If the image contains dark lines on
	a light background, the edge detect will include the bitmap outline.</p>
	<p>If <value>False</value> the edges of darker pixels are detected. If the image contains dark lines on
	a light background, only the edges of the lines are included.</p>
</td>
</tr>
<tr>
<td>
	<property>Position</property>
</td>
<td>
	<p>This is the X,Y,Z drawing location of the lower left corner of the bitmap.</p>
</td>
</tr>
<tr>
<td>
	<property>Smooth Pixels</property>
</td>
<td>
	<p>If <property>Smoothing</property> = <value>True</value>, <property>Smooth Pixels</property> controls the maximum allowed error
	between a spline curve and the raw outline.  The error is measured in number of pixels.</p>
	<p>Smaller values will produce splines that closely match the outline, but may take longer 
	to calculate, produces larger splines and possibly result in unwanted detail such as jagged pixel outlines.</p>
	<p>Larger values will produce faster, simpler, smoother curves but with less accuracy.</p>
</td>
</tr>
<tr>
<td>
	<property>Smoothing</property>
</td>
<td>
	<p><value>True</value> | <value>False</value>.</p>
	<p>If <value>True</value>, a spline will be used to fit the raw pixel edges.</p>
	<p>If <value>False</value>, only the raw pixel edges are used.</p>
	<p>Raw pixel edges will be faster to calculate, but result in 'jagged' pixel edges.</p>
</td>
</tr>
<tr>
<td>
	<property>Source File</property>
</td>
<td>
	<p>The path to the source bitmap file.</p>
	<p>The following bitmap formats are supported:<br>
	BMP, GIF, EXIF, JPG, PNG and TIFF</p>
    <p>The filename will be relative to the current CamBam drawing path.  Saving the CamBam
	drawing first, before inserting bitmaps is recommend so the relative rather than full paths will
	be detected.  Relative paths make is easier to copy the .cb file and bitmap files together
	without needing to change paths within the drawing.</p>
</td>
</tr>
<tr>
<td>
	<property>Threshold</property>
</td>
<td>
	<p>The brightness threshold used to determine if a pixel is set when vectorizing.</p>
	<p>A value between 0 and 1, where 0 is black and 1 is white.</p>
	<p>Note that color images use monochrome edge detection so edges between different colors
	with similar brightnesses may not be detected.</p>
</td>
</tr>
<!-- $include file="../en/_prop/mop.Tag.html" -->
<tr>
<td>
	<property>Tag</property>
</td>
<td class="desc">
    <p>A general purpose, multi-line text field that can be used to store notes or parameter data.</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/Transform.html" -->
<tr>
<td>
	<property>Transform</property>
</td>
<td>
    <p>A 4 x 4 matrix of numbers used for general transformations of the drawing objects.</p>
    <p>The transform matrix can be used for rotations, translations and scaling about all 3 axis.</p>
    <p><value>Identity</value> indicates no transformations will be applied to the object.</p>
</td>
</tr>
<!-- $include.end -->
</table>


