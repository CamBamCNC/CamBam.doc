---
title: Tutorial - 3D Profile
---
# Tutorial: 3D Profile

This tutorial gives an introduction to the [3D Profile](../cam/3d.md){:.name} operation, and covers:

- Loading 3D models, sizing and positioning.
- Front face waterline roughing.
- Front face scanline finishing.


[![Half way through finishing pass](../images/tutorials/3DProfile/apple-s.jpg){:.icenter}](../images/tutorials/3DProfile/apple.jpg)

## Loading 3D models, sizing and positioning

{::options auto_ids="false" /}
### Loading

CamBam can read .3DS file, .STL and .RAW 3D mesh files. These can be loaded using the <span class="cmd">File - Open</span> menu option
or by dragging files onto the CamBam drawing window.

If an imported object is not immediately visible, it may be because its default dimensions are very small compared to the
currently display stock object.  If this is the case, temporarily hide the stock using <span class="cmd">View - Show stock</span>, then use
<span class="cmd">View - Zoom to fit</span>.

To machine successfully, the 3D model needs to be aligned within the intended machining area.  This may involve combinations
of the following transformations.

### Sizing

To make the model a fixed size, the <span class="cmd">Transform - Resize</span> command can be used.  
This will open the Resize window which will show the existing object dimensions and allow them to be resized to a specific
dimension, or by a scaling percent.

![Resize window](../images/tutorial.3d-resize.png){:.icenter}

### Rotating

The model should be rotated so that it is facing toward the screen (i.e. in the positive Z direction).

<span class="cmd">Transform - Rotate</span> can be used to rotate selected objects.  First select a rotation point, and then move the mouse around this point
to select a rotation angle.  Press the X, Y or Z keys to change the current axis of rotation.  If snap to grid is enabled, the rotation angle will
snap to multiples of 30 and 45 degrees.

Selected objects can also be rotated by using the [transformation property editor](../cad/transformations.md){:.name}.  
Rotations follow a right hand rule, so to visualise this, point your right thumb in the direction of the positive axis of rotation.  
A positive rotation is then in the direction that your fingers curl around the axis.

Another alternative is to use free-hand rotation.  This is done by selecting objects,
then holding the <span class="key">SHIFT</span> key while using the view rotation key+mouse combination (ie <span class="key">ALT</span> + left mouse drag
or Center mouse + Left mouse drag, depending on your configuration settings).

### Positioning

<span class="cmd">Transform - Align</span> can be used to position selected objects.  This will display a form with 3 columns, one
for each axis.  Select the point of the selected axis to align, or none to leave the current axis position intact.
Enter the drawing coordinate underneath which will be the new location of the alignment point, then press <span class="cmd">Apply</span>.

For example, to position an object so that it's lower left corner is at the drawing origin and the highest Z point is
just below the stock surface (if using Z=0), use the following alignment values:

~~~
X - Left, Value = 0  
Y - Bottom, Value = 0  
Z - Upper, Value = -0.5
~~~

![Resize window](../images/tutorial.3d-align.png){:.icenter}

It may be more convenient to reference the machine's Z=0 to the work table, then use a <property>Stock Surface</property> value that is the Z height of the stock.
This works well when the stock used has an uneven surface or is difficult to reference a tool to (particularly after a roughing pass).
This can also simplify back face machining.  If using this method, use the following Z alignment options:

~~~
Z - Center, Value = 0
~~~

{:align="center"}
[![3D Model loaded and positioned](../images/tutorial.3d-1-t.png){:.icenter}](../images/tutorial.3d-1.png)  
This image shows a 3D model loaded, sized and positioned

## Front face waterline roughing

Waterline roughing is an efficient way to clear the bulk of the stock around the 3D model.

### Create a 3D Profile operation

Select the 3D surfaces to machine, then insert a 3D Profile machining operation (<span class="cmd">Machining - 3D Profile</span>)
or select the ![3D Surface](../images/basics/cam_3dsurface.png) icon from the toolbar.

If a <span class="name">Stock</span> object has been correctly defined, some properties may be automatically calculated, such as <property>Stock Surface</property> and <property>Target Depth</property>,
as the default CAM Style has these values set as <value>Auto</value> values.

### Basic Properties

Note: dimensions shown here are metric.

<table border="0" width="100%" cellpadding="4px">
    <thead>
    <tr>
        <th align="left" width="25%">Property</th>
        <th align="left" width="20%">Value</th>
        <th align="left" width="55%">Notes</th>
    </tr>
    </thead>
    <tbody>
	<tr>
	<td>
		<property>Profile 3D Method</property>
	</td>
	<td>
		<value>Waterline Rough</value>
	</td>
	</tr>
	<tr>
	<td>
		<property>Depth Increment</property>
	</td>
	<td>
		3
	</td>
	<td>
		The maximum Z depth per cut of each machining layer.
	</td>
	</tr>
    <tr>
	<td>
		<property>Lead In Move</property>
	</td>
	<td>
		<value>Spiral</value><br />
		3 degree<br />
	</td>
    <td>
		As well as making life easier for the cutter, this also gives the Fast Plunge Height behaviour a reference point which
           helps avoid slow plunges.
	</td>
	</tr>
    <tr>
	<td>
		<property>Roughing Clearance</property>
	</td>
	<td>
		1
	</td>
	<td>
		Leave a small amount of stock to be cleared away in the finishing pass, to avoid waterline machining marks being visible.
	</td>
	</tr>
	<tr>
	<td>
		<property>Stock Surface</property>
	</td>
	<td>
		0
	</td>
	<td>
		In this example, Z=0 is referenced to the stock surface.</td>
	</tr>
	<tr>
	<td>
		<property>Target Depth</property>
	</td>
	<td>
		-50
	</td>
	<td>
		If the model is 100 units tall, this will machine the top half of the model.
	</td>
	</tr>
	<tr>
	<td>
		<property>Tool Diameter</property>
	</td>
	<td>
		6
	</td>
	<td>
		To increase roughing speed, use a larger tool.
	</td>
	</tr>
    <tr>
	<td>
		<property>Tool Profile</property>
	</td>
	<td>
		<value>End Mill</value>
	</td>
    <td>
		<p>Scanline (horizontal /vertical) and waterline finishing methods will adjust tool paths for bull/ball nose cutters.</p>
		<p>For waterline roughing operations, the tool profile does not affect the tool path.</p>
	</td>
	</tr>
    </tbody>
</table>

### Advanced Properties

<table border="0" width="100%" cellpadding="4px">
    <thead>
    <tr>
        <th align="left" width="25%">Property</th>
        <th align="left" width="20%">Value</th>
        <th align="left" width="55%">Notes</th>
    </tr>
    </thead>
    <tbody>
	<tr>
	<td>
		<property>StepOver</property>
	</td>
	<td>
		0.5
	</td>
    <td>
		Distance between toolpaths expressed as a fraction (0-1) of cutter diameter.
	</td>
	</tr>
	<tr>
	<td>
		<property>Plane Slice Only</property>
	</td>
	<td>
		False
	</td>
    <td>
		CamBam's waterline routines have been designed to work best with natural / curved shapes.  Engineering shapes with perpendicular sides
        can potentially cause problems.  If problems are encountered, setting <property>Plane Slice Only</property> to <value>True</value> can help but will only work
        with shapes that do not have any overhangs.
	</td>
	</tr>
    </tbody>
</table>

### General Settings

There are some properties under <span class="name">Machining</span> that are useful when working with 3D files.

<table border="0" width="100%" cellpadding="5px" >
    <thead>
    <tr>
        <th align="left" width="27%">Property</th>
        <th align="left" width="18%">Value</th>
        <th align="left" width="55%">Notes</th>
    </tr>
    </thead>
	<tbody>
	<tr>
	<td>
		<property>Rebuild Toolpath Before Post</property>
	</td>
	<td>
		<value>Prompt</value>
	</td>
	<td>
		3D Toolpath generation can take many minutes.  This option will prompt whether to regenerate toolpaths
		before creating gcode.  If 'No' is specified, the post processor will use the last generated toolpaths.
	</td>
    </tr>
    <tr>
	<td>
		<property>Fast Plunge Height</property>
	</td>
	<td>
		0.2
	</td>
    <td>
		<p>A small value here allows the post processor to rapid down to the fast plunge height distance
        above the last cut stock depth and can speed machining times considerably.</p>
		<blockquote class="warning"><span class="warning">Warning!</span> Care should be taken with this setting, especially for
        machines with flex or backlash.  Setting <property>Fast Plunge Height</property> to a little larger than <property>Depth Increment</property> should be safest.</blockquote>.
	</td>
	</tr>
	<tr>
	<td>
		<property>Toolpath Visibility</property>
	</td>
	<td>
		<value>Selected Only</value>
	</td>
    <td>
        <p>
		Having front roughing, finishing and back face toolpaths visible is very confusing.  This option will only
        show the toolpaths for the machining operation currently selected in the drawing tree.<br>
        <b>NOTE:</b> From version 0.9.8 this option is now set in the file's properties (the first object in the drawing tree).
        </p>
	</td>
	</tr>
    </tbody>
</table>

{:align="center"}
[![Waterline roughing pass](../images/tutorial.3d-2-t.png){:.icenter}](../images/tutorial.3d-2.png)  
This image shows the waterline roughing toolpaths.

{:align="center"}
[![Simulation of the roughing pass in CutViewer Mill](../images/tutorial.3d-3-t.png){:.icenter}](../images/tutorial.3d-3.png)  
Simulation of the roughing pass in CutViewer Mill

## Scanline finishing

Once the bulk of the material has been cleared by roughing, a scanline finishing pass can be applied.
Select the 3D surface and insert a second <span class="name">3D Profile</span> operation.

This time, set the <property>Profile 3D Method</property> to <value>Vertical</value> or <value>Horizontal</value>.
To attain a finer finish, with less toolmarks, a horizontal pass followed by a vertical finishing pass can be used.

### Basic Properties

Note: dimensions shown here are metric.

<table border="0" width="100%" cellpadding="5px">
    <thead>
    <tr>
        <th align="left" width="25%">Property</th>
        <th align="left" width="20%">Value</th>
        <th align="left" width="55%">Notes</th>
    </tr>
    </thead>
    <tbody>
    <tr>
	<td>
		<property>Profile 3D Method</property>
	</td>
	<td>
		<value>Horizontal</value><br />or <value>Vertical</value>
	</td>
	</tr>
    <tr>
	<td>
		<property>Depth Increment</property>
	</td>
	<td>
		0
	</td>
	<td>
		Depth increment should be 0 to do a single finishing pass.
	</td>
	</tr>
    <tr>
	<td>
		<property>Target Depth</property>
	</td>
	<td>
		-50
	</td>
	<td>
		Use the same target depth as the roughing pass.
	</td>
	</tr>
    <tr>
	<td>
		<property>Roughing Clearance</property>
	</td>
	<td>
		0
	</td>
    <td>
		No roughing clearance - will clear off clearance stock from the roughing pass.
	</td>
	</tr>
	<tr>
	<td>
		<property>StepOver</property>
	</td>
	<td>
		0.1
	</td>
	<td>Distance between toolpaths expressed as a fraction (0-1) of the cutter diameter.
        Smaller stepovers will give a nicer finish but take longer to machine.
	</td>
	</tr>
	<tr>
	<td>
		<property>Resolution</property>
	</td>
	<td>
		0.1
	</td>
    <td>
		This is the distance along toolpaths expressed as a fraction (0-1) of the cutter diameter, at which the height of 
        the model is tested.<br />
        0.1 should be adequate, but a smaller value could be used if inaccuracies occur (especially around
        small features or perpendicular edges).
	</td>
	</tr>
	<tr>
	<td>
		<property>Tool Diameter</property>
	</td>
	<td>
		3
	</td>
	<td>
		A smaller tool will result in more detail but takes longer to machine.
	</td>
	</tr>
	<tr>
	<td>
		<property>Tool Profile</property>
    </td>
	<td>
		<value>Bull Nose</value>
	</td>
    <td>
        <p>The horizontal and vertical scanline, as well as waterline finish, methods will adjust tool paths for ball nose cutters.</p>
    </td>
	</tr>
	</tbody>
</table>

{:align="center"}
[![Horizontal finish pass](../images/tutorial.3d-4-t.png){:.icenter}](../images/tutorial.3d-4.png)  
This image shows the scanline finishing toolpaths

{:align="center"}
[![The finishing pass simulated in CutViewer Mill](../images/tutorial.3d-5-t.png){:.icenter}](../images/tutorial.3d-5.png)  
The finishing pass simulated in CutViewer Mill

## Adjusting the machining boundary

The 3D profile operation will machine the minimum area around the objects.  To change this behaviour, a number of boundary options 
can be defined.

<table border="0" width="100%" cellpadding="4px">
    <thead>
    <tr>
        <th align="left" width="25%">Property</th>
        <th align="left" width="20%">Value</th>
        <th align="left" width="55%">Notes</th>
    </tr>
    </thead>
	<tbody>
	<tr>
	<td>
		<property>Boundary Margin</property>
	</td>
	<td>
		2
	</td>
    <td>
		Adds a small extra margin around the shape outline boundary.
	</td>
	</tr>
	<tr>
	<td>
		<property>Boundary Taper</property>
	</td>
	<td>
		3
	</td>
    <td>
		Tapers the boundary edge which helps give cutter clearance at lower depths.
	</td>
	</tr>
    </tbody>
</table>

## Back face machining

Please refer to the [3D Profile - back face tutorial](3d-profile-back-face.md){:.name}.
