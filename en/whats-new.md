---
title: What's new
---
## New for V1.0

> <b>CamBam version 1.0</b> is a new major version, which means that it will be installed
in a new program folder and it will have it's own System Folder. This enables CamBam version 1.0 to be installed alongside version 0.9.8.  
To copy settings such as styles, post processors, tool libraries and templates from version 0.9.8, copy relevant files from the old system folder location:
<br>    
`\ProgramData\CamBam plus 0.9.8\`  
to  
`\ProgramData\CamBam plus 1.0.0\`  
<br>
The <span class="cmd">Tools - Browse system folder</span> menu item can be used to find the system folder location.  
<span class="warning">Required .NET framework change</span> CamBam is now using the .NET framework version 4 as framework version 2 becomes end of life.  
.NET Framework version 4 is usually installed by default on most newer PCs.  For older computers this can be downloaded from here:  
[www.microsoft.com/en-gb/download/details.aspx?id=17718](https://www.microsoft.com/en-gb/download/details.aspx?id=17718)
{:.warning}

### New Script drawing

A <span class="name">script object</span> works like other drawing objects, but the geometry is created from a script (python only for now).

Machining operations can be based on script objects in the same way as static drawing objects.  Any changes made by the script will be automatically picked up by associated machining operations.

The script object includes scripts from the <property>Script</property> property of the parent layer, as well as the <property>Script</property> property at the top level of the drawing file.
This allows functions, classes or variables to be defined once in the drawing, then referenced in any number of drawing objects.
Scripts can also be included from external sources (the current drawing folder as well as the CamBam system folder\scripts folder are searched).

Scripts can be useful for generating parametric shapes (such as a gear, or tabbed box), creating copies of, or manipulating 
other drawing objects.  Script objects can also be used to run general scripts such as an animation.

Script objects will usually run when required to update geometry, but can be forced to run by selecting them in the drawing tree and pressing 'F5'.

Use <span class="cmd">Edit - Explode</span> to turn script objects into their component static drawing objects, which can be used with previous CamBam versions or further manipulated using CamBam CAD operations.

The samples folder contains a drawing <span class="name">C-BEAM-Belt-Drive.cb</span> which uses included library scripts (located in the CamBam system\scripts folder) to draw timing pulleys, stepper motor outlines and bearings.  The goal is to create a library of common parts that can be shared among many drawings and allow collaboration.  Any fixes or enhancements to the library scripts will be automatically picked up by drawings that reference them.

Refer to the following section for more detailed information.
[Script Drawing Objects...](cad/script-object.md){:.name}

### New Bitmap drawing object

Use <span class="cmd">Draw - Bitmap</span>, then select a drawing file such as a jpeg or bitmap.
CamBam will detect edges in the bitmap, allowing you to scan a drawing, or draw a shape in an external application like photoshop, then load it into CamBam and insert a machining operation based from it.  Changes to the drawing will be picked up automatically by the machining operation.

CamBam will automatically vectorize the bitmap using a quick and simple edge detect.
It is also possible to smooth the edge outlines using splines.

<table>
<tr>
<td>
	<img src="images/cad/bitmap_bmp_only.png" title="Show Bitmap Only"><br>
	showing bitmap only
</td>
<td>
	<img src="images/cad/bitmap_edge_only.png" title="Show Vectors Only"><br>
	showing vectors only
</td>
</tr>
</table>

Refer to the following section for more detailed information.
[Bitmap Drawing Objects...](cad/bitmap.md){:.name}

### Printing support added

Accessed using <span class="cmd">File - Print</span>.

CamBam mouse zoom, pan and rotate functionality work in the print preview window so the area of interest can be
positioned within the page.

A right click menu is available to set the print scale and other options such as page orientation.

### Region triangulation.

Surfaces can now be created from shapes such as regions and polylines.  This, combined with the <span class="cmd">Extrude</span> operation
allows the creation of 'solid' meshes from CamBam shapes.

The new <span class="name">klannimate.cb</span> sample demonstrates solid extrusions as well as scripting objects to generate shapes and provide animations.

### Spline and other shape drawing and editing.

Splines can now be drawn using <span class="cmd">Draw - Spline</span> or the new spline toolbar icon.

Double clicking a spline object allows the control points to be dragged to change the spline shape.

Drawing edit modes have also been added for <span class="name">Circles</span>, <span class="name">Arcs</span> and <span class="name">Lines</span>.
Double clicking any of these objects enters edit mode.

### CAM Part - Export - Toolpaths to DXF

A new context menu option for Part objects: <span class="cmd">Export - Toolpaths to DXF</span>.

This will export the selected part's toolpaths to a DXF file as polylines.  This operation will use
settings from the current post processor for options such as arc handling (default, convert to lines etc.)

This feature can be used to generate drawings for many commercial laser cutting machines such as those
based on LaserDraw.  These lasers are only able to import vector files such as DXF, PDF etc.

### 4th axis wrapping support

This is provided using some extra post processor settings.  There is a new group <span class="name">Rotary</span>, with just three properties : <span class="name">Axis of Spin</span>, <span class="name">Rotary Axis</span> and <span class="name">Rotary Wrap</span>.
If <property>Rotary Wrap</property> is set <value>True</value>, the post processor will convert all toolpaths to lines only, then wrap all the toolpaths selected around the rotary axis.

The radius of rotation is taken from each machining operations <property>Stock Surface</property> property.
It is up to the user to make sure the toolpath is within a width of <b>2 x PI x stock surface</b>.

> **Note:** The post-processor should also be modified so as not to output the axis registers for the non rotational axis.
For example, if rotating about the Y axis, the X{$x} move parameters should not be output.
{:.note}

### Three point Edit - Move

The <span class="cmd">Edit - Move</span> command now supports up to 3 source and destination points.

Hold down the <span class="cmd">CTRL</span> key and click to select up to 3 source points.

If only one point is selected, the move operation works as before by selecting the target destination point.

If two source points are selected, the first destination point will be the new location for the first point.  The second destination point will
form a line on which the moved second point lies.  This in effect allows a move and a rotation in the XY plane in one operation.

If three source points are selected, the first destination point will be the new location for the first point.  The second destination point will
form a line on which the moved second point lies. The third destination point will form a plane on which the moved third point lies. This allows a shape to
be moved in three dimensions.  For example a 3D mesh could be aligned to a mounting plate by selecting three matching reference points at the center
of three bolt holes.

The selected points are color coded: Red, Green, Blue to identify the current selection point.

### Other changes and new features:

- <span class="cmd">Draw - Surface - Extrude</span> has been modified to extrude positive heights along the positive Z axis.
Version 0.9.8 would extrude 'down' along the negative Z axis.
- API methods to manage plugins have been added to help with plugin registration and scripting.  
An example python script that references a plugin...  
```python
import clr
p = CamBamUI.MainUI.FindPlugin("MyPlugin")
clr.AddReference(p.Assembly)
from MyPlugin import *
```
- New configuration option: <property>DXF Options - Export 3D Polylines</property>.  Setting <value>True</value>, forces 2D polylines to be
output to DXFs as 3D i.e. with Z values at each point.  This was added for some Busellato controllers that did not
recognize Z offsets when importing DXF toolpaths.  <b>Warning!</b> setting true seems to cause problems for LaserDraw.
- The selection rectangle has been rewritten to avoid problems with screen rescaling.

### Revision rc-3 changes - 2019-10-18

- Toolpath generation speed improvements (~20 to 30% in some cases)
- Issue [36] - Added support for Gerber G74/G75 and Excellon G85 commands. <a href="http://www.cambam.co.uk/forum/index.php?topic=8133.0">link</a>
- Bugfix [37] - Holding tab normals on cusp point problems (manual normals not restored and autocalc normals sometimes incorrect.)
- Bugfix [38] - Error with polylines with large radius arcs. <a href="http://www.cambam.co.uk/forum/index.php?topic=8231.0">link</a>
- Issue [39] - Use machining/part tool diameter when machining/part tool number set and style tool is set to Auto.
- Issue [40] - Added new config option **Selected Entity Focus**.  
If set **False**, selecting a drawing object will not automatically expand its parent layer.<a href="http://www.cambam.co.uk/forum/index.php?topic=8214.msg65492#msg65492">link</a>
- Added CTRL+TAB hotkey combintation to toggle focus of selected drawing objects in drawing tree.
- Added CTRL+L hotkey to open the align window.
- Bugfix [41] (mono) - Incorrect text in combo boxes such as Transform editor and Transform Resize form bugfix. <a href="http://www.cambam.co.uk/forum/index.php?topic=8214.msg65448#msg65448">link</a>
- Issue [42] - Tool Library - Rename all tools improvements.  Added {$partcode} macro. <a href="http://www.cambam.co.uk/forum/index.php?topic=8253.0">link</a>
- Bugfix - holding tabs after spiral drilling operation display bug.
- Added new config option - **File Open Worker Thread**.  Defaults to **True** on Windows and **False** on Mono.  
This should prevent File Open hangs on some Ubuntu systems.

### Revision rc-2 changes - 2019-10-02

- Issue [08] - More machining outfile improvements. <a href="http://www.cambam.co.uk/forum/index.php?topic=8202.0">link</a>  
> - Prompts to create output folder if needed.
> - New from template no longer clears Machining.OutFile, so default output path can be set in template.
> - New Gcode Output - **Out File Prompt** config setting
>     - **If Exists** - prompts if output gcode file already exists
>     - **Always** - always prompts for output filename
>     - **Never** - automatically overwrites existing output
- Mono Only: New Display - **Menu Font Family** and **Menu Font Size** config options.
- Issue [34] (mono): Fixed some icon transparancy issues that appeared with libmono 6.4
- Mono Only: Look for captialized file extensions in File Open filter.
- Bugfix: failed to print when polylines with single point. <a href="http://www.cambam.co.uk/forum/index.php?topic=8203.0">link</a>  

### Revision rc-1 changes - 2019-09-11

- Bugfix [04] - 3D Profile + Mold + Scanline + Roughing + Ballnose added extra tool radius roughing clearance to depth <a href="http://www.cambam.co.uk/forum/index.php?topic=5386.msg59938#msg59938">link</a>
> **Warning:** This fix will result in extra, uncleared stock when using  Mold + Scanline + Roughing + Ballnose.  
> Please ensure any following machining
passes will allow for this.
{:.warning style="width:45%"}

- Bugfix [01] - Bitmap vectors now refresh when size property changes.
- Bugfix [05] - Linux/Mono layer problems when Layer.Locked set True.
- Bugfix [08] - Default GCode out folder improvements. <a href="http://www.cambam.co.uk/forum/index.php?topic=4325.0">link#1</a> <a href="http://www.cambam.co.uk/forum/index.php?topic=7470.0">link#2</a>  
> When prompted for a gcode output file, the default folder is now determined by looking for (in order of preference):
>1. Folder of Part.OutFile (for when producing gcode for a single part or machining operation)
>2. Folder of Machining.OutFile
>3. Folder of the saved CamBam drawing file.
>4. The last browsed folder (if current drawing is not saved).
>  
> Note, the default folder must already exist, otherwise the next folder option will be used.  
- [10] STEP File mesh resolution setting  
Added new System Configuration setting : STEP Resolution (default 0.05) (defined in millimetres). This resolution is used when converting surfaces to triangle meshes.
- Bugfix [12] - Empty Text object causing errors. <a href="http://www.cambam.co.uk/forum/index.php?topic=7492.msg60237">link</a>
- [13] Printing improvements  
White now prints as black for monochrome and black/white modes.  
For colour mode, will invert colour if it matches the background colour.  
Note, this now also affects the display view, which should help whenever
a layer colour matches the background.  
Also added new 'Line Width Scale' property to printer options (default = 1).  
A number > 1 will cause printed lines to be displayed with greater thickness.  
A number < 1 will print very fine lines, but these may not be visible (depending on printer).
- [14] Added new configuration option 'Default Layer Pen Width'
- Bugfix [16] - Linux/Mono - Fixed speed feeds calculator form. <a href="http://www.cambam.co.uk/forum/index.php?topic=7526.0">link</a>
- Bugfix [17] - Linux/Mono - fixed point parsing bug in mono causing file not to load. <a href="http://www.cambam.co.uk/forum/index.php?topic=7534.0">link</a>
- Bugfix [19] - bug fix when undo/redo a Layer delete. <a href="http://www.cambam.co.uk/forum/index.php?topic=7577">link</a>
- Bugfix [21] - 3D Profile boundary bug fix. <a href="http://www.cambam.co.uk/forum/index.php?topic=7611.msg61099#msg61099">link</a>
- Bugfix [22] - fixed 3D Profile, Backface milling support. <a href="http://www.cambam.co.uk/forum/index.php?topic=7659.msg61459">link</a>
- Bugfix [23] - Profile Cut Width Starting at line instead of at max width when "max crossover distance"=0. <a href="http://www.cambam.co.uk/forum/index.php?topic=7800.09">link</a>
- Bugfix [25] - Added warning if no source shapes are detected for a MOP. <a href="http://www.cambam.co.uk/forum/index.php?topic=7819.0">link</a>
- Bugfix [26] - Refresh property grid when nesting point list selection changes. <a href="http://www.cambam.co.uk/forum/index.php?topic=7813.0">link</a>
- Bugfix [28] - Wrong scaling used in some STEP files. <a href="http://www.cambam.co.uk/forum/index.php?topic=8018">link</a>
- [31] File Open and drag/drop of bitmaps into CamBam drawing now inserts a bitmap object into current drawing.  
Previously would try to open as an import NC File.
- Bugfix [32] - Linux/Mono - Selecting multiple layers in drawing tree causes property grid crash.

### Revision alpha 16 changes - 2018-06-26

- Changed folder writing permission check.
- Bugfix - DXF block rotation was not being applied. <a href="http://www.cambam.co.uk/forum/index.php?topic=3852.0">link</a>
- Bugfix - max stepover incorrect. <a href="http://www.cambam.co.uk/forum/index.php?topic=5755.0">link</a>
- Bugfix - for 3D profile molds with ball nose cutters / roughing. <a href="http://www.cambam.co.uk/forum/index.php?topic=5386.msg42731#msg42731">link</a>
- Bugfix - 3D profile glitches on vertical sections. <a href="http://www.cambam.co.uk/forum/index.php?topic=7211.msg58625#msg58625">link</a>
- Bugfix - ensure polyline offset direction consistent with source. <a href="http://www.cambam.co.uk/forum/index.php?topic=4271.0">link</a>
- Holding tab improvements when lead in/outs lie within tab. <a href="http://www.cambam.co.uk/forum/index.php?topic=3493.0">link</a>

### Revision alpha 15 changes - 2018-06-20

- File - export to STL support.  Merges all visible meshes into a single STL file.
- Added CAD3DUtils. GetPolylineSkeleton(), GetRegionSkeleton(), GetShapeListSkeleton() functions, used in scripting or plugins to generate a shape skeletons.
- Drilling operations now work from point lists in script drawing objects.
- Tools - Set file associations command to associate .cb files with current running CamBam version.
- Automatically run CamBam as administrator when elevated privileges needed such as when in evaluation mode.
- Set system folder permissions within CamBam (previously was done in installer).
- 3D surface machining tool path bug fixes.
- Draw - Surface - Fill, creates a mesh from selected drawing objects.
- Draw - Surface - Extrude Solid, similar to Extrude but also includes end caps to form a solid mesh.
- 3D machining operations and various other routines now work from script entities containing 3D surfaces.
- Arc - apply transformations bug fix.
- Various bitmap bug fixes - vectors, height and width now change when source bitmap property changes.
- New Extremas class available to help with extremas in scripting.

### Revision alpha 14 changes - 2017-11-16

- 3D Profile / surface slicing bug fixes.
- Removed the alternating X and Y half stepover offsets for scanline roughing.
- Paste Format and Undo bug fixes.
- Ensure depth increment is always positive when applied - fixes problem with negative DI and lead ins.
- Bug fix when unioning rectangles that had been moved.

### Revision alpha 13.4 changes - 2017-09-14

- Moved to new Wix based installer system.  Removed dependency on .NET framework 3.5 in installer.
- Permission changes to work better with Windows 10.

### Revision alpha 13 changes - 2017-02-19

- Initial STEP file support added.  Still work in progress.
- Some spline to polyline performance increases (sometime 2x or better).
- 3D splines are now also supported.
- Spline transformations handled better.
- VBScript hopefully will work again (but only for Windows).
- Added new Layer lock functionality.  CTRL+SPACE will toggle locked layers.
Locked layers will be displayed (if visible), but objects on them will not be selectable.
Locked objects still work with snap to points.
- DXF export now coverts objects to polylines where they cannot be transformed correctly (rectangle, rotated text, some arc and circle transformations).
Unrotated text will still export as a DXF text object, which loses font information (and some CAD will ignore), ...for these manual conversion to polylines is still needed.
- Layer - 'hide all but this' performance fix (was very slow previously).
- Bitmaps no longer snap to points when vectors hidden
- AlignUI now only allow one instance to be opened.
- Bug fixes to do with applying transformations and joining polylines with transforms.

### Revision alpha 12 changes - 2017-01-13

- New Edit - Move behaviour supports up to 3 source and destination points.
- Selection rectangle should work better with high resolution monitors.
- GDI display modes bug fixes and performance tweaks.
- Excessive DrillingMethod – cache conflict messages bug fix
- IronPython has been bumped to version 2.7.7.0 – if any script problems are reported,
try clearing out any old IronPython or Microsoft.Scripting dll versions lying around (such as in plugins folder).
- The bitmap vectorizer now treats transparent colour as white.
- Bitmaps are now displayed for GDI and OpenGL_Legacy display modes.
- Fixed clicking on 'hidden' holding tabs bug.
- 3D surface snap to vertex points added.
- Print preview buttons are now be translatable.
- The drawing view is now refreshed after print preview closes to avoid background colour bug.

