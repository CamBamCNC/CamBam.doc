---
title: Part Machining Object
---
# Part Machining Object

A Part is a way of grouping multiple, related machining operations into a single object.
A single drawing file can contain many different part objects.

![](../images/parts.png){:align="right" .iright}

Parts can be enabled or disabled individually.  As with layers and machining operations, pressing the space bar when the 
item is selected in the drawing tree, will toggle a part's enabled state.

To generate the toolpaths for all the machining operations in a part, right click the part in the drawing tree, then select
<span class="cmd">Generate toolpaths</span>.  Right click an individual machining operation to generate toolpaths for just that mop, and right click
the Machining folder (or press <span class="key">CTRL+T</span>) to generate toolpaths for all enabled operations in the drawing.

By default, generating gcode will write the output from all the enabled parts in the drawing.
To create gcode for just one part, right click the part in the drawing tree, then select <span class="cmd">Produce gcode</span>.

The file <span class="file">heart-shaped-box.cb</span>, in the CamBam samples folder illustrates a good use of different parts.
Here machining operations are separated into parts for front and back faces for the lid and base of a small wooden box.

Some of the Part properties such as <property>Stock</property> and <property>Tools</property> are repeated in the parent <span class="name">Machining</span> folder.  Usually it is best to define these properties at the Machining folder
level, so they need only be defined once per drawing.  If the Part properties are unspecified, the corresponding value will be used from
the machining object.  It may be useful to define the properties at the part level if they differ from the global Machining settings,
for example if a part uses a different stock definition.

## Properties
<table class="prop" cellspacing="0" width="100%">
<tr>
<td>
	<property>Enabled</property>
</td>
<td>
	If <property>Enabled</property> is <value>True</value>, the (enabled) machining operations in this part will have their toolpaths displayed and they will be included in the gcode output.
</td>
</tr>
<!-- $include file="../en/_prop/MachiningOrigin.html" -->
<tr>
<td>
	<property>Machining Origin</property>
</td>
<td>
<p>A drawing point that will be used as the machining origin (X=0,Y=0) point when gcode is created.</p>
<p markdown="1">The ellipsis button ![](../images/CONTENT.jpg) to the right of this property can be used to select a point in the drawing.</p>
<p>
An 'X' icon will be displayed on the drawing at the machining origin point.  This cross can be dragged to
a new location using the mouse.
</p>
<p><b>NOTE:</b> MachiningOrigin replaces the GCodeOrigin and GCodeOriginOffset properties of earlier releases.</p>
</td>
</tr>
<!-- $include.end -->

<tr>
<td>
	<property>Name</property>
</td>
<td>
	A descriptive name for the part.  This name will be used to generate a filename when creating gcode output from the part.
</td>
</tr>

<tr>
<td>
	<property>Nesting</property>
</td>
<td>
	<p>This composite property provides a method of generating an array or nest of parts.</p>
	<p><property>Nest Method:</property> Change this to <value>Grid</value> or <value>Iso Grid</value>, then set the <property>Rows</property> and <property>Columns</property> values
	to determine the number of copies of each part.  The <property>Spacing</property> value will control the distance between each copy.</p>
	<p>When the toolpaths are generated, an outline should be displayed to indicate the location of each copy.  The centre of
	each outline contains a triangular icon.  Clicking and dragging this icon will change the nesting pattern and will
	also change the nesting method to <value>Manual</value>.</p>
    <p>
	<property>Grid Order</property> Controls the direction of the grid layout.  For example
	<value>Right Up</value> will make copies to the right of the original, then move up to the next row.
    </p>
    <p>
	<property>Grid Alternate</property> If set to <value>True</value>, the grid will alternate the direction of each row or column (depending on <property>Grid Order</property>).
	If <value>False</value> then each row or column will proceed in the same order with a rapid back to the start of each.
    </p>
    <p>
	<property>Nest Method</property> = <value>Point List</value> The location of each nest copy is taken from a point list
	drawing object which is set in the <property>Point List ID</property> property. A new <span class="cmd">Nest to point list</span> Part context menu function has
	been added, in this way a list of nest points can effectively be copied from one part to another by sharing a common point list.
    </p>
    <p>
	<property>GCode Order</property> Controls how the nested machining operations are ordered in the gcode output.<br />
	<ul>
	<li><value>Auto</value> - All consecutive MOPs within the part with the same toolnumber will be posted then repeated for each nest copy, before
	moving to the next MOP (which would require a tool change).</li>
	<li><value>Nest Each MOP</value> - Each MOP is output at each nest location before moving to the next MOP.</li>
	<li><value>All MOPs Per Copy</value> - All the MOPs in the part are posted before moving to the next nest location.</li>
	</ul>
    </p>
    <p>
	Multiple copies of the part's toolpaths will be written to the gcode output.  This will increase the gcode file size, 
	but does avoid some of the issues encountered when using subroutines.
    </p>
<p markdown="1">
More detail is available on the [Nesting documentation page](nesting.md).
</p>
</td>
</tr>
<!-- $include file="../en/_prop/OutFile.html" -->
<tr>
<td>
	<property>Out File</property>
</td>
<td>
<p markdown="1">
This is the location of the destination gcode file.  Clicking the ![](../images/CONTENT.jpg) button to the right of
this property will open a file browser.
</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/Stock.html" -->
<tr>
<td>
	<a name="Stock"><property>Stock</property></a>
</td>
<td>
<p>
    The stock object is used to define the dimensions of a block of material from which the part will be cut.
</p>
<p>
    The properties of the stock object can be used to automatically determine some machining properties.
</p>
<ul>
<li>If a machining operation or style's <property>Stock Surface</property> property is set to <value>Auto</value>, the stock's stock surface value will be used.</li>
<li>If a machining operation or style's <property>Target Depth</property>  property is set to <value>Auto</value>, the stock's stock surface and Z size will be used
to determine the target depth, so a machining operation will by default machine all the way through the stock.</li>
</ul>
<p>Stock properties:</p>
<p>
    <property>Material</property>: Informational text that describes the stock material.<br>
    <property>Stock Offset</property>: X and Y offset of the lower left corner of the stock block. For example, a stock offset of -10,-20
    would position the stock 10 units to the left of the Y axis (X=0) and 20 units below the X axis (Y=0).<br>
    <property>Stock Surface</property>: The Z location of the top of the stock block.<br>
    <property>Stock Size</property>: The X, Y and Z dimensions of the stock block.<br>
    <property>Color</property>: Color to use when displaying this stock object.
</p>
<p>
    Stock is undefined if the X,Y and Z sizes are all zero. Stock can be defined at the part or machining level.
    Stock defined at the part level will override and machining level stock definitions and will be used for all operations within the part.
</p>
<p>
    The stock object dimensions can also be passed to simulators such as CutViewer when post processors with appropriate stock macros are included, such as the
    <i>Mach3-CutViewer</i> post processor.
</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/Style.html" -->
<tr>
<td>
	<property>Style</property>
</td>
<td>
<p markdown="1">Select a default [CAM Style](cam-style.md){:.name} for this part.</p>
<p>All machining operations in the part will use this style unless set otherwise in the machining operation's Style property.</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/StyleLibrary.html" -->
<tr>
<td>
	<property>Style Library</property>
</td>
<td>
    <p>This property is used to locate the style definitions used in the Part or machining operations.</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.Tag.html" -->
<tr>
<td>
	<property>Tag</property>
</td>
<td class="desc">
    <p>A general purpose, multi-line text field that can be used to store notes or parameter data.</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.ToolDiameter.html" -->
<tr>
<td>
	<property>Tool Diameter</property>
</td>
<td>
    <p>This is the diameter of the current tool in drawing units.</p>
    <p>
        If the tool diameter is 0, the diameter from the tool information stored in the tool library
        for the given tool number will be used.
    </p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/ToolLibrary.html" -->
<tr>
<td>
	<property>Tool Library</property>
</td>
<td>
    <p>
        If left blank, the default tool library will be used (Default-{$Units}), otherwise the specified library will be used when
        looking up tool numbers.
    </p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.ToolNumber.html" -->
<tr>
<td>
	<property>Tool Number</property>
</td>
<td class="desc">
    <p>The ToolNumber is used to identify the current tool.</p>
    <p>
        If ToolNumber changes between successive machine ops a toolchange instruction is created in gcode.
        ToolNumber=0 is a special case which will not issue a toolchange.
    </p>
    <p>The tool number is also used to look up tool information in the current tool library.  The tool library is specified
	in the containing Part, or if this is not present in the Machining folder level.  If no tool library is defined the
	Default-(units) tool library is assumed.</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.ToolProfile.html" -->
<tr>
<td>
	<property>Tool Profile</property>
</td>
<td>
    <p>The shape of the cutter</p>
    <p>
        If the tool profile is Unspecified, the profile from the tool information stored in the tool library
        for the given tool number will be used.
    </p>
    <p>
        <value>EndMill</value> | <value>BullNose</value> | <value>BallNose</value> | <value>Vcutter</value> | <value>Drill</value> | <value>Lathe</value>
    </p>
</td>
</tr>
<!-- $include.end -->
</table>
