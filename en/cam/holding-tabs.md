---
title: Holding Tabs
---
# Holding Tabs

Holding tabs or bridges are used to hold material in place when cutting through the entire thickness of stock.
They are formed by breaking or bending the toolpaths at the lower depths of the cut, to leave areas of stock intact.

The [Profile machining operation](profile.md){:.name} contains a <property>Holding Tabs</property> composite property.
Click on the + sign to the left of this to expand the property and modify the sub properties.  

[![](../images/holding-tabs1-t.png){:.icenter}](../images/holding-tabs1.png)

The quickest way to enable holding tabs is to select the profile machining operation in the drawing tree.
Then right click the drawing window to open the drawing context menu.
At the bottom of the context menu, a <span class="cmd">Holding Tabs</span> sub menu is displayed. From here, select <span class="cmd">Autocalc</span>.
This is similar to setting the holding tab <property>Tab Method</property> property to <value>Automatic</value> and rebuilding the toolpaths.

![](../images/tabX.png){:align="right" width="178" height="300" .iright}

The holding tabs will be displayed as a series of rectangles spaced around the source drawing shapes.
If the automatically generated holding tabs are in inconvenient positions, they can be quickly moved by clicking
and dragging them to an alternate position.  This will also change the <property>Tab Method</property> to <value>Manual</value>.

If a tab is displayed with a red cross marker through it, this indicates a holding tab that cannot be applied to
any toolpaths.  This is often caused when holding tabs are positioned on the corners of shapes.  In these cases,
manually adjusting the tab position will resolve the problem.  The X marker will not be cleared until the toolpaths
are regenerated.

When a profile machining operation is selected, the drawing context menu can also be used to <span class="cmd">Add</span> and <span class="cmd">Remove</span>
holding tabs.  When removing tabs, right click the mouse within the rectangle of the holding tab to
remove.  Similarly, when adding tabs, first right click on the source shape where the new shape should
be located, then select <span class="cmd">Add Tab</span> from the resulting Holding Tabs context menu.

The number and spacing of the automatically generated holding tabs is controlled by the <property>Tab Distance</property>
parameter as well as the <property>Minimum</property> and <property>Maximum Tabs</property> properties.  For example, if the perimeter of
an object is 160mm and a tab distance of 30mm is used, the nearest whole number to 160/30 ie 5 holding tabs
will be considered.  If however this number is greater than the <property>Maximum Tabs</property> property, the maximum
tabs number will be used instead.  Similarly, if the automatic number of tabs is less than the <property>Minimum Tabs</property> property,
the minimum number will be inserted.

If <property>Tab Distance</property> is set to 0, the minimum number of tabs is always presumed.

The <property>Size Threshold</property> is used such that if a source shape's perimeter is smaller that this value, no holding tabs
will be inserted for that shape.

The size of the holding tabs is controlled by the <property>Height</property> and <property>Width</property> holding tab properties.
Height is taken to be as measured from the target depth of the profile, to the top of the desired holding tab.
The width will be the width as measured at the thinnest part of the holding tab.
The rectangles used to display tabs and the resulting gaps in the toolpaths will appear wider than this width setting.
This is to compensate for the tool diameter.

Experience will dictate the optimum tab height and widths.  Too large tabs will hold parts securely but require
extra manual cleanup to remove the tab stock.  Too small tabs run the risk of the parts breaking free, which can damage 
both parts and cutting tools.  The type of material will also affect this choice.  Metals typically can use smaller
holding tabs while woods and plastics will need wider or thicker tabs to compensate for the brittle nature of the material.

There are two types of holding tab cross-section shapes available which is defined in the <property>Tab Style</property> property: 
 *Square*{:.value} and *Triangle*{:.value}. Triangles are a good all-round tab shape, easy to clean up and provide a degree
of ramping back down into the stock.  Squares can be stronger and can also be used with lead in moves when <property>Use Lead Ins</property>
is set true.  This is useful for holding tabs in harder materials.

![](../images/02.jpg){:.icenter} 

## Properties

<table class="prop" cellspacing="0" width="100%">
<tr>
<td>
	<property>Height</property>
</td>
<td>
	The height of the holding tab measured from the stock base or target depth.
</td>
</tr>
<tr>
<td>
	<property>Maximum Tabs</property>
</td>
<td>
	The maximum number of auto tabs to insert around each shape.
</td>
</tr>
<tr>
<td>
	<property>Minimum Tabs</property>
</td>
<td>
	The minimum number of auto tabs to insert around each shape.
</td>
</tr>
<tr>
<td>
	<property>Size Threshold</property>
	<br /><span class="new">[New! 0.9.8]</span>
</td>
<td>
	Shapes with perimeters less than this value will not have any automatically calculated tabs.
</td>
</tr>
<tr>
<td>
	<property>Tab Distance</property>
</td>
<td>
	The approximate distance between each automatically generated holding tab.
</td>
</tr>
<tr>
<td>
	<property>Tab Method</property>
</td>
<td>
	<p><value>None</value> - No holding tabs will be inserted.</p>
	<p><value>Automatic</value> - Tab positions will be automatically determined.</p>
	<p><value>Manual</value> - Tab positions have been modified or set manually.</p>
	<p>
	<value>Automatic (Outer | Inner)</value> - Similar to <value>Automatic</value>, except tabs will only be added to the outside
	(or inside) shapes of regions.
    </p>
</td>
</tr>
<tr>
<td>
	<property>Tab Style</property>
	<br /><span class="new">[New! 0.9.8]</span>
</td>
<td class="desc">
	<p><value>Square</value> - Square cross section tabs.</p>
	
	<p><value>Triangle</value> - Triangle cross section tabs.</p>
	
	<p><span class="new">[New! 0.9.8N]</span><br/>
	<value>Skip</value> - Similar to the <value>Square</value> style except a rapid move will be used across the top of the tab.
	This method is commonly used in plasma cutting to insert a holding tab or bridge without having to turn off the plasma.</p>
</td>
</tr>
<tr>
<td>
	<property>Use Lead Ins</property>
</td>
<td>
	Square holding tabs will result in a vertical plunge on the trailing edge.  This can be hard on cutters, especially
	in harder materials.  If <property>Use Lead Ins</property> is set <value>True</value>, an extra lead in move (as defined in the profile's <property>Lead In Move</property> property) is inserted
	at the trailing edge.
</td>
</tr>
<tr>
<td>
	<property>Width</property>
</td>
<td>
	The final width of the holding tab, measured at the thinnest part of the tab.
</td>
</tr>
</table>

## Advanced Settings


![](../images/tabs_scale.png){:align="right" .iright} 

In some cases, such as very narrow source shapes, a problem can occur where the shape of the holding tab may extend to the toolpath
on the other side of the part and holding tabs incorrectly assigned to the wrong side.

To help resolve this problem, two parameters are available in the <span class="name">Machining</span> properties of the drawing: <property>Inner Tab Scale</property>
and <property>Outer Tab Scale</property>.  These are used to extend or contract the size of the tab 'rectangle'.  The inner tab scale will alter
the size of the tab rectangle that extends towards the source shape.  The outer tab scale will affect the tab size away from the stock shape.

The following image show a narrow source shape that has caused an incorrect holding tab. Reducing the inner tab scale resolves this problem.

![](../images/tab_length.png){:.icenter width="750" height="254"}
