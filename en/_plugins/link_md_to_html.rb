=begin

Generator to convert (filename.md) and (filename.md#anchor) format links to .html

Why?

Jekyll by default won't automatically convert .md links to .html 
Unless using jekyll-relative-links plugin.

However, this also makes links relative to the baseurl, which is a problem if we want to make standalone, self contained html documentation 

Thanks to...
https://stackoverflow.com/questions/25826770/jekyll-using-links-to-internal-markdown-files

Script modified to allow anchor links

=end
module ChangeLocalMdLinksToHtml
  class Generator < Jekyll::Generator
    def generate(site)
		site.pages.each { |p| rewrite_links(site, p) }
    end
    def rewrite_links(site, page)
		puts "changing .md links..."
		page.content = page.content.gsub(/(\[[^\]]*\]\([^:\)]*)\.md([#\)])/, '\1.html\2')
    end
  end
end