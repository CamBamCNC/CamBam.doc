---
title: Side Profiles
---
# Side Profiles

Side profiles are a method of producing 3D contours from 2D shapes, by creating radii and slopes.
Side profiles are created by manipulating the <property>Side Profile</property> composite property of the [2D Profile](profile.md){:.name} machining operation.

The files <span class="file">side profiles.cb</span> and <span class="file">heart-shaped-box.cb</span>, in the CamBam samples folder illustrate
various uses of side profile operations.

## Properties

<table class="prop" cellspacing="0" width="100%">
<tr>
<td>
	<property>Method</property>
</td>
<td>
	<value>None</value> - Normal perpendicular sides.<br>
	<value>Slope</value> - <property>Value</property> contains the angle in degrees from vertical of the slope (or bevel).<br>
	<value>Convex Radius</value> - <property>Value</property> contains the radius of the convex contour.<br> 
	<value>Concave Radius</value> - <property>Value</property> contains the radius of the concave contour.<br>
</td>
</tr>
<tr>
<td>
	<property>Value</property>
</td>
<td>
	A value that controls the selected side profile method.
</td>
</tr>

<tr>
<td>
	<property>Adjust Cut Width</property>
</td>
<td>
	When <value>False</value>, the toolpaths will just follow the calculated profile.  This is fine for a finishing pass,
	but is not suitable for clearing stock.<br>
	Set <property>Adjust Cut Width</property> = <value>True</value> to machine all the stock layers above as well as on the profile.
	This is useful for roughing operations.
	
</td>
</tr>
</table>
<br>

<img src="../images/cam/sideprofil-cutwidth.jpg">
<br>

<table class="prop" cellspacing="0" width="100%">
    <tr>
        <td width="280"><div align="left"><span class="prop">Adjust Cut Width</span> = <span class="val">True</span></div></td>
        <td width="280"><div align="right"><span class="prop">Adjust Cut Width</span> = <span class="val">False</span></div></td>
     </tr>
</table>

The sign of the <property>Value</property> parameter is significant and reversing the sign will result in different effects.

Below are some examples of various combinations of side profile methods, value signs and profile inside/outside settings.  
These images were created from the <span class="file">side profiles.cb</span> sample file.

<table width="100%">
<tr>
<td>
	<img src="../images/cam/sideprofile-outside-pos-convex.jpg" title="Method=CovexRadius, Value=+Ve, Profile=Outside"><br>
    Method=CovexRadius, Value=+Ve, Profile=Outside
</td>
<td>
    <img src="../images/cam/sideprofile-outside-ve-convex.jpg" title="Method=CovexRadius, Value=-Ve, Profile=Outside"><br>
    Method=CovexRadius, Value=-Ve, Profile=Outside
</td>
</tr>
<tr>
<td>
    <img src="../images/cam/sideprofile-inside-pos-convex.jpg" title="Method=CovexRadius, Value=+Ve, Profile=Inside"><br>
    Method=CovexRadius, Value=+Ve, Profile=Inside
</td>
<td>
    <img src="../images/cam/sideprofile-inside-ve-convex.jpg" title="Method=CovexRadius, Value=-Ve, Profile=Inside"><br>
    Method=CovexRadius, Value=-Ve, Profile=Inside
</td>
</tr>
</table>
<table width="100%">
<tr>
<td>
    <img src="../images/cam/sideprofile-outside-pos-concave.jpg" title="Method=ConcaveRadius, Value=+Ve, Profile=Outside"><br>
    Method=ConcaveRadius, Value=+Ve, Profile=Outside
</td>
<td>
    <img src="../images/cam/sideprofile-outside-ve-concave.jpg" title="Method=ConcaveRadius, Value=-Ve, Profile=Outside"><br>
    Method=ConcaveRadius, Value=-Ve, Profile=Outside
</td>
</tr>
<tr>
<td>
    <img src="../images/cam/sideprofile-inside-pos-concave.jpg" title="Method=ConcaveRadius, Value=+Ve, Profile=Inside"><br>
    Method=ConcaveRadius, Value=+Ve, Profile=Inside
</td>
<td>
    <img src="../images/cam/sideprofile-inside-ve-concave.jpg" title="Method=ConcaveRadius, Value=-Ve, Profile=Inside"><br>
    Method=ConcaveRadius, Value=-Ve, Profile=Inside
</td>
</tr>
<tr>
<td>
    <img src="../images/cam/sideprofile-outside-pos-slope.jpg" title="Method=Slope, Value=+Ve, Profile=Outside"><br>
    Method=Slope, Value=+Ve, Profile=Outside
</td>
<td>
    <img src="../images/cam/sideprofile-outside-ve-slope.jpg" title="Method=Slope, Value=-Ve, Profile=Outside"><br>
    Method=Slope, Value=-Ve, Profile=Outside
</td>
</tr>
<tr>
<td>
    <img src="../images/cam/sideprofile-inside-pos-slope.jpg" title="Method=Slope, Value=+Ve, Profile=Inside"><br>
    Method=Slope, Value=+Ve, Profile=Inside
</td>
<td>
    <img src="../images/cam/sideprofile-inside-ve-slope.jpg" title="Method=Slope, Value=-Ve, Profile=Inside"><br>
    Method=Slope, Value=-Ve, Profile=Inside
</td>
</tr>
</table>

