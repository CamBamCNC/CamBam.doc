---
title: Selecting Objects
---
# Selecting Objects

Objects can be selected by clicking them in the drawing view window, or by selecting them from the tree view on the left of the screen.<br/>
Clicking on empty space will clear any selections.

<span class="key">CTRL</span>+<b>click</b> will select multiple objects.  To deselect an object, <span class="key">CTRL</span>+<b>click</b> it again.

<span class="key">CTRL+A</span> will select all visible objects.

<span class="key">SHIFT+CTRL+A</span> will select all objects in the active layer.

Multiple objects can be selected by dragging the left mouse button to form a selection rectangle. To be selected the entire object must be inside the rectangle.

Once selected, object properties can be viewed and modified in the property browser in the lower left.

Objects can be deleted by selecting them then pressing the <span class="key">Delete</span> key.

