---
title: Automation
---
# Automation

CamBam supports two forms of automation : Scripting and Plugins.

## Scripting

Some example scripts are provided in the \scripts sub folder of the CamBam installation directory.

Refer to these forum sections for more information and examples of scripting.

[Scripts and Plugin Help](http://www.cambam.co.uk/forum/index.php?board=4.0)  

[Resources - Scripts and Plugins](http://www.cambam.co.uk/forum/index.php?board=6.0)  

## Plugins

Plugins are .NET .dll files which can be written in a variety of supported .NET languages such as C#, Visual Basic, C/C++ etc.

Some example plugins are provided in the \plugins sub folder of the CamBam installation directory.

For a fantastic introduction to writing user plugins refer to MrBean's thread on the CamBam forum:  
[How to write a CamBam plugin](http://www.cambam.co.uk/forum/index.php?topic=5.0)

