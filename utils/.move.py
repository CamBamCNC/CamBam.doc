﻿"""Includes files into other files...
"""
from subprocess import check_output
import fnmatch
import os
import re

filepattern = "*.md"
# filepattern = "cam.3d.md"

for root, dirnames, filenames in os.walk('.'):
    for filename in fnmatch.filter(filenames, filepattern):
        # matches.append(os.path.join(root, filename))
        srcfile = os.path.join(root, filename)
        if (srcfile != "_todo.md"):
            check_output("git mv " + srcfile + " en\\" + srcfile)
        