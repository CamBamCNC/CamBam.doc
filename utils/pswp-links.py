﻿"""
Convert image links of form...

<p><a href="../images/tutorial.profile-1.png"><img src="../images/tutorial.profile-1-t.png" alt="Step 1 - Insert pulley profile" class="icenter" /></a></p>

<p align="center"><a href="../images/tutorial.3d-4.png"><img src="../images/tutorial.3d-4-t.png" alt="Horizontal finish pass" class="icenter" /></a><br />
This image shows the scanline finishing toolpaths</p>

to

<div style="text-align:center;">
<figure><a href="../images/tutorial.profile-1.png" title="Step 1 - Insert pulley profile" data-size="1024x768" data-index="0">
<img src="../images/tutorial.profile-1-t.png"/>
</a></figure></div>


"""
import fnmatch
import os
import io
import re
import getimageinfo
from html.parser import HTMLParser

def GetImageSize(img_file):
    # image_url = url
    # if not image_url.endswith("/"): image_url += "/"
    # image_url += href
    # response2 = urllib.request.urlopen(image_url)
    with open(img_file, 'rb') as fimg:
        image_type,width,height = getimageinfo.getImageInfo(fimg)
    return str(width) + "x" + str(height);

class MyHTMLParser(HTMLParser):
    def __init__(self):
        HTMLParser.__init__(self,convert_charrefs=False)
        self.outbuff = []
        self.tagstack = []
        self.buffstack = [self.outbuff]
        self.inImgLink = False
        self.imgIndex = 0
        self.inCenterImg = False
        self.inCenterPtag = ""
        self.skipBreak = False
        self.PtoDIV = False

    def inLink(self):
        for ts in reversed(self.tagstack):
            if ts[0] == "a":
                return ts
        return None

    def cleanPTag(self):
        if len(self.buffstack) > 1:
            # look at 2nd top buffer
            buff2clean = self.buffstack[len(self.buffstack)-2]
            for i in range(len(buff2clean)-1,-1,-1):
                if buff2clean[i].startswith("<p"):
                    ptag = buff2clean[i]
                    buff2clean[i] = ""
                    return ptag
                if buff2clean[i].startswith("</p") or buff2clean[i].startswith("</div"):
                    return None

        return None

    def handle_starttag(self, tag, attrs):
        self.tagstack.append([tag,attrs])

        # in a tag, so start recording to a new buffer...
        if tag == "a":
            self.buffstack.append([])
            self.outbuff = self.buffstack[len(self.buffstack)-1]
        elif tag == "img":
            link = self.inLink()
            if link:
                self.inImgLink = True
                self.inCenterImg = False
                href = ""
                # replace current outbuff with <figure><a [attrs] title="" data-size="" data-index="">
                self.outbuff = ["<figure><a"]
                self.buffstack[len(self.buffstack)-1] = self.outbuff
                for aname, aval in link[1]:
                    self.outbuff.append(" "+ aname + "=\"" + aval + "\"")
                    if aname=="href":
                        href = aval
                
                img_size = GetImageSize(os.path.join(os.path.dirname(srcfile),href))
                self.outbuff.append(" data-size=\"" + img_size + "\"")
                self.outbuff.append(" data-index=\"" + str(self.imgIndex) + "\"")
                self.imgIndex += 1
                self.outbuff.append(">")

                self.outbuff.append("<"+tag)
                for aname, aval in attrs:
                    if aname=="class" and aval=="icenter":
                        # look back to previous <p> tag and remove this from buffer
                        self.inCenterPtag = self.cleanPTag()
                        self.outbuff[0] = "<div style=\"text-align:center;\">" + self.outbuff[0]
                        self.inCenterImg = True
                    elif not (aname=="alt"):
                        self.outbuff.append(" "+ aname + "=\"" + aval + "\"")
                self.outbuff.append(">")
                return
            
        elif tag=="p":
            # concatentate strings as we may move this ptag later with cleanPTags
            ptag = ""
            ptag += "<" + tag
            for aname, aval in attrs:
            # if align=center, replace with style="text-align:center;"
                if aname=="align" and aval=="center":
                    # use a <div> tag instead...
                    self.PtoDIV = True
                    ptag = "<div "
                    ptag += " style=\"text-align:center;\""
                else:
                    ptag += " "+ aname + "=\"" + aval + "\""
            ptag += ">"
            self.outbuff.append(ptag)
            return

        elif tag=="br" and self.skipBreak:
            # skip breaks in centered image paragraphs
            return

        self.skipBreak = False

        self.outbuff.append("<"+tag)
        for aname, aval in attrs:
            self.outbuff.append(" "+ aname + "=\"" + aval + "\"")
        self.outbuff.append(">")

    def handle_endtag(self, tag):
        if len(self.tagstack)==0:
            print("end tag missmath at ", str(self.getpos()))
        else:
            self.tagstack.pop()

        if tag=="p":
            if self.PtoDIV:
                self.outbuff.append("</div>")
                self.PtoDIV = False
                return
            if self.outbuff[len(self.outbuff)-1].startswith("<p"):
                # clean empty paragraphs...
                self.outbuff[len(self.outbuff)-1] = ""
                return
                

        # ignore self closing tags
        # if not any(tag in s for s in ["link","meta"]): 
        #    self.outbuff.append("</" + tag + ">")
        if not (tag == "br" or tag == "img" or tag == "link" or tag == "meta"):
            self.outbuff.append("</" + tag + ">")

        if tag=="a":
            # pop the top buffer and append it to outbuff
            abuff = self.buffstack.pop()
            self.outbuff = self.buffstack[len(self.buffstack)-1]
            self.outbuff.append("".join(abuff))
            if self.inImgLink:
                self.outbuff.append("</figure>")
                if self.inCenterImg:
                    self.outbuff.append("</div>")
                    if self.inCenterPtag:
                        self.outbuff.append(self.inCenterPtag)
                        # don't need <br> tags immediate after <p> tag
                        self.skipBreak = True
                        self.inCenterPtag = ""
                    self.inCenterImg = False
                self.inImgLink = False
        
    def handle_data(self, data):
        self.outbuff.append(data)

    def handle_comment(self, data):
        self.outbuff.append("<!-- " + data + " -->")

    def handle_entityref(self, name):
        self.outbuff.append("&" + name + ";")

    def handle_charref(self, name):
        self.outbuff.append("&#" + name + ";")

    def handle_decl(self, data):
        self.outbuff.append("<!" + data + " >")


def process_file_parser( srcfile, destfile ):
    print( "processing " + srcfile )
    parser = MyHTMLParser()

    with io.open(srcfile, 'r', encoding="utf-8") as f:
        text = f.read()

    parser.feed(text)

    with io.open(destfile, "w", encoding="utf-8") as f:
        f.write("".join(parser.outbuff))

def process_file( srcfile, destfile ):
    print( "processing " + srcfile )

    # {$include file="prop/3d.Additive.txt"}        
    # pattern = re.compile(r"{\$include file=\"([^\"]*)\"}", re.IGNORECASE)
    # <!-- $include file="prop/mop.FinalDepthIncrement.html" -->
    # <!-- $include.end -->

    # \<p\>\<a(.*)href=(\"[^\"]*\")\>\<img src=(\"[^\"]*\")

    # link in paragraph
    # \<p\>(\<a(.*)\<\/a\>)\<\/p>
    # image link in paragraph
    # \<p\>(\<a(.*)(\<img([^\>]*)\>)\<\/a\>)\<\/p>

    img_index = 0

    re_imglink = re.compile(r"\<p\s*(?:[^\>]*?)\>(\<a(.*?)(\<img([^\>]*?)\>)\<\/a\>)(.*)\<\/p>", re.IGNORECASE)
    re_href = re.compile(r"href=\"([^\"]*)\"", re.IGNORECASE)
    re_src = re.compile(r"src=(\"[^\"]*\")", re.IGNORECASE)
    re_alt = re.compile(r"alt=(\"[^\"]*\")", re.IGNORECASE)
    re_imgclass = re.compile(r"class=(\"[^\"]*\")", re.IGNORECASE)
    
    ftext = ""
    
    with io.open(srcfile, 'r', encoding="utf-8") as f:
        for line in f:
            match = re.search(re_imglink,line)
            if match:
                ftext += "<!-- Change this.... -->\n"
                
                href = ""
                src = ""
                title = ""
                img_class = ""
                img_size = ""

                match_href = re.search(re_href,match.group(1))
                if match_href:
                    href = match_href.group(1)
                match_src = re.search(re_src,match.group(3))
                if match_src:
                    src = match_src.group(1)
                match_alt = re.search(re_alt,match.group(3))
                if match_alt:
                    title = match_alt.group(1)
                match_imgclass = re.search(re_imgclass,match.group(3))
                if match_imgclass:
                    img_class = match_imgclass.group(1)

                # TODO: get image size
                img_size = GetImageSize(os.path.join(os.path.dirname(srcfile),href))

                if "icenter" in img_class:
                    ftext += "<div style=\"text-align:center;\">"
                else:
                    ftext += "<div>"
                ftext += "<figure>"
                ftext += "<a href=\"" + href + "\" title=" + title + " data-size=\"" + img_size + "\" data-index=\"" + str(img_index) + "\">"
                ftext += "<img src=" + src + ">"
                ftext += "</a></figure>"
                match.group(1)
                ftext += "</div>"

                img_index += 1

            else:
                ftext += line
 
    with io.open(destfile, "w", encoding="utf-8") as f:
        f.write(ftext)

# filepattern = "3d-profile.html"
filepattern = "*.html"

# for root, dirnames, filenames in os.walk('..\\_site\\tutorials\\'):
for root, dirnames, filenames in os.walk('..\\_site\\'):
    for filename in fnmatch.filter(filenames, filepattern):
        # matches.append(os.path.join(root, filename))
        srcfile = os.path.join(root, filename)
        # destfile = os.path.join("C:\\dump\\pytest\\", filename)
        # destfile = srcfile.replace(".html","-pswp.html")
        destfile = srcfile
        process_file_parser( srcfile, destfile )
        