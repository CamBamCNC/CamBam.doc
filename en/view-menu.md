---
title: View Menu
---
# View Menu

## Zoom

Three zoom options are available from the <span class="cmd">View</span> menu:

- <span class="cmd">Reset</span>  
reverts to a known position. (XY plane) and performs a <span class="cmd">Zoom To Fit</span>.  
Equivalent to <span class="key">ALT</span> + double click.  
If [Rotation Mode](configuration.md#RotationMode){:.prop} <value>Left_Middle</value> is active, the same operation can be done by a double left click while holding the middle button pressed.  
If [Rotation Mode](configuration.md#RotationMode){:.prop} <value>Left_Right</value> is active, a double left click while holding the right button pressed.

- <span class="cmd">Zoom To Fit</span>  
Zoom so that all objects of all visible layers are visible, 
without changing the view orientation. Objects in hidden layers are not taken into account to calculate the zoom factor.

- <span class="cmd">Zoom Actual size</span>  
Zooms so that the drawing objects are shown approximately true sized (allowing for display size variations).

## Displaying the grid and axis


![](images/grid-options.png){:align="right" .iright}
The grid and axis display can be enabled and disabled using the following toolbar icons ![](images/view_tools.png) 
or by selecting <span class="cmd">Show grid</span> and <span class="cmd">Show axis</span> options from the <span class="cmd">View</span> menu.

The appearance of the grid, including color, major and minor units, size and position, can be changed in the grid
[system configuration settings.](configuration.md#GridColor){:.prop}.

There are two sets of grid settings: One for inch drawing units and the other for metric.

## Display Setup

The following options enable or disable the display of graphical aids.

- <span class="cmd">Show toolpaths</span> - Enable / disable the display of lines representing the toolpaths.

- <span class="cmd">Show cut widths</span> - Enable / disable the display of a shaded area depicting the width of cuts
along the toolpaths based on the specified tool diameters.
![](images/graphique2.png){:.icenter}

- <span class="cmd">Show stock</span> - Enables / disables the display of the 3D representation of the block of material to be machined.

- <span class="cmd">Show nests</span> - Enables / disables the display of arrays of machining operations, defined in the Part <property>Nest</property> properties.

- <span class="cmd">Show rapids</span> - Enable / disable the display of dotted lines representing rapid moves (G0).

- <span class="cmd">Show direction arrows</span> - Enable / disable the display of arrows indicating the direction of travel of the tool.

- <span class="cmd">Show grid</span> - Enable / disable the display of the grid.

- <span class="cmd">Show axis</span> - Enable / disable the display of XYZ axis lines of the 3D view.

<a name="Stock">&nbsp;</a>
![](images/drawing-view-items.png){:.icenter}
 
- <span class="cmd">Anti-alias</span> - Enable / disable anti-aliasing.

- <span class="cmd">Wireframe</span> - Toggle the display of 3D objects between shaded surfaces or wireframe mode.

![](images/graphique3.png){:.icenter}

- <span class="cmd">Snap to grid</span> - Enable / disable snap to grid.

- <span class="cmd">Snap to object</span> - Enable / disable snapping to other drawing objects.

- <span class="cmd">Windows opacity</span> - A value between 0 and 100% (opaque) which allows tracing over reference drawings in windows behind the CamBam drawing.

[![](images/graphique4-t.png){:.icenter}](images/graphique4.png)

- <span class="cmd">XY / XZ / YZ Plane</span> - Switches the view seen from above (XY - default), Front (XZ) or side (YZ). For now only the XY plane can be used to draw with the mouse.

- <span class="cmd"><a name="ToolpathFilter">Toolpath view filter</a></span> - Used to view step by step the tool path according to their order of execution or level in Z.

[![](images/toolpath-filter-t.png){:align="left" .ileft}](images/toolpath-filter.png)

<span class="prop">Toolpath index</span>: if checked, you can view the tool path in order of their execution by changing the numerical value on the right.

In this example, the 7th toolpath is highlighted in yellow, previously cut toolpaths are shown in purple and uncut toolpaths are not visible.

<span class="prop">Z depth index</span>: if checked, you can view the tool path in order of Z level by changing the numerical value on the right. All tool paths on the same Z level will be displayed simultaneously.

If both Toolpath index and Z depth index are ticked, the toolpath will be filtered by index down to the maximum Z depth specified.

<span class="prop">Cut toolpath color</span> display or hide toolpaths cut previous to the current toolpath.

<span class="prop">Toolpath color</span> when checked, the current toolpath will be highlighted in the selected color on the right; 
If unchecked, the current toolpath will be displayed using the standard arc and line move colors.

Click on the colored rectangles to change the display color of the toolpaths.

You can also choose the line width by changing the width of <property>line width</property> and transparency by the <property>alpha</property> value.

The <span class="name">Toolpath View Filter</span> window can be kept open while manipulating the drawing, such zooming and panning the display.

Depending on the drawing's <property>Toolpath Visibility</property> setting (<value>All</value> or <value>Selected Only</value>), the filter will show the path of all machining operations 
or only those machining operations or Parts selected in the drawing tree.

Display settings are available in the property grid by selecting the top level (Drawing) object of the drawing tree.

![](images/drawing-view-properties.png){:.icenter}

The display colors can be changed in the [system configuration](configuration.md){:.name} Colors group.

![](images/config-colors.png){:.icenter}   


