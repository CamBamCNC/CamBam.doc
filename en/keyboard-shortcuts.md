---
title: Keyboard Shortcuts
---
# Keyboard Shortcuts

<table style="width:101%;">
	<tr>
		<td><span class="key">Ctrl+A</span></td>
		<td>Select all objects</td>
	</tr>
	<tr>
		<td><span class="key">Shift+Ctrl+A</span></td>
		<td>Select all objects on the active layer</td>
	</tr>
	<tr>
		<td><span class="key">Ctrl+B</span></td>
		<td>Edit - Break at intersections</td>
	</tr>
	<tr>
		<td><span class="key">Ctrl+C</span></td>
		<td>Copy selected object to the clipboard</td>
	</tr>
	<tr>
		<td><span class="key">Ctrl+E</span></td>
		<td>Resize selected drawing objects</td>
	</tr>
	<tr>
		<td><span class="key">Ctrl+F</span></td>
		<td>Open the toolpath filter window</td>
	</tr>
	<tr>
		<td><span class="key">Ctrl+G</span></td>
		<td>Toggle snap to grid mode</td>
	</tr>
	<tr>
		<td><span class="key">Ctrl+I</span></td>
		<td>Convert to Region</td>
	</tr>
	<tr>
		<td><span class="key">Ctrl+J</span></td>
		<td>Join selected drawing objects</td>
	</tr>
	<tr>
		<td><span class="key">Ctrl+M</span></td>
		<td>Move selected drawing objects</td>
	</tr>
	<tr>
		<td><span class="key">Ctrl+O</span></td>
		<td>Open a file</td>
	</tr>
	<tr>
		<td><span class="key">Ctrl+P</span></td>
		<td>Convert selected objects to polylines</td>
	</tr>
	<tr>
		<td><span class="key">Ctrl+R</span></td>
		<td>Rotate selected drawing objects</td>
	</tr>
	<tr>
		<td><span class="key">Ctrl+S</span></td>
		<td>Save the current file</td>
	</tr>
	<tr>
		<td><span class="key">Ctrl+T</span></td>
		<td>Regenerate all toolpaths</td>
	</tr>
	<tr>
		<td><span class="key">Ctrl+U</span></td>
		<td>Union selected drawing objects</td>
	</tr>
	<tr>
		<td><span class="key">Ctrl+V</span></td>
		<td>Paste from the clipboard</td>
	</tr>
	<tr>
		<td><span class="key">Shift+Ctrl+V</span></td>
		<td>Copy the format from the clipboard object to the selected object</td>
	</tr>
	<tr>
		<td><span class="key">Ctrl+W</span></td>
		<td>Produce gcode file</td>
	</tr>
	<tr>
		<td><span class="key">Ctrl+X</span></td>
		<td>Cut object and place on clipboard</td>
	</tr>
	<tr>
		<td><span class="key">Ctrl+Y</span></td>
		<td>Redo the last undone operation</td>
	</tr>
	<tr>
		<td><span class="key">Ctrl+Z</span></td>
		<td>Undo the last operation</td>
	</tr>
	<tr>
		<td><span class="key">Space</span></td>
		<td>Hide/Unhide selected Layers, Parts or MOPs</td>
	</tr>
	<tr>
		<td><span class="key">Ctrl+Space</span> (<span class="new">New V1.0</span>)</td>
		<td>Lock/Unlock selected Layers</td>
	</tr>
	<tr>
		<td><span class="key">A</span></td>
		<td>Draw an arc</td>
	</tr>
	<tr>
		<td><span class="key">C</span></td>
		<td>Draw a circle</td>
	</tr>
	<tr>
		<td><span class="key">D</span></td>
		<td>Draw a point list (dots)</td>
	</tr>
	<tr>
		<td><span class="key">M</span></td>
		<td>Measure</td>
	</tr>
	<tr>
		<td><span class="key">P</span></td>
		<td>Draw a polyline</td>
	</tr>
	<tr>
		<td><span class="key">R</span></td>
		<td>Draw a rectangle</td>
	</tr>
	<tr>
		<td><span class="key">T</span></td>
		<td>Insert text</td>
	</tr>
	<tr>
		<td><span class="key">Cursor Up/Down/Left/Right</span></td>
		<td>Pan the drawing view</td>
	</tr>
	<tr>
		<td><span class="key">Shift+Cursor Up/Down/Left/Right</span></td>
		<td>Move the selected object of a minor unit of the grid</td>
	</tr>
	<tr>
		<td><span class="key">Shift+Ctrl+Cursor Up/Down/Left/Right</span></td>
		<td>Move the selected object of a major unit of the grid</td>
	</tr>
	<tr>
		<td><span class="key">Page Up</span> or <span class="key">Num Pad -</span>
		</td>
		<td>Zoom out</td>
	</tr>
	<tr>
		<td><span class="key">Page Down</span> or <span class="key">Num Pad +</span>
		</td>
		<td>Zoom in</td>
	</tr>
	<tr>
		<td><span class="key">Home</span></td>
		<td>Reset view</td>
	</tr>
	<tr>
		<td><span class="key">Del</span></td>
		<td>Delete selected Objects/Layers/Mops</td>
	</tr>
	<tr>
		<td><span class="key">F1</span></td>
		<td>Help</td>
	</tr>
	<tr>

</table>
