---
title: Drawing and System Tabs
---
# Drawing and System Tabs

Two tabs are available above the tree view, at the left side of the CamBam window: <span class="name">Drawing</span> and <span class="name">System</span>.

The <span class="name">Drawing Tab</span> shows the contents of the current open CamBam drawing. 

The <span class="name">System Tab</span> contains libraries and settings common to all drawings. 

## Drawing Tab

![Drawing tree](images/drawing-tree.png){:align="right" .iright}

The <span class="name">Drawing Tab</span> displays the contents of the current drawing file, presented in a tree layout.

The first item of the drawing tree contains general settings specific to the drawing.  This top object will be labeled
using the name of the drawing file.  In the example pictured, the file is titled : 'MyDrawing'.

The drawing is then divided into two main sections: <span class="name">Layers</span> and <span class="name">Machining</span>.

[Layers](cad/layers.md){:.name} are used to separate the drawings items into manageable sections
which can be labeled, color coded, hidden and made visible to aid CAD design.  The drawing tree shows
the name of each layer and the color used to display drawing objects contained within the layer.

Expanding a layer within the tree shows the drawing objects in the layer.  The icon and name of each item denote
the drawing object's type.  The object's ID is shown in brackets.  All objects within the drawing have a unique identifier number.

The <span class="name">Machining</span> folder is further divided into [Parts](cam/part.md){:.name} which in turn
contain all the machining operations used within the part.

## Property Window

![](images/drawing-tree-selection.png){:align="right" .iright}

Selecting items in the tree view allows their properties to be modified in the property window below the tree.

In the image shown, the properties of the operation 'Profile1' in 'Part1' are displayed in the property window.

The size of the tree and property window can be adjusted by dragging the left mouse button on the dividing line
between the two sections, when the mouse cursor changes to an ![](images/pointeur_2fv.png) icon.
The property window's column size can be adjusted by dragging with the left mouse button on the column divider, when
the mouse cursor changes to: ![](images/pointeur_2fh.png).

The tool bar at the top of the property window contains a number of buttons, used to customize the property display:

![](images/bouton_tri.png) Switch between displaying properties alphabetically or by category.

<span class="cmd">Advanced / Basic</span>  

In <span class="cmd">Basic</span> view mode, only a subset of the properties are shown; the most commonly used ones together with any values that have
been changed from their default settings.  Clicking <span class="cmd">Advanced</span> will make all the selected item's properties visible.

<!-- $pagebreak -->

![](images/bouton_help.png)
Displays a small window at the bottom of the property window, containing a brief description of the selected parameter.


For some objects, such as machining operations, a symbol may be shown to the right of the property name.
These are:<br/> 
![](images/basics/cbv_auto.png) <b>Auto</b>, indicating the value used will be automatically calculated.  
![](images/basics/cbv_value.png) an explicit <b>Value</b> has been entered.  
![](images/basics/cbv_default.png) The current value is the <b>Default</b> (usually inherited from a machining operation's style.

![](images/value-context-menu.png){:align="right" .iright}

Clicking these icons will show a context menu where the type of value can be changed.

## <a name="System">System Tab</a>

![](images/system-tree.png){:align="right" .iright}

The <span class="name">System Tab</span> shows another tree view, this time displaying objects and settings available to all CamBam drawings,
and contains the following sub folders:

<span class="name">Configuration</span>: Equivalent to the <span class="cmd">Tools - Options</span> menu and allows access to the system global [configuration settings](configuration.md){:.name}.

 <span class="name">CAM Styles</span> : Folder containing [machining style libraries](cam/cam-style.md){:.name}.

 <span class="name">Tools</span> : Folder containing [libraries of cutting tools](cam/tool-libraries.md){:.name}.

 <span class="name">Post Processor</span> : [Post Processor](cam/post-processor.md){:.name} definitions, used to control how gcode is formatted from machining operations.

 <span class="name">Materials/Machine Definitions</span> : Both these sections are in an early stage of development and are intended for use in future releases.


