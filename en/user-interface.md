---
title: User Interface
---
# User Interface

This section introduces parts of the CamBam user interface and explains some terminology used.

![CamBam user interface](images/user-interface-m.png){:.icenter}

<table width="100%"> 
<tbody> 
<tr> 
	<td width="50%">
		<h2>1. Main Drawing Window</h2>
		3D View of the current drawing and toolpaths.
	</td> 
	<td width="50%">
		<h2>5. Tool Bar</h2>
		Short cuts to commonly used tools and settings.
	</td> 
</tr>
<tr>
	<td>
		<h2>2. Drawing Tree View</h2>
		Shows all layers, drawing objects and machining operations (mops) in the current drawing.
	</td>
	<td>
		<h2>6. Message Window</h2>
		Errors, warnings and informational messages are displayed here.
	</td>
</tr>
<tr>
	<td>
		<h2>3. Object Property Window</h2>
		Display and edit properties of objects that are selected in the drawing window or drawing tree.
	</td>
	<td>
		<h2>7. Drawing Context Menu</h2>
		Menu for commonly used routines and operations applicable to selected objects.
	</td>
</tr>
<tr>
	<td>
		<h2>4. Main Menu Bar</h2>
		Main menus for the application.
	</td>
	<td>
		<h2>8. System Tab</h2>
		Provides access to settings common to all drawings such as general configuration settings,
		tool libraries, machining styles and post processors.
	</td>
</tr> 
</tbody> 
</table>

