---
title: Rotating and panning the drawing
---
# Rotating and panning the drawing

## Rotation

The 3D view is rotated by holding down the <span class="key">ALT</span> key whilst dragging the left mouse button.

Other mouse and key combinations for rotations are available in the [Rotation Mode](configuration.md#RotationMode){:.prop} option of the system configuration settings.

## Panning

The drawing view is translated by dragging the center mouse button.

The cursor keys can also be used to translate the drawing view.

## Zooming

Scrolling the mouse wheel will zoom in and out.  Move the mouse cursor over the area you would like to zoom in on when scrolling.

The number pad <span class="key">+</span> and <span class="key">-</span> keys can also be used to zoom in and out.

## Resetting

<span class="key">ALT</span> + double click will reset the view orientation. If <value>Left_Middle</value> [Rotation Mode](configuration.md#RotationMode){:.prop} is used, holding the middle mouse button whilst left double clicking will reset the view.

If the <value>Left_Middle</value> [Rotation Mode](configuration.md#RotationMode){:.prop} is used, hold down the middle mouse button while double clicking the left button to reset the view.

The view can also be reset by selecting the <span class="cmd">View - Zoom To Fit</span> menu option.
