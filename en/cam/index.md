---
title: Machining (CAM)
---

## Machining (CAM)

{:.subcontents}
- **[Machining Basics](basics.md)**
- **[Profile](profile.md)**
- **[Pocket](pocket.md)**
- **[Drill](drill.md)**
- **[Engrave](engrave.md)**
- **[3D Profile](3d.md)**
- **[Lathe](lathe.md)**
- **[Creating GCode](creating-gcode.md)**
- **[Machining Options](machining-options.md)**
- **[Edit Gcode](gcode-editor.md)**
- **[CAM Part](part.md)**
- **[CAM Styles](cam-style.md)**
- **[Lead Moves](lead-moves.md)**
- **[Holding Tabs](holding-tabs.md)**
- **[Side Profiles](side-profile.md)**
- **[Post Processor](post-processor.md)**
- **[Nesting](nesting.md)**
- **[Back Plotting](back-plotting.md)**
- **[Tool Libraries](tool-libraries.md)**
- **[Speeds and Feeds Calculator](speeds-and-feeds.md)**
