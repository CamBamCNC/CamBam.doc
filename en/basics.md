---
title: CamBam Basics
---

## Basics

{:.subcontents}
- **[User Interface](user-interface.md)**
- **[Drawing and System tabs](basics-drawing-and-system.md)**
- **[Rotating and panning](basics-rotating-and-panning.md)**
- **[Selecting objects](basics-selecting-objects.md)**
- **[Toolpaths and gcode](toolpaths-and-gcode.md)**
- **[Drawing Units](drawing-units.md)**
- **[File Menu](file-menu.md)**
- **[View Menu](view-menu.md)**
- **[Tools Menu](tools-menu.md)**
- **[Simple Example](simple-example.md)**
- **[Keyboard Shortcuts](keyboard-shortcuts.md)**
