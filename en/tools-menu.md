---
title: Tools Menu
---
# Tools Menu

CamBam has a number of utility functions grouped in the <span class="cmd">Tools</span> menu.

- <span class="cmd">Save settings</span>  
Saves system configuration settings and any modified system libraries or post processors.

- <span class="cmd">Save settings on exit</span>  
If this menu item is checked, configuration and other system changes will be saved automatically when CamBam is closed.

- <span class="cmd">Browse system folder</span>  
Opens the folder containing CamBam system files (libraries, post processors, samples, scripts etc).  
The location of this folder can be specified in the [System Path](configuration.md#SystemPath){:.prop} configuration setting.

- <span class="cmd">Options</span>  
Opens a window where system [configuration settings](configuration.md){:.name} can be maintained.

- <span class="cmd">Check for new version</span>  
Determines whether there are any newer CamBam updates available from the CamBam website.

- <span class="cmd">Clear messages</span>  
Clears messages from the information window below the drawing window.

- <span class="cmd">Get object extremas</span>  
Shows the extrema points and dimensions of the selected drawing objects.

[![](images/extremas-t.png){:.icenter}](images/extremas.png)

<span class="prop">Min</span>: minimum coordinates of the object in X, Y and Z are separated by a comma.   
Example: X=-60, Y=-50.000..., Z=0  

<span class="prop">Max</span>: maximum coordinates of the object in X, Y and Z are separated by a comma.  
Example: X=60, Y=50.000..., Z=15.000...

<span class="prop">Width, Height, Depth</span> Maximum dimensions of the object in drawing units.


- <span class="cmd">Measure</span> (<span class="key">M</span> shortcut key)
Allows you to draw a line to make a measurement between two points.
The measurement result is displayed in a new window.

{:align="center"}
![](images/mesure1.png){:width="275" height="157"} ![](images/mesure2.png){:width="392" height="106"}

- <span class="cmd">Reload post processors</span>  
Reloads all the post processor definitions from disk.  This may be needed if a post processor has been modified from another
instance of CamBam.

- <span class="cmd">Simulate with CutViewer</span>

Starts the third-party software _CutViewer Mill_, to provide a 3D machining simulation from the Gcode file produced. 
To avoid having to provide _CutViewer_ parameters manually, you must use a post processor designed to work with this software. 
(E.g. Mach3-CV for milling, Mach3-Turn-CV for turning). You must also define a [stock object](cam/machining-options.md#Stock){:.name} 
in the machining or Part objects.

[![](images/CutViewer_UI-t.png){:.icenter}](images/CutViewer_UI.png)


