---
title: Tutorial - Timing Pulley Profile
---
# Tutorial: Timing Pulley Profile

This tutorial demonstrates using [Profile](../cam/profile.md){:.name} machining operations to generate an HTD5 timing pulley.

This tutorial uses the <span class="name">Plus Toolkit plugin</span> to generate the timing pulley profile. 

[Download the files used in this tutorial](../images/tutorials/pulley/24tooth_pulley.zip)

## Step 1 - Insert an HTD timing pulley outline. 

Use the new Plus Toolkit to generate a timing pulley by selecting the <span class="cmd">Toolkit - Timing Pulley</span> menu item.

The plugin will prompt for the number of teeth for a 5mm pitch pulley, then insert a new curve with the center of the pulley about the origin.

<span class="key">ALT</span> + double click will zoom the drawing to fit the view window.

[![Step 1 - Insert pulley profile](../images/tutorial.profile-1-t.png){:.icenter}](../images/tutorial.profile-1.png)

## Step 2 - Insert a Profile machine operation

![](../images/tutorial.profile-2.png){:align="right" .iright}

Select the pulley outline then click the <span class="name">Profile</span> machining operation button ![CAM Profile MOP](../images/basics/cam_profile.png) from the toolbar.
A new profile object will be created and displayed under the <span class="name">Machining</span> folder in the drawing tree.
The object property window will display the profile's properties ready for editing.

Change the Profile machine operation's properties to the following:

<table border="0" align="center">
    <tbody>
	<tr><td style="width: 150px"><property>Tool Diameter</property></td><td style="width: 100px">2</td></tr>
    <tr><td><property>Depth Increment</property></td><td>0.5<br /></td></tr>
    <tr><td><property>Target Depth</property></td><td> -5</td></tr>
	<tr><td><property>Cut Feedrate</property></td><td>200<br /></td></tr>
    <tr><td><property>Plunge Feedrate</property></td><td>100 <br /></td></tr>
	<tr><td><property>Clearance Plane</property></td><td>1.5<br /></td></tr>
	</tbody>
</table>

> **Note:** Some properties such as <property>Clearance Plane</property> may not be shown in the property grid.  
Clicking the <span class="name">Advanced</span> button at the top of the property grid will display all available properties.
{:.note}

![](../images/poulie-3.png){:.icenter}

Generate the resulting toolpath for the profile; right click the drawing to bring up the drawing context menu, then select <span class="cmd">Machine - Generate Toolpaths</span>.

[![Step 2 - Insert Profile MOP](../images/tutorial.profile-3-t.png){:.icenter}](../images/tutorial.profile-3.png)

To rotate the 3D drawing view, hold the <span class="key">ALT</span> key then click and drag on the drawing.
To reset the view, hold the <span class="key">ALT</span> key then double click the drawing.
Another rotation mode (<value>Left_Middle</value>) can be set in the [Rotation Mode](../configuration.md#RotationMode){:.prop} property of system configuration settings.
If this mode is selected the view can be rotated by clicking the middle mouse button and dragging with the left.
To reset the view in this mode hold the center mouse button and double click.

[![Step 2 - Rotating View](../images/tutorial.profile-4-t.png){:.icenter}](../images/tutorial.profile-4.png)

## Step 3 - Creating the inner hole 

First draw a circle using the circle drawing tool ![Draw Circle](../images/basics/circle.png) with the center on the origin with <property>Diameter</property> = 8.

Select the circle and insert another profile machining operation ![CAM Profile MOP](../images/basics/cam_profile.png).  
Set the target depth and other properties to match the first profile operation.

> **Hint:** A quick way to do this is to select <span class="name">Profile1</span> and copy it to the clipboard
(using the context menu or <span class="key">CTRL+C</span>).  Then select <span class="name">Profile2</span> and use the <span class="cmd">Paste Format</span> command from the context
menu shown when right clicking <span class="name">Profile2</span>, or use <span class="key">SHIFT+CTRL+V</span>.
{:.note}

Change the <property>Inside / Outside</property> property to <value>Inside</value>.  
Again, right click the machine operation in the file tree and <span class="cmd">Generate Toolpaths</span>.

[![Step 3 - Inside Profile](../images/tutorial.profile-5-t.png){:.icenter}](../images/tutorial.profile-5.png)

## Step 4 - Creating GCode

Before producing the gcode output, now would be a good time to save your drawing.

Visually inspect the toolpaths and double check the parameters of each machining operations.

To create a gcode file (post), right click to get the drawing menu then select <span class="cmd">Machine - Produce GCode</span>.

CamBam will then prompt for the location of the gcode file to produce.
If the drawing file has been saved, the default gcode file will be in the same folder as the drawing file, with a .nc extension.

If the destination file already exists you will be asked to confirm whether to overwrite it.

To control how the gcode file is produced, select the <span class="name">Machining</span> folder from the drawing tree.
The machining properties for this drawing will then be displayed in the object properties window.
