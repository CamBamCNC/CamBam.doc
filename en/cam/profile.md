---
title: ![icon](../img/icon/cam_profile32.png) Profile Machining Operation
---
# ![icon](../img/icon/cam_profile32.png) Profile Machining Operation

A 2.5D Profile machining operation is typically used to cut out shapes.

Other uses include facing edges and with increased cut widths can be used to create pockets.

Cuts can be inside or outside a selected shape.

Lead in moves and holding tabs are supported.

## Properties
<table class="prop" cellspacing="0" width="100%">
<!-- $include file="../en/_prop/mop.ClearancePlane.html" -->
<tr>
<td>
	<property>Clearance Plane</property>
</td>
<td>
    <p>The clearance plane (offset from the work plane).</p>
    <p>The clearance plane should be clear of the stock and any holding devices to allow free movement to any location.</p>
</td>
</tr><!-- $include.end -->
<!-- $include file="../en/_prop/mop.CollisionDetection.html" -->
<tr>
<td>
	<property>Collision Detection</property>
</td>
<td>
    <p>Makes sure adjacent toolpaths do not overlap. Multiple Toolpaths are unioned together.</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.CornerOvercut.html" -->
<tr>
<td>
	<property>Corner Overcut</property>
	<br/><span class="new">[New 0.9.8]</span>
</td>
<td markdown="1">
![](../images/overcut.jpg){:align="right" .iright}
Set CornerOvercut to True to add an extra machining move, which will cut into inside corners that would not ordinarily be cut.
This will result in some stock overcutting but is useful in cases where machined parts
will be fitted together such as slot joints or inlays.
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.CustomMOPFooter.html" -->
<tr>
<td>
	<property>Custom MOP Footer</property>
</td>
<td>
    <p>A multi-line gcode script that will be inserted into the gcode post after the current machining operation.</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.CustomMOPHeader.html" -->
<tr>
<td>
	<property>Custom MOP Header</property>
</td>
<td>
    <p>A multi-line gcode script that will be inserted into the gcode post before the current machining operation.</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.CutFeedrate.html" -->
<tr>
<td>
	<property>Cut Feedrate</property>
</td>
<td>
    <p>The feed rate to use when cutting.</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.CutOrdering.html" -->
<tr>
<td>
	<property>Cut Ordering</property>
</td>
<td>
    <p>Controls whether to cut to depth first or all cuts on this level first.</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.CutWidth.html" -->
<tr>
<td>
	<property>Cut Width</property>
</td>
<td>
    <p>The total width of the cut.  If this width is greater than the tool diameter, multiple parallel cuts are used.</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.DepthIncrement.html" -->
<tr>
<td>
	<property>Depth Increment</property>
</td>
<td>
    <p>Depth increment of each machining pass. Determines the number of passes to reach the final target depth.</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.Enabled.html" -->
<tr>
<td>
	<property>Enabled</property>
</td>
<td>
    <p>
        <value>True</value>: The toolpaths associated with this machining operation are displayed and included in the gcode output<br>
        <value>False</value>: The operation will be ignored and no gcode or tool paths will be produced for this operation.
    </p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.FinalDepthIncrement.html" -->
<tr>
<td>
	<property>Final Depth Increment</property>
</td>
<td>
    <p>The depth increment of the final machining pass.</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.HoldingTabs.html" -->
<tr>
<td>
	<property>Holding Tabs</property>
</td>
<td>
<p>Defines holding tabs (bridges) to prevent cut parts moving while cutting.</p>
<p markdown="1">See the [holding tab reference](holding-tabs.md){:.name} for more information.</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.InsideOutside.html" -->
<tr>
<td>
	<property>Inside / Outside</property>
</td>
<td>
    <p>
        Controls whether to cut Inside or Outside the selected shapes.<br>
        For open shapes there is not inside or outside, so the point order controls which side of the line to cut.
    </p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.LeadInMove.html" -->
<tr>
<td>
	<property>Lead In Move</property>
</td>
<td>
<p>Defines the type of lead in move to use.</p>
<p>
    <property>Lead Move Type</property>: <value>None</value> | <value>Spiral</value> | <value>Tangent</value><br>
    <property>Spiral Angle</property>: Used by spiral and tangents to control ramp angle.<br>
    <property>Tangent Radius</property> : The radius of the tangent lead in<br>
    <property>Lead Move Feedrate</property> : The feedrate to use for the lead move.  If 0, <property>Cut Feedrate</property> is used.
</p>
<p markdown="1">Refer to the [lead move section](lead-moves.md){:.name} for more information.</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.LeadOutMove.html" -->
<tr>
<td>
	<property>Lead Out Move</property>
	<br/><span class="new">[New! 0.9.8]</span>
</td>
<td>
<p>Defines the type of lead out move to use.</p>
<p markdown="1">Refer to the [lead move section](lead-moves.md){:.name} for more information.</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.MaxCrossoverDistance.html" -->
<tr>
<td>
	<property>Max Crossover Distance</property>
</td>
<td>
<p>Maximum distance as a fraction (0-1) of the tool diameter to cut in horizontal transitions.</p>
<p>If the distance to the next toolpath exceeds MaxCrossoverDistance, a retract, rapid and plunge to the next position, via the clearance plane, is inserted.</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.MillingDirection.html" -->
<tr>
<td>
	<property>Milling Direction</property>
</td>
<td>
    <p>Controls the direction the cutter moves around the toolpath.</p>
    <p>
        <value>Conventional</value> | <value>Climb</value> | <value>Mixed</value>
    </p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.Name.html" -->
<tr>
<td>
	<property>Name</property>
</td>
<td>
    <p>
        Each machine operation can be given a meaningful name or description.<br>
        This is output in the gcode as a comment and is useful for keeping track of the function of each machining operation.
    </p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.OptimisationMode.html" -->
<tr>
<td>
	<property>Optimisation Mode</property>
</td>
<td>
    <p>An option that controls how the toolpaths are ordered in gcode output.</p>
    <p>
        <value>New (0.9.8)</value> - A new, improved optimiser currently in testing.<br>
        <value>Legacy (0.9.7)</value> - Toolpaths are ordered using same logic as version 0.9.7.<br>
        <value>None</value> - Toolpaths are not optimised and are written in the order they were generated.
    </p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.PlungeFeedrate.html" -->
<tr>
<td>
	<property>Plunge Feedrate</property>
</td>
<td>
    <p>The feed rate to use when plunging.</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.PrimitiveIds.html" -->
<tr>
<td>
	<property>Primitive IDs</property>
</td>
<td>
    <p>List of drawing objects from which this machine operation is defined.</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.RoughingFinishing.html" -->
<tr>
<td>
	<property>Roughing / Finishing</property>
</td>
<td>
    <p>
        Currently only supported by <span class="name">3D Profile</span> and <span class="name">Lathe</span> machining operations.
    </p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.RoughingClearance.html" -->
<tr>
<td>
	<property>Roughing Clearance</property>
</td>
<td>
    <p>This is the amount of stock to leave after the final cut.</p>
    <p>Remaining stock is typically removed later in a finishing pass.</p>
    <p>Negative values can be used to oversize cuts.</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.SideProfile.html" -->
<tr>
<td>
	<property>Side Profile</property>
</td>
<td>
    <p>
        A composite property that enables the creation of pseudo 3D
        objects from 2D shapes by creating radii and slopes.
    </p>
<p markdown="1">See the [side profiles reference](side-profile.md){:.name} for more information.</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.SpindleDirection.html" -->
<tr>
<td>
	<property>Spindle Direction</property>
</td>
<td>
    <p>The direction of rotation of the spindle.</p>
    <p><value>CW</value> | <value>CCW</value> | <value>Off</value></p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.SpindleRange.html" -->
<tr>
<td>
	<property>Spindle Range</property>
</td>
<td>
    <p>The pulley number or dial setting of the spindle for the target speed.</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.SpindleSpeed.html" -->
<tr>
<td>
	<property>Spindle Speed</property>
</td>
<td>
    <p>The speed in RPM of the spindle.</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.StartPoint.html" -->
<tr>
<td>
	<property>Start Point</property>
</td>
<td>
    <p>
        Used to select a point, near to where the first toolpath should begin machining.<br />
        If a start point is defined, a small circle will be displayed at this point when the machining operation
        is selected.  The start point circle can be moved by clicking and dragging.
    </p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.StepOver.html" -->
<tr>
<td>
	<property>StepOver</property>
</td>
<td>
    <p>The cut is increased by this amount each step, expressed as a fraction (0-1) of the cutter diameter.</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.StepoverFeedrate.html" -->
<tr>
<td>
	<property>Stepover Feedrate</property>
</td>
<td>
    <p>The feed rate to use for crossover moves.</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.StockSurface.html" -->
<tr>
<td>
	<property>Stock Surface</property>
</td>
<td>
    <p>This is the Z offset of the stock surface at which to start machining.</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.Style.html" -->
<tr>
<td>
	<property>Style</property>
	<br /><span class="new">[New! 0.9.8]</span>
</td>
<td>
<p markdown="1">
Select a [CAM Style](cam-style.md){:.name} for this machining operation.
All default parameters will be inherited from this style.
</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.Tag.html" -->
<tr>
<td>
	<property>Tag</property>
</td>
<td class="desc">
    <p>A general purpose, multi-line text field that can be used to store notes or parameter data.</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.TargetDepth.html" -->
<tr>
<td>
	<property>Target Depth</property>
</td>
<td>
    <p>The Z coordinate of the final machining depth.</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.ToolDiameter.html" -->
<tr>
<td>
	<property>Tool Diameter</property>
</td>
<td>
    <p>This is the diameter of the current tool in drawing units.</p>
    <p>
        If the tool diameter is 0, the diameter from the tool information stored in the tool library
        for the given tool number will be used.
    </p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.ToolNumber.html" -->
<tr>
<td>
	<property>Tool Number</property>
</td>
<td class="desc">
    <p>The ToolNumber is used to identify the current tool.</p>
    <p>
        If ToolNumber changes between successive machine ops a toolchange instruction is created in gcode.
        ToolNumber=0 is a special case which will not issue a toolchange.
    </p>
    <p>The tool number is also used to look up tool information in the current tool library.  The tool library is specified
	in the containing Part, or if this is not present in the Machining folder level.  If no tool library is defined the
	Default-(units) tool library is assumed.</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.ToolProfile.html" -->
<tr>
<td>
	<property>Tool Profile</property>
</td>
<td>
    <p>The shape of the cutter</p>
    <p>
        If the tool profile is Unspecified, the profile from the tool information stored in the tool library
        for the given tool number will be used.
    </p>
    <p>
        <value>EndMill</value> | <value>BullNose</value> | <value>BallNose</value> | <value>Vcutter</value> | <value>Drill</value> | <value>Lathe</value>
    </p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.Transform.html" -->
<tr>
<td>
	<property>Transform</property>
</td>
<td>
    <p>Used to transform the toolpath.</p>
	<div class="warning"><b>Warning!</b> This property is experimental and may give unpredictable results.</div>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.VelocityMode.html" -->
<tr>
<td>
	<property>Velocity Mode</property>
</td>
<td>
    <p>Instructs the gcode interpreter whether or to use look ahead smoothing.</p>
    <p>
        <value>Constant Velocity</value> - (G64) Smoother but less accurate.<br>
        <value>Exact Stop</value> - (G61) All control points are hit but movement may be slower and jerky.<br>
        <value>Default</value> - Uses the global VelocityMode value under machining options.
    </p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.WorkPlane.html" -->
<tr>
<td>
	<property>Work Plane</property>
</td>
<td>
    <p>
        Used to define the gcode workplane.  Arc moves are defined within this plane.<br>
        Options are <value>XY</value> | <value>XZ</value> | <value>YZ</value>
    </p>
</td>
</tr>
<!-- $include.end -->
</table>
