﻿"""Remove utf-8 BOM marker from files as Jekyll cannot handle this!
"""
import fnmatch
import os
import codecs

def process_file( srcfile, destfile ):
    print "processing " + srcfile    
    ftext = ""

    l = 0
    
    with open(srcfile, 'U') as f:
        for line in f:
            l += 1

            if l == 1:
                if line.startswith("# "):
                    ftext += "---\n"
                    ftext += "title: " + line[2:]
                    ftext += "---\n"
                else:
                    return

            ftext += line
 
    with open(destfile, "w") as f:
        f.write(ftext)

filepattern = "*.md"
# filepattern = "test.md"

for root, dirnames, filenames in os.walk('..\en'):
    for filename in fnmatch.filter(filenames, filepattern):
        srcfile = os.path.join(root, filename)
        destfile = srcfile
        process_file( srcfile, destfile )
        