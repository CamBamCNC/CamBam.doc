---
title: Tutorial - 3D Profile - back face machining
---
# Tutorial: 3D Profile - back face machining

This tutorial explains some more advanced concepts of the [3D profiling operation](../cam/3d.md){:.name} and covers:

- Back face machining.
- 3D Holding Tabs.

## Back face machining.

Back face machining is very similar to the front face roughing and finishing passes, with a few extra parameters
to control the back face machining behaviour.

The front and back faces may be machined in a single piece of stock, by flipping the stock over after the font face
has been machined.  Alternatively the front and back faces may be machined in separate pieces of stock which can
then be fixed together.

The *Back Face Zero Z*{:.prop} parameter is a key concept to understand.  The 3D model is in effect, flipped over to machine the reverse side.
*Back Face Zero Z*{:.prop} determines the current Z coordinate that will become Z=0 when the model is flipped.

Referencing Z=0 to the machine's work surface and setting a positive stock surface value will result in the model being rotated
about Z=0.  In this case *Back Face Zero Z*{:.prop} should be set to 0.

If the top of the stock is referenced as the machine Z=0, <property>Back Face Zero Z</property> would be the deepest Z coordinate of the model.
When the model is flipped, this point will then be at or ideally just below the stock surface (Z=0) plane.

### Basic Properties

> **Hint:** If a <span class="name">Part</span> has already been created containing the roughing and finishing passes
for the front face, creating the back face can be simplified by copying and pasting the <span class="name">Part</span> used by the front face and then
altering the properties specific to back face machining.
{:.note}

<table border="0" width="100%" cellpadding="4px">
    <thead>
    <tr>
        <th align="left" width="25%">Property</th>
        <th align="left" width="20%">Value</th>
        <th align="left" width="55%">Notes</th>
    </tr>
    </thead>
    <tbody>
	<tr>
	<td>
		<property>Back Face</property>
	</td>
	<td>
		<value>True</value>
	</td>
	</tr>
	<tr>
	<td>
		<property>Back Face Zero Z</property>
	</td>
	<td>
		0
	</td>
	<td>
		<p>In this example, the table surface Z=0 is used, so the model is rotated about Z=0 to machine the back face.</p>
	</td>
	</tr>
    <tr>
		<td></td>
		<td>-100</td>
        <td>
            <p>In this example, the stock surface Z=0 is used, the model is around 100 units tall and is aligned so that the highest
			Z point is at or just below the stock surface (Z=0).</p>
		</td>
	</tr>
    <tr>
	<td>
		<property>Flip Axis</property>
	</td>
	<td>
		<value>X</value>
	</td>
    <td>
		<p>The stock will be rotated around the X axis (top to bottom), when the back face is to be machined.</p>
	</td>
	</tr>
	<tr>
	<td></td>
	<td>
		<value>Y</value>
	</td>
    <td>
		<p>The stock will be rotated around the Y axis (left to right), when the back face is to be machined.</p>
	</td>
	</tr>
	</tbody>
</table>

The back face toolpaths will be displayed in the orientation they will be machined, which may overlay the top face of the surface.  
Hiding the drawing layer containing the 3D surface, or setting wireframe view mode makes viewing the toolpaths easier.

{:align="center"}
[![Back face roughing toolpaths](../images/tutorial.3dback-1-t.png){:.icenter}](../images/tutorial.3dback-1.png)  
Back face roughing toolpaths

{:align="center"}
[![Back face finishing toolpaths (with wireframe view)](../images/tutorial.3dback-2-t.png){:.icenter}](../images/tutorial.3dback-2.png)  
Back face finishing toolpaths (with wireframe view)

## 3D Holding Tabs.

There is currently no automatic 3D holding tabs functionality, but this feature is planned for a future release.

Here is a method to manually create 3D holding tabs or sprues using cylinder meshes.

### Extrude a circle

![](../images/hide.png){:align="right" .iright}
Hide the drawing layer containing the 3D mesh.

Create a new layer to contain the holding cylinders ('tabs').  
![](../images/tutorial.3dback-newlayer.png)

Draw a 2D circle with a diameter of the holding tabs to be used.  Place the center of the circle at the drawing origin (0,0).

![](../images/tube.png){:align="right" .iright width="300" height="310"}

With the circle selected, select <span class="cmd">Draw - Surface - Extrude</span>.  Enter an extrusion height large enough to span the largest width
of the model plus an extra margin to allow for tool diameters.  
You will also be prompted for the `Arc Expand Tolerance`.  Curves will be converted to flat facets when extruding.  This tolerance setting
controls how much error or deviation is allowed from the curve (measured in drawing units).

Rotating the drawing view should show a 3D cylinder extending in the positive Z direction.

### Position the cylinder

First, center the cylinder (<span class="cmd">Transform - Center (Extents)</span>).

Use a combination of cut and paste and transformation rotations to position the cylinders at required positions around the model.

![](../images/3d_tab1.png){:width="400" height="320"}

### Adjust the machining boundary

The holding tab shapes need to be added to the 3D profiles list of surfaces to machine.  To do this, right click on the machining
operation in the drawing tree and select <span class="cmd">Select Drawing Objects</span>.  <span class="key">Ctrl</span>+click to select the extruded cylinders.

To prevent the machining operation machining around the ends of the cylinders, we need to reduce the boundary shape.  
This is achieved by specifying <value>Selected Shapes</value> in the <property>Boundary Method</property> property and selecting only the main surface object,
excluding any holding tab cylinders.

<table border="0" width="100%" cellpadding="4px">
    <thead>
    <tr>
        <th align="left" width="25%">Property</th>
        <th align="left" width="20%">Value</th>
        <th align="left" width="55%">Notes</th>
    </tr>
    </thead>
    <tbody>
	<tr>
	<td>
		<property>Boundary Method</property>
	</td>
	<td>
		<value>Selected Shapes</value>
	</td>
    <td>
		<p>This will create a trimming boundary just from selected shapes.</p>
	</td>
	</tr>
	<tr>
	<td>
		<property>Boundary Shape IDs</property>
	</td>
	<td>
		1
	</td>
    <td>
		<p>Enter the ID of the main 3D surface to machine, excluding any holding cylinders.<br>
        The [...] button to the right of the property can be used to select the shapes.</p>
	</td>
	</tr>
    </tbody>
</table>

{:align="center"}
[![Manual 3D holding tabs](../images/tutorial.3dback-3-t.png){:.icenter}](../images/tutorial.3dback-3.png)  
Front roughing toolpath with manual holding tabs.
