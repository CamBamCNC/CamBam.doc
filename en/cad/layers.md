---
title: Layers
---
# Layers

Drawing objects can be organised within multiple, color coded, layers.

Layers (and the drawing objects contained within them), can be made hidden or visible, which can greatly simplify
working on complicated drawings.

Drawing objects can be moved between layers using cut, copy and paste, or by simply dragging and dropping them
within the drawing tree view.

Selecting a layer in the drawing tree allows its properties to be altered in the property grid.
The appearance of the layers, such as colour and line widths, can be set in this way.

![](../images/layer_01.png){:.icenter} 

![](../images/layer_02.png){:align="right" .iright}
 
When drawing new shapes, they will be inserted into the layer marked as the <i>Active</i> layer, which is indicated in the
drawing tree by a small green arrow icon.  The active layer is set by right clicking a layer in the drawing tree, then
selecting <span class="cmd">Set as active layer</span>.

> **Note:** It is possible for the active layer to also be hidden, so new drawing objects will be inserted into the layer,
but not displayed until the layer is marked as visible again.
{:.note}

Other operations for manipulating layers are available from the context menu, visible when right clicking on a layer.

![](../images/layer_04.png){:.icenter}

<br/><span class="new">[New! 1.0]</span>

A layer can now be locked. In this case, the objects it contains can not be selected from the drawing area (but remain selectable in the list)

When a layer is locked, a small padlock appears on its icon.![](../images/layer_08.png)

To lock a layer, once selected in the list, use <span class="key">CTRL + Spacebar</span>, or toggle its <property>Locked</property> property to <value>True</value> in the property grid.

![](../images/layer_07.png){:.icenter}

## Layer Operations

<span class="cmd">New layer</span>  
Creates a new layer and also makes this the <i>active layer</i>.  The default color of new layers can be changed
in the [Default Layer Color](../configuration.md#DefaultLayerColor){:.prop} property of the system configuration settings.

<span class="cmd">Set as active layer</span>  
New drawing objects will be inserted into the current active layer.

<span class="cmd">Hide</span>  
The selected layer is marked as hidden and the drawing objects will not be displayed in the drawing view.
These objects will also be prevented from being selected using operations such as <span class="cmd">Select All</span> (<span class="key">CTRL+A</span>).
Hidden layers will be displayed greyed in the drawing tree.

Layers can be quickly toggled between visible and hidden by selecting them in the drawing tree view then pressing the <span class="key">SPACE</span> key.

<span class="cmd">Hide all but this</span>  
Will hide all layers in the drawing, apart from the selected one.

<span class="cmd">Show</span>  
Makes the selected layer and drawing objects visible.

<span class="cmd">Show all</span>  
Makes sure all the layers in the drawing are marked visible.

<span class="cmd">Clear</span>  
This operation will delete all the drawing objects contained in the selected layer.

<span class="cmd">Select all on layer</span>  
Selects all the drawing objects on the selected layer.

<span class="cmd">Cut / Copy / Paste</span>  
Cut / Copy and Paste selected layers and all their drawing objects.

<span class="cmd">Delete</span>  
Removes a selected layer and contained drawing objects from the drawing.

<span class="cmd">Rename</span>  
Rename the selected layer.  Layers can also be renamed by selecting them in the drawing tree and pressing <span class="key">F2</span>, or by
a slow double click on the layer name.
 
## Properties

![](../images/layer_05.png){:.icenter}

<table class="prop" cellspacing="0" width="100%">
<tr>
<td>
	<property>Alpha</property>
</td>
<td>
	<p>The level of transparency of the drawing objects in the layer.  0 to 1, 1=opaque, 0 = completely transparent.</p>
</td>
</tr>
<tr>
<td>
	<property>Color</property>
</td>
<td>
	<p>Color used to display the drawing objects.</p>
</td>
</tr>
<tr>
<td>
	<property>Locked</property> 
	<br/><span class="new">[New! 1.0]</span>
</td>
<td>
	<p>Locked layers prevent visible objects from being selected.</p>
</td>
</tr>
<tr>
<td>
	<property>Name</property>
</td>
<td>
	<p>The name of the layer.</p>
</td>
</tr>
<tr>
<td>
	<property>Number of Entities</property>
	<br/><span class="new">[New! 1.0]</span>
</td>
<td>
	<p>The number of entities in this layer.</p>
</td>
</tr>
<tr>
<td>
	<property>Pen Width</property>
</td>
<td>
	<p>Thickness of the drawing lines.</p>
</td>
</tr>

<tr>
<td>
	<property>Script</property>
	<br/><span class="new">[New! 1.0]</span>
</td>
<td>
	<p>This script is included into all child script entities.</p>
</td>
</tr>

<tr>
<td>
	<property>Tag</property>
</td>
<td class="desc">
    <p>A general purpose, multi-line text field that can be used to store notes or parameter data.</p>
</td>
</tr>
<!-- $include.end -->
<tr>
<td>
	<property>Visible</property>
</td>
<td>
	<p>The state of the layer visibility: <value>True</value>=Visible, <value>False</value>=Hidden.</p>
</td>
</tr>
</table>

## Moving drawing objects between layers.

You can move the drawing objects from one layer to another by simply dragging and dropping with the left mouse button.


![](../images/layer_06.png){:align="right" .iright}

You can cut, copy and paste the drawing objects between layers using the context menu for each drawing object in the tree or the context menu of the drawing area, 
or through the main Edit menu.

NOTE: The paste function will behave differently depending on which context menu it was called from:

From the context menu of the layer:  
The object will be pasted into the layer that the context menu was opened.

From the context menu of the drawing view of the main Edit menu:  
The object will be pasted into the selected layer, if none is selected, it will be pasted into the current active layer.
