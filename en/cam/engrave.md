---
title: ![icon](../img/icon/cam_engrave32.png) Engraving Machining Operation
---
# ![icon](../img/icon/cam_engrave32.png) Engraving Machining Operation

Engraving machining operations 'follow' their selected shapes, including Z movements.

## Properties
<table class="prop" cellspacing="0" width="100%">
<!-- $include file="../en/_prop/mop.ClearancePlane.html" -->
<tr>
<td>
	<property>Clearance Plane</property>
</td>
<td>
    <p>The clearance plane (offset from the work plane).</p>
    <p>The clearance plane should be clear of the stock and any holding devices to allow free movement to any location.</p>
</td>
</tr><!-- $include.end -->
<!-- $include file="../en/_prop/mop.CustomMOPFooter.html" -->
<tr>
<td>
	<property>Custom MOP Footer</property>
</td>
<td>
    <p>A multi-line gcode script that will be inserted into the gcode post after the current machining operation.</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.CustomMOPHeader.html" -->
<tr>
<td>
	<property>Custom MOP Header</property>
</td>
<td>
    <p>A multi-line gcode script that will be inserted into the gcode post before the current machining operation.</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.CutFeedrate.html" -->
<tr>
<td>
	<property>Cut Feedrate</property>
</td>
<td>
    <p>The feed rate to use when cutting.</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/engrave.DepthIncrement.html" -->
<tr>
<td>
	<property>Depth Increment</property>
	<br /><span class="new">[New! 0.9.8]</span>
</td>
<td>
    <p>Depth increment of each machining pass. Determines the number of passes to reach the final target depth.</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.Enabled.html" -->
<tr>
<td>
	<property>Enabled</property>
</td>
<td>
    <p>
        <value>True</value>: The toolpaths associated with this machining operation are displayed and included in the gcode output<br>
        <value>False</value>: The operation will be ignored and no gcode or tool paths will be produced for this operation.
    </p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.FinalDepthIncrement.html" -->
<tr>
<td>
	<property>Final Depth Increment</property>
</td>
<td>
    <p>The depth increment of the final machining pass.</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.MaxCrossoverDistance.html" -->
<tr>
<td>
	<property>Max Crossover Distance</property>
</td>
<td>
<p>Maximum distance as a fraction (0-1) of the tool diameter to cut in horizontal transitions.</p>
<p>If the distance to the next toolpath exceeds MaxCrossoverDistance, a retract, rapid and plunge to the next position, via the clearance plane, is inserted.</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.Name.html" -->
<tr>
<td>
	<property>Name</property>
</td>
<td>
    <p>
        Each machine operation can be given a meaningful name or description.<br>
        This is output in the gcode as a comment and is useful for keeping track of the function of each machining operation.
    </p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.OptimisationMode.html" -->
<tr>
<td>
	<property>Optimisation Mode</property>
</td>
<td>
    <p>An option that controls how the toolpaths are ordered in gcode output.</p>
    <p>
        <value>New (0.9.8)</value> - A new, improved optimiser currently in testing.<br>
        <value>Legacy (0.9.7)</value> - Toolpaths are ordered using same logic as version 0.9.7.<br>
        <value>None</value> - Toolpaths are not optimised and are written in the order they were generated.
    </p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.PlungeFeedrate.html" -->
<tr>
<td>
	<property>Plunge Feedrate</property>
</td>
<td>
    <p>The feed rate to use when plunging.</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.PrimitiveIds.html" -->
<tr>
<td>
	<property>Primitive IDs</property>
</td>
<td>
    <p>List of drawing objects from which this machine operation is defined.</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.RoughingFinishing.html" -->
<tr>
<td>
	<property>Roughing / Finishing</property>
</td>
<td>
    <p>
        Currently only supported by <span class="name">3D Profile</span> and <span class="name">Lathe</span> machining operations.
    </p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.RoughingClearance.html" -->
<tr>
<td>
	<property>Roughing Clearance</property>
</td>
<td>
    <p>This is the amount of stock to leave after the final cut.</p>
    <p>Remaining stock is typically removed later in a finishing pass.</p>
    <p>Negative values can be used to oversize cuts.</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.SpindleDirection.html" -->
<tr>
<td>
	<property>Spindle Direction</property>
</td>
<td>
    <p>The direction of rotation of the spindle.</p>
    <p><value>CW</value> | <value>CCW</value> | <value>Off</value></p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.SpindleRange.html" -->
<tr>
<td>
	<property>Spindle Range</property>
</td>
<td>
    <p>The pulley number or dial setting of the spindle for the target speed.</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.SpindleSpeed.html" -->
<tr>
<td>
	<property>Spindle Speed</property>
</td>
<td>
    <p>The speed in RPM of the spindle.</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.StartPoint.html" -->
<tr>
<td>
	<property>Start Point</property>
</td>
<td>
    <p>
        Used to select a point, near to where the first toolpath should begin machining.<br />
        If a start point is defined, a small circle will be displayed at this point when the machining operation
        is selected.  The start point circle can be moved by clicking and dragging.
    </p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.StockSurface.html" -->
<tr>
<td>
	<property>Stock Surface</property>
</td>
<td>
    <p>This is the Z offset of the stock surface at which to start machining.</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.Style.html" -->
<tr>
<td>
	<property>Style</property>
	<br /><span class="new">[New! 0.9.8]</span>
</td>
<td>
<p markdown="1">
Select a [CAM Style](cam-style.md){:.name} for this machining operation.
All default parameters will be inherited from this style.
</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.Tag.html" -->
<tr>
<td>
	<property>Tag</property>
</td>
<td class="desc">
    <p>A general purpose, multi-line text field that can be used to store notes or parameter data.</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/engrave.TargetDepth.html" -->
<tr>
<td>
	<property>Target Depth</property>
</td>
<td>
    <p>The Z coordinate of the final machining depth.</p>

	<div class="warning">For engraving operations, the Z coordinate of the source drawing object point will also be added
	to the toolpath so that the engraving toolpath can 'follow' the shape's Z contour.</div>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.ToolDiameter.html" -->
<tr>
<td>
	<property>Tool Diameter</property>
</td>
<td>
    <p>This is the diameter of the current tool in drawing units.</p>
    <p>
        If the tool diameter is 0, the diameter from the tool information stored in the tool library
        for the given tool number will be used.
    </p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.ToolNumber.html" -->
<tr>
<td>
	<property>Tool Number</property>
</td>
<td class="desc">
    <p>The ToolNumber is used to identify the current tool.</p>
    <p>
        If ToolNumber changes between successive machine ops a toolchange instruction is created in gcode.
        ToolNumber=0 is a special case which will not issue a toolchange.
    </p>
    <p>The tool number is also used to look up tool information in the current tool library.  The tool library is specified
	in the containing Part, or if this is not present in the Machining folder level.  If no tool library is defined the
	Default-(units) tool library is assumed.</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.ToolProfile.html" -->
<tr>
<td>
	<property>Tool Profile</property>
</td>
<td>
    <p>The shape of the cutter</p>
    <p>
        If the tool profile is Unspecified, the profile from the tool information stored in the tool library
        for the given tool number will be used.
    </p>
    <p>
        <value>EndMill</value> | <value>BullNose</value> | <value>BallNose</value> | <value>Vcutter</value> | <value>Drill</value> | <value>Lathe</value>
    </p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.Transform.html" -->
<tr>
<td>
	<property>Transform</property>
</td>
<td>
    <p>Used to transform the toolpath.</p>
	<div class="warning"><b>Warning!</b> This property is experimental and may give unpredictable results.</div>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.VelocityMode.html" -->
<tr>
<td>
	<property>Velocity Mode</property>
</td>
<td>
    <p>Instructs the gcode interpreter whether or to use look ahead smoothing.</p>
    <p>
        <value>Constant Velocity</value> - (G64) Smoother but less accurate.<br>
        <value>Exact Stop</value> - (G61) All control points are hit but movement may be slower and jerky.<br>
        <value>Default</value> - Uses the global VelocityMode value under machining options.
    </p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.WorkPlane.html" -->
<tr>
<td>
	<property>Work Plane</property>
</td>
<td>
    <p>
        Used to define the gcode workplane.  Arc moves are defined within this plane.<br>
        Options are <value>XY</value> | <value>XZ</value> | <value>YZ</value>
    </p>
</td>
</tr>
<!-- $include.end -->

</table>
