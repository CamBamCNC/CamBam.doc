---
title: Configuration
---
# Configuration

## Tools - Options

<table class="prop" cellspacing="0" width="100%" markdown="1">

<tr>
<td>
	<property><a name="ArcDisplayDegrees">Arc Display Degrees</a></property>
	<br /><span class="new">New [0.9.8f]</span>
</td>
<td>
	<p>Arcs are displayed using multiple line segments. This setting defines the angle between each segment. Smaller numbers make smoother curves but slower display rates.</p>
</td>
</tr>

<tr>
<td>
	<property><a name="ArcFitTolerance">Arc Fit Tolerance</a></property>
	<br /><span class="new">New [0.9.8N]</span>
</td>
<td>
	<p>The tolerance used when automatic arc fitting is applied in some drawing operations.</p>
	<p>Zero will use an automatically calculated value.</p>
</td>
</tr>

<tr>
<td>
	<property><a name="AutoApplyTransformations">Auto-Apply Transformations</a></property>
	<br /><span class="new">New [0.9.8]</span>
</td>
<td>
	<p>If <value>True</value>, transformations such as rotations, moving, resizing and array copies will automatically apply transformations and reset the transformation matrix.</p>
	<p>In some cases, such as rotating a circle around the Y or X axis, this is not possible so the original entity plus transformation matrix is retained.</p>
</td>
</tr>

<tr>
<td>
	<property><a name="AutoArcFitting">Auto Arc Fitting</a></property>
	<br /><span class="new">New [0.9.8N]</span>
</td>
<td>
	<p>Whether to apply arc fitting in certain drawing operations (such as plane slicing).</p>
</td>
</tr>
<tr>
<td>
	<property><a name="BackfaceCulling">Backface Culling</a></property>
</td>
<td>
	<p>When displaying surface meshes, faces with back facing normals (using right hand rule) are not displayed.</p>
	<p>This can speed up displaying meshes considerably and also make the wireframe 3D view clearer.</p>
</td>
</tr>
<tr>
<td>
	<property><a name="CheckVersionAtStart">Check Version At Start</a></property>
</td>
<td>
	<p><value>True</value> | <value>False</value></p>
	<p>If <value>True</value>, the program will check for updates from the internet when it loads.</p>
	<p>Set this option <value>False</value> if you are not connected to the internet.</p>
	<p>The version check only downloads a tiny text file from the CamBam web site containing the latest version number. No other information is transferred.</p>
</td>
</tr>
<tr>
<td>
	<property><a name="CutWidthColor">Cut Width Color</a></property>
</td>
<td>
	<p>The color used to display toolpath cut widths.</p>
</td>
</tr>
<tr>
<td>
	<property><a name="DefaultFontFamily">Default Font Family</a></property>
</td>
<td>
	<p>This is the font used when no font is specified for text drawing objects.</p>
</td>
</tr>
<tr>
<td>
	<property><a name="DefaultGCodeExtension">Default GCode Extension</a></property>
</td>
<td>
	<p>A default file extension used when gcode files are produced.</p>
</td>
</tr>
<tr>
<td>
	<property><a name="DefaultLayerColor">Default Layer Color</a></property>
</td>
<td>
	<p>The color to use when new layers are inserted into a drawing.</p>
</td>
</tr>
<tr>
<td>
	<property><a name="DefaultScriptEntity">Default Script Entity</a></property>
</td>
<td markdown="1">
The default script to insert whenever [Script Objects](cad/script-object.md){:.name} are inserted..
</td>
</tr>
<tr>
<td>
	<property><a name="DefaultStockColor">Default Stock Color</a></property>
</td>
<td>
	<p>The default color to use to display stock objects.</p>
</td>
</tr>
<tr>
<td>
	<property><a name="DiagnosticLevel">Diagnostic Level</a></property>
</td>
<td>
	<p>An integer number used to control the number of information messages displayed in the message pane at the bottom of 
	the CamBam interface.  Typical values are 0 to 4, where 0 displays little or no messages and 4 displays reams of diagnostic information.</p>
</td>
</tr>
<tr>
<td>
	<property><a name="DisplayMode">Display Mode</a></property>
</td>
<td>
	<p>Controls the method used to display the 3D drawing view.</p>
	<p><value>OpenGL</value> is a fast, preferred method but may cause problems with some graphics drivers.</p>
	<p><value>OpenGL_Legacy</value> uses immediate mode which was the standard OpenGL method up to 0.9.8M.</p>
	<p><value>GDI</value> is a slower but potentially less susceptible to driver problems.  Use this mode if the drawing display seems very slow or corrupted.</p>
	<p>Changing the DisplayMode option requires CamBam to be restarted.</p>
	<p>Holding the <span class="key">SHIFT</span> key while starting CamBam will force the use of <value>GDI</value> mode.</p>
</td>
</tr>
<tr>
<td>
	<property><a name="DrawingTemplate">Drawing Template</a></property>
</td>
<td>
<p>This property can contain the filename of a CamBam drawing (.cb file) to be used as a template for new drawings.</p>
<p>Whenever a new drawing is created, or a non CamBam (such as DXF, 3DS etc) is loaded, the basic format and properties of the 
drawing template will be used for that document.</p>
<p markdown="1">This is useful for setting default values for properties stored in documents, such as <span class="name">[Post Processors](cam/post-processor.md)</span>.</p>
</td>
</tr>
<tr>
<td>
	<property><a name="DrawingUnits">Drawing Units</a></property>
</td>
<td>
	<p>This sets the drawing units to be used for new drawings.</p>
	<p>This property may be overridden by the drawing units of the <property>Drawing Template</property>, if one is supplied.</p>
</td>
</tr>
<tr>
<td>
	<property><a name="FileBackups">File Backups</a></property>
	<br /><span class="new">New [0.9.8]</span>
</td>
<td>
	<p>When saving CamBam (.cb), library or post processor files, a backup of the existing file is created before overwriting.</p>
	<p>The backup file is of the format 'filename.b#', where # is a number.  The number of backups to keep is specified in the <property>File Backups</property> property.</p>
</td>
</tr>
<tr>
<td>
	<property><a name="GCodeEditor">GCode Editor</a></property>
	<br /><span class="new">New [0.9.8]</span>
</td>
<td>
	<p>Specify an external command used to edit gcode files.  If no command is specified, the internal editor is used.</p>
	<p>GCode files can be edited by invoking the Machining, <span class="cmd">Edit gcode</span> menu option.</p>
	<p>Example:<br>
	%windir%\system32\notepad.exe.</p>
</td>
</tr>
<tr>
<td>
	<property><a name="GerberFlatten">Gerber - Flatten</a></property>
	<br /><span class="new">New [0.9.8k]</span>
</td>
<td>
	<p>If <value>True</value>, flatten all layers to a single (unioned) layer.</p>
</td>
</tr>
<tr>
<td>
	<property><a name="GerberSubtractLayers">Gerber - Subtract Layers</a></property>
	<br /><span class="new">New [0.9.8k]</span>
</td>
<td>
	<p>If <value>True</value>, '<i>Clear</i>' layers will be subtracted from previous layers.</p>
</td>
</tr>
<tr>
<td>
	<property><a name="GerberUnionLayers">Gerber - Union Layers</a></property>
	<br /><span class="new">New [0.9.8k]</span>
</td>
<td>
	<p>If <value>True</value>, all shapes on each layer will be unioned together.</p>
</td>
</tr>
<tr>
<td>
	<property><a name="GerberUnionTraces">Gerber - Union Traces</a></property>
	<br /><span class="new">New [0.9.8k]</span>
</td>
<td>
	<p>If <value>True</value>, each trace will be unioned together.  If <value>False</value> the traces will be left as line and arc sections.</p>
</td>
</tr>
<tr>
<td>
	<property><a name="GLSLShaderVersion">GLSL Shader Version</a></property>
	<br /><span class="new">New [0.9.8N]</span>
</td>
<td>
	<p>The version of the OpenGL shader language to use.</p>
	<p>110 is older but should be more compatible with older display drivers.</p>
	<p>330 may only be supported by newer display drivers.</p>
</td>
</tr>
<tr>
<td>
	<property><a name="GridColor">Grid Color</a></property>
</td>
<td>
	<p>The color of the drawing grid.</p>
</td>
</tr>
<tr>
<td>
	<property><a name="GridInfoInches">Grid Info (Inches)</a></property>
</td>
<td>
	<p>Information that defines the drawing grid when Inches drawing units are used.</p>
	<ul>
	<li><property>Drawing Units</property> - drawing units used by the grid.</li>
	<li><property>Minimum</property> - X, Y location of the lower left point of the visible grid.</li>
	<li><property>Maximum</property> - X, Y location of the upper right point of the visible grid.</li>
	<li><property>Major Scale</property> - Number of units in the grid's major scale.</li>
	<li><property>Minor Scale</property> - Number of units in the grid's minor scale.</li>
	</ul>
</td>
</tr>
<tr>
<td>
	<property><a name="GridInfoMetric">Grid Info (Metric)</a></property>
</td>
<td>
	<p>Information that defines the drawing grid when Metric drawing units are used.</p>
</td>
</tr>
<tr>
<td>
	<property><a name="HoldingTabDragToolpathRefresh">Holding Tab Drag Toolpath Refresh</a></property>
	<br /><span class="new">New [0.9.8i]</span>
</td>
<td>
	<p><value>True</value> | <value>False</value></p>
    <p>If <value>True</value>, holding tabs are automatically applied to toolpaths when tabs are moved.<br />
	If <value>False</value> tabs will be applied when the toolpaths are regenerated.</p>
</td>
</tr>
<tr>
<td>
	<property><a name="Language">Language</a></property>
	<br /><span class="new">New [0.9.8k]</span>
</td>
<td>
	<p>The desired language to use for the CamBam user interface.</p>
	<p>CamBam will need to be restarted for this change to take effect.</p>
	<p>Language translation files will need to be downloaded from the internet for the translations to function.</p>
	<p>The translation files may be periodically updated.  Use the <span class="cmd">Tools - Download latest translations</span> menu item
	to download the latest versions from the CamBam website.</p>
<p markdown="1">See [www.cambam.info/ref/ref.lang](http://www.cambam.info/ref/ref.lang) for more details.</p>
</td>
</tr>
<tr>
<td>
	<property><a name="OffsetBacktrackCheck">Offset Backtrack Check</a></property>
	<br /><span class="new">New [0.9.8f]</span>
</td>
<td>
	<p>If <value>True</value>, back track drawing glitches in polylines are detected and removed by the offset routine used in toolpath generation.</p>
	<p>Back tracks can cause the offset routines to produce unexpected results.</p>
</td>
</tr>
<tr>
<td>
	<property><a name="RepeatCommands">Repeat Commands</a></property>
</td>
<td>
	<p>If <value>True</value>, drawing commands will be repeated.  To end the current drawing mode, press <span class="key">ESC</span> or
	click the middle mouse button.</p>
</td>
</tr>
<tr>
<td>
	<property><a name="RotationMode">Rotation Mode</a></property>
</td>
<td>
	<p><value>ALT+Left</value> | <value>Left+Middle</value> | <value>Left+Right</value></p>
	<p>The key and mouse combination used to rotate the drawing view.</p>
	<p><value>ALT+Left</value> - the view is rotated by holding down the <span class="key">ALT</span> key and dragging with the left mouse button.</p>
	<p><value>Left+Middle</value> - the view is rotated by pressing the middle mouse button and dragging with the left mouse button.  The middle mouse button can be released while dragging.</p>
	<p><value>Left+Right</value> <span class="new">New [0.9.8]</span> - the view is rotated by pressing the right then left mouse button and dragging.  The right mouse button can be released while dragging.</p>
</td>
</tr>
<tr>
<td>
	<property><a name="SelectColor">Select Color</a></property>
</td>
<td>
	<p>The color used to select paint selected shapes.</p>
</td>
</tr>
<tr>
<td>
	<property><a name="SelectedEntityFocus">Selected Entity Focus</a></property>
    <br /><span class="new">New [1.0rc3]</span>
</td>
<td>
	<p>If <value>True</value>, when selecting a drawing object, the drawing tree layer is expanded and scrolled to display the item.</p>
    <p><value>ALT+TAB</value> can also be used to toggle the focus of selected drawing objects in the drawing tree.<p>
</td>
</tr>
<tr>
<td>
	<property><a name="SelectFade">Select Fade</a></property>
</td>
<td>
	<p>Controls how much unselected shapes are faded (as a percent).</p>
</td>
</tr>
<tr>
<td>
	<property><a name="ShowGrid">Show Grid</a></property>
</td>
<td>
	<p>Sets whether the drawing grid is displayed.</p>
	<p>Alternatively use the show grid button on the toolbar.</p>
</td>
</tr>
<tr>
<td>
	<property><a name="SnaptoGrid">Snap to Grid</a></property>
</td>
<td>
	<p><value>True</value> | <value>False</value></p>
	<p>If <value>True</value>, drawing points will snap to the minor grid units.</p>
	<p>This option can also be changed from the <span class="cmd">View - Snap to Grid</span> menu option or toggled using <span class="key">Ctrl+G</span>.</p>
</td>
</tr>
<tr>
<td>
	<property><a name="SnaptoPoints">Snap to Points</a></property>
</td>
<td>
	<p><value>True</value> | <value>False</value></p>
	<p>If <value>True</value>, drawing points will snap to shape control points, circle centers and other significant points.</p>
</td>
</tr>
<tr>
<td>
	<property><a name="SplineCurveSteps">Spline Curve Steps</a></property>
</td>
<td>
	<p>When splines are displayed, their shape is approximated by line segments.  This setting controls the number
	of segments used to display.  A larger number will give a smoother appearance but may slow display performance.</p>
	<p>This setting does not affect the resolution of geometric operations based on splines, such as toolpath generation.</p>
</td>
</tr>
<tr>
<td>
	<property><a name="SplinetoPolylineTolerance">Spline to Polyline Tolerance</a></property>
</td>
<td>
	<p>Splines are converted to polylines internally before they can be used for some operations, such as toolpath generation.</p>
	<p>This setting controls the degree of error allowed in this conversion, measured in drawing units.</p>
	<p>A smaller value will result in more accurate spline conversions but can hinder performance considerably.</p>
</td>
</tr>
<tr>
<td>
	<property><a name="SystemPath">System Path</a></property>
	<br /><span class="new">New [0.9.8f]</span>
</td>
<td>
<p>The system path is the root folder where CamBam library (styles, tools etc), post processor and drawing templates are stored.</p>
<p>The following macros can be used:</p>
<p>	
<value>{$common}</value> - Common application data folder (%ALLUSERPROFILE%).<br/>
In Windows XP this is typically:<br/>  
\Documents and Settings\All Users\Application Data\CamBam plus 0.9.8\<br/>
And in Windows 7<br/>
\ProgramData\CamBam plus 0.9.8\<br/>
</p>
<p>
<value>{$user}</value> - User application data folder (%USERPROFILE%).
</p>
</td>
</tr>

<tr>
<td>
	<property><a name="TextCurveTolerance">Text Curve Tolerance</a></property>
</td>
<td>
	<p>Text objects are converted to polylines internally before they can be used for some operations, such as toolpath generation.</p>
	<p>This setting controls the degree of error allowed in this conversion, measured in font units (0-2048).</p>
	<p>A smaller value will result in more accurate text conversions but can hinder performance considerably.</p>
</td>
</tr>
<tr>
<td>
	<property><a name="ThinkingMessage">Thinking Message</a></property>
</td>
<td>
	<p>Message to display when CamBam is busy calculating.  Displayed in full, unexpurgated technicolor! :-)</p>
</td>
</tr>
<tr>
<td>
	<property><a name="ToolpathArcColor">Toolpath Arc Color</a></property>
</td>
<td>
	<p>The color of arc segments in toolpaths.</p>
</td>
</tr>
<tr>
<td>	
	<property><a name="ToolpathLineColor">Toolpath Line Color</a></property>
</td>
<td>
	<p>The color of line segments in toolpaths.</p>
</td>
</tr>
<tr>
<td>
	<property><a name="ToolpathRapidColor">Toolpath Rapid Color</a></property>
</td>
<td>
	<p>The color used to display toolpath rapids.</p>
</td>
</tr>
<tr>
<td>
	<property><a name="UseSurfaceSelectionColor">Use Surface Selection Color</a></property>
	<br /><span class="new">New [0.9.8N]</span>
</td>
<td>
	<p>If <value>True</value>, selected 3D meshes will be displayed using the selection color.</p>
</td>
</tr>
<tr>
<td>
	<property><a name="View3DWireframe">View 3D Wireframe</a></property>
	<br /><span class="new">New [0.9.8]</span>
</td>
<td>
	<p>If <value>True</value>, 3D meshes will be displayed in wireframe mode.</p>
</td>
</tr>
<tr>
<td>
	<property><a name="ViewBackgroundColor">View Background Color</a></property>
</td>
<td>
	<p>The color of the drawing background.</p>
</td>
</tr>
<tr>
<td>
	<property><a name="ViewFocalLengthScale">View Focal Length Scale</a></property>
	<br /><span class="new">New [0.9.8N]</span>
</td>
<td>
	<p>For <value>Perspective</value> projection mode, drawing objects are set back from the view point by a distance based
	on the height of the view port, multiplied by the <property>View Focal Length Scale</property>.</p>
	<p>Smaller values will give a greater perspective effect but may result in some clipping when viewing larger objects.</p>
</td>
</tr>
<tr>
<td>
	<property><a name="ViewProjectionMode">View Projection Mode</a></property>
	<br /><span class="new">New [0.9.8N]</span>
</td>
<td>
	<p><value>Orthographic</value> - drawing dimensions are consistent regardless of depth.</p>
	<p><value>Perspective</value> - drawing dimensions are perspective scaled based on depth.  Drawing objects are set back from the view point
	by a distance base on <property>View Focal Length Scale</property>.</p>
</td>
</tr>
<tr>
<td>
	<property><a name="ViewTextColor">View Text Color</a></property>
</td>
<td>
	<p>The color used to display text in the drawing view.</p>
</td>
</tr>
<tr>
<td>
	<property><a name="WaterlineSafetyCheck">Waterline Safety Check</a></property>
	<br /><span class="new">New [0.9.8L]</span>
</td>
<td>
	<p>If <value>True</value>, prevents gcode creating if errors are suspected in 3D waterline toolpaths.</p>
</td>
</tr>
<tr>
<td>
	<property><a name="WorkerThreads">Worker Threads</a></property>
</td>
<td>
	<p>Number of simultaneous worker threads to use.</p>
</td>
</tr>
</table>
