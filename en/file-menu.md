---
title: File Menu
---
# File Menu

## File Open ![](images/icone_ouvre_fichier.png)

CamBam can read the following drawing file types:

- CamBam native file format (*.cb)
- Autodesk DXF files - up to AutoCAD 2000 format (*.dxf)
- 3DStudio files (*.3ds)
- Stereo Lithographic 3D meshes (*.stl)
- STEP files (*.stp, *.step) ![](images/New10.png)
- GCode files (*.tap,*.nc, etc)
- Gerber file (*.gbr)

Unrecognised file extensions are presumed to be GCode files.

Use the <span class="cmd">File - Open</span> menu option to open the required file or drag and drop files from Windows Explorer onto 
the CamBam window.

When CamBam is installed, it will be associated with (*.cb) files, so these can be opened by double clicking them from Windows Explorer.

CamBam will also attempt to open any files passed to the application via the command line.

## File New ![](images/icone_new_fichier.png)

Creates a new blank file.

The interface will be reset, the default settings stored in the general configuration will be used.

<!-- TODO: format notes -->

> **Hint**:
If a [Drawing Template](configuration.md#DrawingTemplate.md){:.prop} is defined in the system configuration settings, this file will be used as template for the new drawing.
The drawing template can contain useful default settings such as <property>Post Processor</property>, <property>Fast Plunge Height</property> and <property>Stock</property>,
as well as drawing objects and machining operations.
{:.note}

## New from template

This will create a new drawing, based on an existing CamBam (.cb) file.

Template drawings are typically saved into the <span class="file">templates</span> sub folder of the CamBam system folder.
Use the <span class="cmd">Tools - Browse system folder</span> menu to help find the templates location.

An example template drawing: <span class="file">nameplate.cb</span>, is provided.  This template allows the creation of a nameplate
with raised lettering, commonly used for locomotive name plates.  This template contains all the drawing objects and machining
operations required.  The default text can be quickly changed by double clicking the text object in the drawing view.

Changes made to a drawing based on a template will not affect the template file.  To modify the template file, it will
need to be opened from the template folder using <span class="cmd">File - Open</span>.

<!-- $pagebreak -->

## Save, Save As ![](images/icone_save.png)

Save your work using the menu <span class="cmd">File / Save</span> or <span class="cmd">Save As</span>.

Depending on the value of the [File Backups](configuration.md#FileBackups.md){:.prop} configuration setting, a number of backup files 
may be generated for each file.  These backups are located in the same folder as the saved drawing and will
have extensions such as .b1, .b2 etc. with .b1 being the most recent backup.
 
## Print ![](images/New10.png)

Use this menu to print the drawing area.

The colors and widths of the lines used for printing will be those defined in the <property>Color </property>and <property>Pen Width</property> properties of each layer. The background color will not be printed.

Be careful, with some printers, if  the <property>Pen Width</property> of a layer is 0, the lines of this layer will not be printed.

![](images/print_layer_color-width.png)

The print dialog window allows you to position the drawing on the sheet, change its zoom factor with the mouse or with a pop-up menu, and access printer settings and other options.

![](images/print_scale_EN.png)

<span class="cmd">Pan and zoom</span>: The zoom and pan functions by mouse work in the preview window in the same way as in the drawing area ([see here](basics-rotating-and-panning.md)) so that the area of interest can be positioned in the page; <span class="key">Alt </span>+ double-click in this preview window centers the drawing on the sheet.

At the top left, the zoom factor is displayed in yellow.

The zoom can be changed by turning the mouse wheel or using the <span class="cmd">Scale</span> popup menu. You can use the <span class="cmd">Custom</span> option to precisely set the desired zoom factor.

<span class="cmd">Printer setup button</span>: Provides access to the selection panel and printer settings.

<span class="cmd">Print button</span>: Provides access to the selection panel and printer settings, then starts printing via the OK button.

<span class="cmd">Preview button</span>: Displays an exact preview of the print.

<span class="cmd">Options button</span>: The context menu or the options button allows you to view the print options; these same options are also available in the general options of the program via the Tools / Options menu or via the Configuration folder of the system tab.

![](images/print_setup.png)

<property>Print Color</property>: Color, Monochrome (gray scale), or Black & White.

<property>Border Size</property>: If  0, the border display is disabled ; the values given in <property>Margins</property> define the position of the border.

<property>Page Orientation</property>: portrait (vertical) or landscape (horizontal)

## Export

<span class="cmd">To DXF</span>: Exports the current file to DXF format (AC1009 = AutoCAD R11 / R12)

<span class="cmd">To STL</span>: All surface objects on visible layers will be merged into one file and exported to STL format (mesh) ![](images/New10.png)

