---
title: ![icon](../img/icon/cam_turn32.png) Lathe Machining Operation
---
# ![icon](../img/icon/cam_turn32.png) Lathe Machining Operation

> **Note:** The lathe code is new to version 0.9.8 and is still undergoing testing and development.  
Treat any lathe gcode with caution and run simulations or air cuts before machining.
{:.warning}

The Lathe machining operation has been provided as a plugin.  In this way the plugin can be developed and updated independently
of the main CamBam application.  It is also a demonstration of the ability to extend CamBam's machining capability using
user written plugins.

The file <span class="file">lathe-test.cb</span> in the CamBam samples folder demonstrates the new lathe operation.

In this initial lathe release there are a number of limitations:

- Only profiling operations are currently supported.  No facing, boring or threading support yet.
- Apart from the tool radius, there is no mechanism to define a lathe tool shape.  The part should be drawn to allow for the cutter size
and shape.

## Drawing

[![Lathe drawing](../images/lathe_scheme-t.png){:.icenter}](../images/lathe_scheme.jpg)

A lathe profile can be generated from a 2D line representing the shape to machine.  
The shape should be drawn so that:  
The lathe <b>+X</b> axis is drawn in the <b>-Y</b> direction and  
The lathe <b>+Z</b> axis is drawn in the <b>+X</b> direction.  
This is so that the drawing will appear in the same orientation as when standing in front of a conventional lathe.  
The toolpaths will be converted to standard lathe X and Z coordinates when the gcode is produced.

<b>Only draw the profile line to be cut.</b>  Do not draw closed polylines, mirrored lines on the opposite side
of the turning axis or lines along the turning axis as the lathe operation will try to cut these as well which
will cause problems.

The profile line can be drawn anywhere in the drawing.  If this line is away from the origin, the Machining Origin
should be set so that it lies on the axis of rotation and at the Z=0 (lathe coordinate).

An example showing a profile where the machining 0,0 point is the same as the drawing origin.

![](../images/tour_ori1.png){:.icenter}

The same pattern drawn away from the origin, where the machining origin (red X) has been moved to indicate the lathes X=0, Z=0 point.

![](../images/tour_ori2.png){:.icenter}

You can set the machine zero by setting MachiningOrigin property of the machining or part objects.
Click the ![](../images/CONTENT.jpg) button to the right of the MachiningOrigin property to select the machine zero
point on the drawing.

## Stock Object

The lathe operation can use information from the stock object if one is defined, to determine properties such as stock
surface and the machining envelope.

CamBam's stock definition does not currently support cylindrical stock so the 
stock will be shown as a rectangular block.

The following image shows a stock objects of 9-mm diameter and 100mm long (purple cube).

[![](../images/tour_stock1-t.png){:.icenter}](../images/tour_stock1.png)

If the <i>Stock Surface</i> property is set to <i>Auto</i>, the stock object size is used to define it.

[![](../images/tour_stock2-t.png){:.icenter}](../images/tour_stock2.png)

- The X size will be the length of the stock (along the lathe's Z axis).
- The Z and Y size should both be set to the stock diameter.
- StockSurface should be set to the stock radius.
- The Stock offset Y value should be set to <b>negative</b> the stock radius.

## Using the Lathe Operation

Select a suitable profile line, then insert a lathe operation by selecting the top Machining menu, then select ![Lathe](../images/basics/cam_turn.png) Lathe.<br />
Note - The lathe plugin does not currently add an icon to the toolbar or drawing context menu.

Make sure the following are set:

- Workplane is set to XZ.
- Stock surface equals the radius of the stock.
- Clearance plane is greater than the radius of the stock.
- The machining origin is set along the axis of rotation.
- The tool diameter is set to twice the tool nose radius.
- The tool profile is set to Lathe.
- The correct RoughingFinishing option is set.
- If Roughing, a small RoughingClearance value is set.
- DepthIncrement and feedrates are appropriate for the material.
- Define the stock object if needed.
- A suitable post processor such as Mach3-Turn or EMC-Turn are selected in the Machining properties.

## Properties

<table class="prop" cellspacing="0" width="100%">
<!-- $include file="../en/_prop/lathe.ClearancePlane.html" -->
<tr>
<td>
	<property>Clearance Plane</property>
</td>
<td>
    <p>The safe <b>X</b> lathe coordinate to avoid any stock.  The clearance plane value should always be expressed as a <b>radius</b>.</p>
</td>
</tr><!-- $include.end -->
<!-- $include file="../en/_prop/mop.CustomMOPFooter.html" -->
<tr>
<td>
	<property>Custom MOP Footer</property>
</td>
<td>
    <p>A multi-line gcode script that will be inserted into the gcode post after the current machining operation.</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.CustomMOPHeader.html" -->
<tr>
<td>
	<property>Custom MOP Header</property>
</td>
<td>
    <p>A multi-line gcode script that will be inserted into the gcode post before the current machining operation.</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.CutFeedrate.html" -->
<tr>
<td>
	<property>Cut Feedrate</property>
</td>
<td>
    <p>The feed rate to use when cutting.</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/lathe.DepthIncrement.html" -->
<tr>
<td>
	<property>Depth Increment</property>
</td>
<td>
    <p>When roughing, this is the radial X distance between each parallel cut.</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.Enabled.html" -->
<tr>
<td>
	<property>Enabled</property>
</td>
<td>
    <p>
        <value>True</value>: The toolpaths associated with this machining operation are displayed and included in the gcode output<br>
        <value>False</value>: The operation will be ignored and no gcode or tool paths will be produced for this operation.
    </p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/lathe.LatheCutDirection.html" -->
<tr>
<td>
	<property>Lathe Cut Direction</property>
</td>
<td>
    <ul>
    <li><value>Right Hand</value> - Cuts will move from right (+Z) to left (-Z).</li>
    <li><value>Left Hand</value> - Cuts will move from left (-Z) to right (+Z).</li>
    </ul>
</td>
</tr><!-- $include.end -->
<!-- $include file="../en/_prop/lathe.LatheLeadInLength.html" -->
<tr>
<td>
	<property>Lathe Lead In Length</property>
	<br /><span class="new">New [0.9.8N]</span>
</td>
<td>
    <p>Controls the length of the 45 degree lead in moves.  A zero value will disable these moves.</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/lathe.LatheLeadOutLength.html" -->
<tr>
<td>
	<property>Lathe Lead Out Length</property>
	<br /><span class="new">New [0.9.8N]</span>
</td>
<td>
    <p>Controls the length of the 45 degree back away moves.  A zero value will disable these moves.</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.MaxCrossoverDistance.html" -->
<tr>
<td>
	<property>Max Crossover Distance</property>
</td>
<td>
<p>Maximum distance as a fraction (0-1) of the tool diameter to cut in horizontal transitions.</p>
<p>If the distance to the next toolpath exceeds MaxCrossoverDistance, a retract, rapid and plunge to the next position, via the clearance plane, is inserted.</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.Name.html" -->
<tr>
<td>
	<property>Name</property>
</td>
<td>
    <p>
        Each machine operation can be given a meaningful name or description.<br>
        This is output in the gcode as a comment and is useful for keeping track of the function of each machining operation.
    </p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.OptimisationMode.html" -->
<tr>
<td>
	<property>Optimisation Mode</property>
</td>
<td>
    <p>An option that controls how the toolpaths are ordered in gcode output.</p>
    <p>
        <value>New (0.9.8)</value> - A new, improved optimiser currently in testing.<br>
        <value>Legacy (0.9.7)</value> - Toolpaths are ordered using same logic as version 0.9.7.<br>
        <value>None</value> - Toolpaths are not optimised and are written in the order they were generated.
    </p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.PlungeFeedrate.html" -->
<tr>
<td>
	<property>Plunge Feedrate</property>
</td>
<td>
    <p>The feed rate to use when plunging.</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.PrimitiveIds.html" -->
<tr>
<td>
	<property>Primitive IDs</property>
</td>
<td>
    <p>List of drawing objects from which this machine operation is defined.</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/lathe.RoughingFinishing.html" -->
<tr>
<td>
	<property>Roughing / Finishing</property>
</td>
<td>
    <p>
        The <property>Roughing / Finishing</property> property is used to select the machining method.  If <value>Roughing</value> is selected, a number of straight
        passes are used at each depth increment, down to the source shape + roughing clearance, followed by a single cut at the
        roughing clearance distance that follows the shape.  For <value>Finishing</value>, a single cut that follows the shape at the roughing clearance distance
        is used.
    </p>
</td>
</tr><!-- $include.end -->
<!-- $include file="../en/_prop/mop.RoughingClearance.html" -->
<tr>
<td>
	<property>Roughing Clearance</property>
</td>
<td>
    <p>This is the amount of stock to leave after the final cut.</p>
    <p>Remaining stock is typically removed later in a finishing pass.</p>
    <p>Negative values can be used to oversize cuts.</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.SpindleDirection.html" -->
<tr>
<td>
	<property>Spindle Direction</property>
</td>
<td>
    <p>The direction of rotation of the spindle.</p>
    <p><value>CW</value> | <value>CCW</value> | <value>Off</value></p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.SpindleRange.html" -->
<tr>
<td>
	<property>Spindle Range</property>
</td>
<td>
    <p>The pulley number or dial setting of the spindle for the target speed.</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.SpindleSpeed.html" -->
<tr>
<td>
	<property>Spindle Speed</property>
</td>
<td>
    <p>The speed in RPM of the spindle.</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.StartPoint.html" -->
<tr>
<td>
	<property>Start Point</property>
</td>
<td>
    <p>
        Used to select a point, near to where the first toolpath should begin machining.<br />
        If a start point is defined, a small circle will be displayed at this point when the machining operation
        is selected.  The start point circle can be moved by clicking and dragging.
    </p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/lathe.StockSurface.html" -->
<tr>
<td>
	<property>Stock Surface</property>
</td>
<td>
    <p>This is the X offset of the stock surface at which to start machining.</p>
    <p>Can be set explicitly or determined from the stock object.  Stock surface should always be expressed as a <b>radius</b>.</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.Style.html" -->
<tr>
<td>
	<property>Style</property>
	<br /><span class="new">[New! 0.9.8]</span>
</td>
<td>
<p markdown="1">
Select a [CAM Style](cam-style.md){:.name} for this machining operation.
All default parameters will be inherited from this style.
</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.Tag.html" -->
<tr>
<td>
	<property>Tag</property>
</td>
<td class="desc">
    <p>A general purpose, multi-line text field that can be used to store notes or parameter data.</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.ToolDiameter.html" -->
<tr>
<td>
	<property>Tool Diameter</property>
</td>
<td>
    <p>This is the diameter of the current tool in drawing units.</p>
    <p>
        If the tool diameter is 0, the diameter from the tool information stored in the tool library
        for the given tool number will be used.
    </p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.ToolNumber.html" -->
<tr>
<td>
	<property>Tool Number</property>
</td>
<td class="desc">
    <p>The ToolNumber is used to identify the current tool.</p>
    <p>
        If ToolNumber changes between successive machine ops a toolchange instruction is created in gcode.
        ToolNumber=0 is a special case which will not issue a toolchange.
    </p>
    <p>The tool number is also used to look up tool information in the current tool library.  The tool library is specified
	in the containing Part, or if this is not present in the Machining folder level.  If no tool library is defined the
	Default-(units) tool library is assumed.</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/lathe.ToolProfile.html" -->
<tr>
<td>
	<property>Tool Profile</property>
</td>
<td>
    <p>The shape of the cutter.  The new <value>Lathe</value> tool profile should always be used.</p>
    <p>
        If the tool profile is <value>Unspecified</value>, the profile from the tool information stored in the tool library
        for the given tool number will be used.
    </p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.VelocityMode.html" -->
<tr>
<td>
	<property>Velocity Mode</property>
</td>
<td>
    <p>Instructs the gcode interpreter whether or to use look ahead smoothing.</p>
    <p>
        <value>Constant Velocity</value> - (G64) Smoother but less accurate.<br>
        <value>Exact Stop</value> - (G61) All control points are hit but movement may be slower and jerky.<br>
        <value>Default</value> - Uses the global VelocityMode value under machining options.
    </p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/lathe.WorkPlane.html" -->
<tr>
<td>
	<property>Work Plane</property>
</td>
<td>
    <p><b>Should always be set to XZ for lathe code!</b></p>
</td>
</tr>
<!-- $include.end -->
</table>

## Post Processor

Three sample lathe specific post processor definitions have been provided : Mach3-Turn, Mach3-Turn-CV (Mach3 with CutViewer definitions) and EMC2-Turn.  
These definitions may need to be customised to suit the configuration of those controllers.

This section describes some post processor properties that are relevant to customising the lathe gcode output.

<table class="prop" cellspacing="0" width="100%">
    <tr>
    <td>
		<property>Clearance Plane Axis</property>
	</td>
    <td>
        Used to specify which direction clearance moves are made. Usually Z for normal milling, but must be set to X for lathe turning operations.
    </td>
	</tr>
    <tr>
    <td>
		<property>Lathe X Mode</property>
	</td>
    <td>
        <p>Controls whether the X lathe coordinates will be written to gcode as <value>Radius</value>, or a <value>Diameter</value>.</p>
        <p>
        <property>Depth Increment</property>, <property>Stock Surface</property> and <property>Clearance Plane</property> parameters should always be specified as a radius, regardless
        of the post processor <property>Lathe X Mode</property> setting.
        </p>
    </td>
	</tr>
    <tr>
    <td>
		<property>Lathe Tool Radius Offset</property>
	</td>
    <td>
<p>If <value>False</value>, the toolpath at the center of the tool radius is output.</p>
<p>If <value>True</value>, an appropriate tool radius offset is applied.  The toolpath will be offset by a negative tool radius in the lathe X axis.  
The direction of the Z tool radius offset is determined by the cut direction. 
For right hand cuts the toolpath Z will be offset by a negative tool radius.  For left hand cuts, a positive tool radius Z offset is used.</p>
<p markdown="1">            
![Tool radius offsets](../images/cam/tool-radius-offset.png)
</p>
<p>
In the diagram above, the red cross represents the toolpath reference point when <property>Lathe Tool Radius Offset</property> is set <value>True</value>.  If False, the dot
at the tool radius center will be the reference point.  The reference point is sometimes referred to as the 'Imaginary' or 'Virtual' tool point.
</p>
    </td>
	</tr>
    <tr>
    <td>
		<property>X Mode Diameter</property>
	</td>
    <td>
        <p>Code to use to set X diameter mode (eg G7 for LinuxCNC)</p>
    </td>
    </tr>
    <tr>
    <td>
		<property>X Mode Radius</property>
	</td>
    <td>
        <p>Code to use to set X radius mode (eg G8 for LinuxCNC)</p>
    </td>
    </tr>
    <tr>
    <td>
		<property>Invert Arcs</property>
	</td>
    <td>
        <p>If set <value>True</value>, CW arcs will be output as CCW and vice versa. This may be useful for front face lathe operations.</p>
    </td>
    </tr>
    <tr>
    <td>
		<property>Arc Output</property>
	</td>
    <td>
        <p><value>Normal</value> is the preferred setting and will use G2 and G3 codes to output arcs.  <value>Convert To Lines</value> may be used
        as a last resort if CamBam can not generate arc codes in a format compatible with the destination controller.  <value>Convert To Lines</value> is
        used with the <property>Arc To Lines Tolerance</property> property, where smaller tolerances will result in smoother curves but larger files.</p>
    </td>
    </tr>
</table>


## Tool Definitions

A sample lathe tool library 'Lathe-mm' is provided.  The tool library can be selected by changing the <property>Tool Library</property>
property in the <name>Machining</name> or <name>Part</name> options.

Tool libraries are currently designed to support milling cutters, rather than lathe.  However there are a couple of parameters than 
are useful to store in the tool library.

<property>Tool Profile</property> should always be set to the new <value>Lathe</value> option.  Among other things, this instructs the post processor
to determine the tool radius from the tool diameter.

A new <property>Comment</property> property has been added.  This is a text value that can be included by the post processor when
using the {$tool.comment} macro from with the ToolChange post processor section.    

For example.  CutViewer Turn recognises a gcode comment that defines the geometry of the lathe tool in the following format:  

~~~
TOOL/STANDARD,BA,A,R,IC,ITP
~~~

Refer to the CutViewer Turn documentation for details of this description.  Here is a summary of the parameters:

- **BA** - Back angle.
- **A** - Angle.
- **R** - Radius.
- **IC** - Inner circle.
- **ITP** - Imaginary Tool Point. 0=Tool Center, 3 for right hand offset, 4 left hand offset.

This example <property>Comment</property> property defines a right hand cutter with a 2mm radius, 40 degree back angle and 40 degree taper.  

~~~
{$comment} TOOL/STANDARD,40,40,{$tool.radius},2,3 {$endcomment}
~~~

