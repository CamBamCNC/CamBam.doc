---
title: Lead Moves (Lead In and Lead Out)
---
# Lead Moves (Lead In and Lead Out)

Many machining operations support lead in and out moves, which control the movement used when entering and exiting a cut.

![](../images/lead-moves.png){:align="right" .iright}

Lead in moves can be ramped so the cutter will gradually reduce the cutter Z height while simultaneously feeding in X and or Y.
This can be crucial when using certain cutters that do not support directly plunging into stock.

There are two main types of lead moves; <value>Spiral</value> and <value>Tangent</value>.  Setting the <property>Lead Move Type</property> to <value>None</value> will prevent the use
of a lead move and a direct plunge at the <property>Plunge Feedrate</property> will be used instead.

The lead move properties also support a <property>Lead Move Feedrate</property> parameter.  If this is 0 then the machining operation's <property>Cut Feedrate</property>
is used, otherwise the feedrate entered into the <property>Lead Move Feedrate</property> parameter is used.

## Spiral Lead

The entry move will follow the path of the underlying toolpath in X and Y, while decreasing the cutter Z from the previous
stock level, down to the next target depth.

The angle of the spiral ramp is defined in the <property>Spiral Angle</property> property.  If an angle is specified, once the target depth is reached,
a complete pass of the toolpath is then made at a constant Z depth.  A lead in move will be used at each depth increment.  This may make
it necessary for the cutter to lift up to the clearance plane, then plunge to the start of the next lead move.

> **Hint:** The plunge down to each depth increment can slow machining times considerably.  To reduce this,
a [Fast Plunge Height](machining-options.md#FastPlungeHeight){:.prop} value can be set in the <span class="name">Machining</span> options.  This allows a rapid move to be used down to the 
next cut level.
{:.note }

If <property>Spiral Angle</property> is set to 0, an angle is calculated so that the ramp will complete one depth increment along one pass of 
the target toolpath, in a continuous feed move.<br/>
For closed shapes, the lead move will then replace the toolpath at each depth level,
with just a single constant Z toolpath inserted at the final target depth to ensure a level base to the cut.<br/>
For example; if the source shape used was a circle, the resulting toolpath would be a continuous spiral, with each loop cutting one depth increment,
followed by a circular cut at the target depth.


The following images compare a spiral lead in move with an explicit 15 degree ramp angle and a spiral with a 0 degree angle
where the ramp angle is then automatically calculated.

![](../images/lead-move-spiral.png){:.icenter} 
	
If a very shallow spiral angle is used, it may be necessary for the lead move to complete a number of circuits of the toolpath
before reaching the target depth, as shown in the following image, where a 1 degree angle is used.


![](../images/lead-move-spiral2.png){:.icenter} 


## Tangent

The tangent lead move will use a circular arc move to enter or exit the stock, meeting the target toolpath start point at a tangent.

As well as setting the <property>Lead Move Type</property> to <value>Tangent</value>, the <property>Tangent Radius</property> property must also be defined.

Tangent moves are particularly useful for lead out moves, to avoid tool marks as the cutter moves away from the stock.

![](../images/lead-move-tangent.png){:.icenter}

Tangent moves will also make use of the <property>Spiral Angle</property> parameter, where the arc move will also plunge in the Z direction to form a circular
spiral or spiral segment.  As with spiral lead moves, if the spiral angle is shallow, multiple loops may be needed for the lead move to reach
the target depth.

![](../images/lead-move-tangent2.png){:.icenter}

> In some cases, such as an inside profile cut with internal corners, the default toolpath start point may lead to problems
when using tangent lead moves.  In these cases, the machining start point should be modified to move it to a more sensible 
location, away from inside corners.
{:.warning}

![](../images/lead-move-tangent3.png){:.icenter}




