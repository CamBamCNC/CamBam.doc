---
title: CAD Entities
---
# CAD Entities

# ![icon](../img/icon/poly32.png) Polyline

Polylines consist of multiple straight line and circular arc segments.

Polylines are used internally to represent toolpath shapes as they correspond well to gcode G1 (line) and G2,G3 (arc) moves.

The polylines are drawn with the mouse, making a left click to place the points. A double click on a polyline enter in edit mode and allow to see and to move the points which constitute it.

Click the middle mouse button (wheel) or hit <span class="key">Enter</span> to confirm, or <span class="key">Esc</span> to cancel.


## Properties
<table class="prop" cellspacing="0" width="100%">

<tr>
<td>
	<property>Closed</property>
</td>
<td>
	<p><value>True</value> | <value>False</value></p>
	<p><value>Open</value> polylines have two ends and no defined inside or outside.</p>
	<p><value>Closed</value> polylines are where the first and last points are the same and have a well defined inside and outside.</p>
	<p><b>Note</b> Polylines with first and last points having the same coordinates are not necessarily closed.  The closed marker should
	be set to <value>True</value> for these shapes otherwise unexpected results may occur.</p>
</td>
</tr>

<tr>
<td>
	<property>Points</property>
</td>
<td>
	<p>This property contains a collection of polyline points.  Clicking the [...] button to the right of the property will open up
	a window where the points can be edited directly.</p>
	<p>Each point contains an X,Y and Z coordinate and a bulge parameter.</p>
	<p><property>Bulge</property> is defined as tan(sweep angle/4) for arc segments, where bulge=0 is a straight line.</p>
</td>
</tr>
<!-- $include file="../en/_prop/mop.Tag.html" -->
<tr>
<td>
	<property>Tag</property>
</td>
<td class="desc">
    <p>A general purpose, multi-line text field that can be used to store notes or parameter data.</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/Transform.html" -->
<tr>
<td>
	<property>Transform</property>
</td>
<td>
    <p>A 4 x 4 matrix of numbers used for general transformations of the drawing objects.</p>
    <p>The transform matrix can be used for rotations, translations and scaling about all 3 axis.</p>
    <p><value>Identity</value> indicates no transformations will be applied to the object.</p>
</td>
</tr>
<!-- $include.end -->
</table>

# ![icon](../img/icon/region32.png) Region

A region consists of a closed outer shape and a number of internal holes.

To create a region, select inner and outer shapes then use the <span class="cmd">Edit - Convert - To Region</span> menu option, or press <span class="key">CTRL+R</span>

# ![icon](../img/icon/circle32.png) Circle

The circles are drawn with the mouse, making a left click to place the center, then after releasing the left button, moving the mouse to define the radius followed by a new click of the left button to finish.

<span class="new">New 1.0</span>

A double click on a circle enter in edit mode and thus to see and move the center; click / move the center point to move it ; click / move around the circle to change its diameter.

Click the middle mouse button (wheel) or hit <span class="key">Enter</span> to confirm, or <span class="key">Esc</span> to cancel.

![](../images/cad/edit_circle.png)

## Properties

<table class="prop" cellspacing="0" width="100%">

<tr>
<td>
	<property>Center</property>
</td>
<td>
	<p>The coordinates of the center of the circle.</p>
</td>
</tr>

<tr>
<td>
	<property>Diameter</property>
</td>
<td>
	<p>The diameter of the circle.</p>
</td>
</tr>
<!-- $include file="../en/_prop/mop.Tag.html" -->
<tr>
<td>
	<property>Tag</property>
</td>
<td class="desc">
    <p>A general purpose, multi-line text field that can be used to store notes or parameter data.</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/Transform.html" -->
<tr>
<td>
	<property>Transform</property>
</td>
<td>
    <p>A 4 x 4 matrix of numbers used for general transformations of the drawing objects.</p>
    <p>The transform matrix can be used for rotations, translations and scaling about all 3 axis.</p>
    <p><value>Identity</value> indicates no transformations will be applied to the object.</p>
</td>
</tr>
<!-- $include.end -->
</table>

# ![icon](../img/icon/points32.png) PointList

Point lists are useful for defining points to be used for drilling operations.

A double click on a pointList allows you to enter in edit mode and to move or add points; click / move a point to move it ; click on a the drawing area to add a new one.

Click the middle mouse button (wheel) or hit <span class="key">Enter</span> to confirm, or <span class="key">Esc</span> to cancel.

As well as drawing directly, they can be created from the <span class="cmd">Draw-Point List</span> menu operations

<table class="prop" cellspacing="0" width="100%">

<tr>
<td>
	<span class="cmd">Divide Geometry</span>
</td>
<td>
	<p>Evenly divides a selected shape into a given number and inserts a point at each division.</p>
	<p>This is useful for generating a bolt hole pattern.</p>
</td>
</tr>
<tr>
<td>
	<span class="cmd">Step Around Geometry</span>
</td>
<td>
	<p>Inserts a point at given distances around a selected shape.</p>
</td>
</tr>
<tr>
<td>
	<span class="cmd">Fill Geometry</span>
</td>
<td>
	<p>Fills a closed shape with points.</p>
</td>
</tr>
<tr>
<td>
	<span class="cmd">Offset Fill Geometry</span>
</td>
<td>
	<p>Fills a closed shape with points where alternating rows are offset by half the stepping distance.</p>
</td>
</tr>
<tr>
<td>
	<span class="cmd">Centers</span>
</td>
<td>
	<p>Inserts a point at the center of each selected object.</p>
</td>
</tr>
<tr>
<td>
	<span class="cmd">Extents</span>
</td>
<td>
	<p>Inserts a point at the extremities and center of a boundary rectangle enclosing each selected object.</p>
</td>
</tr>
</table>

## Properties

<table class="prop" cellspacing="0" width="100%">
<tr>
<td>
	<property>Points</property>
</td>
<td>
	This property contains a collection of points.  Clicking the [...] button to the right of the property will open up
	a window where the points can be edited directly.
</td>
</tr>
<!-- $include file="../en/_prop/mop.Tag.html" -->
<tr>
<td>
	<property>Tag</property>
</td>
<td class="desc">
    <p>A general purpose, multi-line text field that can be used to store notes or parameter data.</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/Transform.html" -->
<tr>
<td>
	<property>Transform</property>
</td>
<td>
    <p>A 4 x 4 matrix of numbers used for general transformations of the drawing objects.</p>
    <p>The transform matrix can be used for rotations, translations and scaling about all 3 axis.</p>
    <p><value>Identity</value> indicates no transformations will be applied to the object.</p>
</td>
</tr>
<!-- $include.end -->
</table>

# ![icon](../img/icon/square32.png) Rectangle

The rectangles are drawn with the mouse, making a left click to place an angle, then after releasing the left button, moving the mouse to define the diagonal followed by a new click of the left button to finish.

A double click on a rectangle enter in edit mode and alow to move the points of angles (the object remains rectangular)

Click the middle mouse button (wheel) or hit <span class="key">Enter</span> to confirm, or <span class="key">Esc</span> to cancel.

## Properties

<table class="prop" cellspacing="0" width="100%">
<tr>
<td>
	<property>Corner Radius</property>
</td>
<td>
	<p>This will round the corners of the rectangle to a given radius.</p>
</td>
</tr>

<tr>
<td>
	<property>Height</property>
</td>
<td>
	<p>The height of the rectangle.</p>
</td>
</tr>
<tr>
<td>
	<property>Lower Left</property>
</td>
<td>
	<p>The coordinates of the lower left corner of the rectangle.</p>
</td>
</tr>
<tr>
<td>
	<property>Width</property>
</td>
<td>
	<p>The width of the rectangle.</p>
</td>
</tr>
<!-- $include file="../en/_prop/mop.Tag.html" -->
<tr>
<td>
	<property>Tag</property>
</td>
<td class="desc">
    <p>A general purpose, multi-line text field that can be used to store notes or parameter data.</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/Transform.html" -->
<tr>
<td>
	<property>Transform</property>
</td>
<td>
    <p>A 4 x 4 matrix of numbers used for general transformations of the drawing objects.</p>
    <p>The transform matrix can be used for rotations, translations and scaling about all 3 axis.</p>
    <p><value>Identity</value> indicates no transformations will be applied to the object.</p>
</td>
</tr>
<!-- $include.end -->
</table>

# ![icon](../img/icon/text32.png) Text

## Properties

<table class="prop" cellspacing="0" width="100%">
<tr>
<td>
	<property>Bold</property>
</td>
<td>
	Bold Font Style.
</td>
</tr>
<tr>
<td>
	<property>Char Space</property>
</td>
<td>
	<p>This option scales the width used for each character. The default is 1. A setting of 2 would double the space used for each character 
	(but not the character itself).</p>
</td>
</tr>

<tr>
<td>
	<property>Font</property>
</td>
<td>
	<p>This is the name of the font to use for the text.</p>
</td>
</tr>

<tr>
<td>
	<property>Height</property>
</td>
<td>
	<p>This is the text height in drawing units.</p>
	<p>The height is based on the <b>em</b> square, which is a property of the font that describes the largest 
	dimensions possible of the font.</p>
	<p>To obtain an accurate height, given the text and font entered, the <span class="cmd">Edit - Resize</span> command should be used.</p>
</td>
</tr>
<tr>
<td>
	<property>Italic</property>
</td>
<td>
	<p>Italic Font Style.</p>
</td>
</tr>
<tr>
<td>
	<property>Line Space</property>
</td>
<td>
	<p>This scales the distance between each text line. The default is 1.</p>
</td>
</tr>

<tr>
<td>
	<property>Location</property>
</td>
<td>
	<p>This is the first and at the moment, only alignment point. The TextAlignmentH and TextAlignmentV options are all relative to this point.</p>
</td>
</tr>
<tr>
<td>
	<property>Regular</property>
</td>
<td>
	<p>Regular Font Style.</p>
</td>
</tr>
<!-- $include file="../en/_prop/mop.Tag.html" -->
<tr>
<td>
	<property>Tag</property>
</td>
<td class="desc">
    <p>A general purpose, multi-line text field that can be used to store notes or parameter data.</p>
</td>
</tr>
<!-- $include.end -->
<tr>
<td>
	<property>Text</property>
</td>
<td>
	<p>The text to enter. To enter multi line text, click the [...] button after this property.</p>
</td>
</tr>
<tr>
<td>
	<property>Text Alignment Horizontal</property>
</td>
<td>
	<p><value>Left</value>, <value>Center</value> or <value>Right</value> (relative to <property>Location</property>).</p>
</td>
</tr>
<tr>
<td>
	<property>Text Alignment Vertical</property>
</td>
<td>
	<p><value>Top</value>, <value>Center</value> or <value>Bottom</value> (relative to Location).<br />
	NOTE: <value>Bottom</value> is actually the baseline of the text.</p>
</td>
</tr>
<!-- $include file="../en/_prop/Transform.html" -->
<tr>
<td>
	<property>Transform</property>
</td>
<td>
    <p>A 4 x 4 matrix of numbers used for general transformations of the drawing objects.</p>
    <p>The transform matrix can be used for rotations, translations and scaling about all 3 axis.</p>
    <p><value>Identity</value> indicates no transformations will be applied to the object.</p>
</td>
</tr>
<!-- $include.end -->
</table>

# ![icon](../img/icon/arc32.png) Arc

The arcs are drawn with the mouse by clicking the two extreme points, then by positioning the center with a third click.

<span class="new">New 1.0</span>

A double click on an arc allows you to enter in edit mode and to move its center start and end  points.

Click the middle mouse button (wheel) or hit <span class="key">Enter</span> to confirm, or <span class="key">Esc</span> to cancel.

**Note:** While drawing or editing an arc, pressing the <span class="key">Shift</span> key will invert the direction of the arc when moving the center.


## Properties

<table class="prop" cellspacing="0" width="100%">
<tr>
<td>
	<property>Center</property>
</td>
<td>
	<p>The center of the arc.</p>
</td>
</tr>
<tr>
<td>
	<property>Radius</property>
</td>
<td>
	<p>The radius of the arc.</p>
</td>
</tr>
<tr>
<td>
	<property>Start</property>
</td>
<td>
	<p>The start angle in degrees of the first arc point.  Angle = 0 is along the positive X axis.</p>
</td>
</tr>
<tr>
<td>
	<property>Sweep</property>
</td>
<td>
	<p>The sweep angle in degrees from the first to second arc point.  Positive angles are counter clockwise and negative angles clockwise.</p>
</td>
</tr>
<!-- $include file="../en/_prop/mop.Tag.html" -->
<tr>
<td>
	<property>Tag</property>
</td>
<td class="desc">
    <p>A general purpose, multi-line text field that can be used to store notes or parameter data.</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/Transform.html" -->
<tr>
<td>
	<property>Transform</property>
</td>
<td>
    <p>A 4 x 4 matrix of numbers used for general transformations of the drawing objects.</p>
    <p>The transform matrix can be used for rotations, translations and scaling about all 3 axis.</p>
    <p><value>Identity</value> indicates no transformations will be applied to the object.</p>
</td>
</tr>
<!-- $include.end -->
</table>

# ![icon](../img/icon/line32.png) Line

Lines have multiple segments, similar to polylines, but can only contain straight sections.

<span class="new">New 1.0</span>

Lines can be edited by double-clicking on it, like polylines

# ![icon](../img/icon/surface32.png) Surface

These are triangular faceted 3D meshes imported from an STL or 3DS file

<span class="new">New 1.0</span>

They also can be created in CamBam with Draw/Surface/extrude solids and Draw/Surface/Fill

Surfaces are triangle face meshes imported from STL and 3DS files.

# ![icon](../img/icon/spline32.png) Spline

Splines (or NURBS) are curves that are defined by a series of control points and weights.

<span class="new">New 1.0</span>

The splines are drawn with the mouse, making a left click to place the points of inflection.

A double click on a spline enter in edit mode and thus to see and move the control points that constitute it, as well as the points that make it possible to modify the curvature locally.

Pressing the <span class="key">Ctrl</span> key while moving a control point (red circle) temporarily breaks the link with its curvature control points (yellow squares)

Click the middle mouse button (wheel) or hit <span class="key">Enter</span> to confirm, or <span class="key">Esc</span> to cancel.

![](../images/cad/edit_spline.png)

# ![icon](../img/icon/cadscript32.png) Script object

<span class="new">New 1.0</span>

[See here](script-object.md)

# ![icon](../img/icon/bitmap32.png) Bitmap object

<span class="new">New 1.0</span>

[See here](bitmap.md)

# Draw/More

# Tangent

Draws a line between points of tangency of circles or arcs (or arcs contained in a polyline)

After choosing the <span class="cmd">Tangent</span> function, move the mouse over the first circle / arc; the end of the line will follow and stick to the curve ; click the left mouse button to fix that first point (which will "slide" along the curve depending on the orientation you give to the line)

Then move the mouse cursor on the second circle / arc; a triangle will appear at the nearest point of tangency. This triangle will change places depending on the position of the mouse.

![](../images/cad/tangente_01.png) ![](../images/cad/tangente_02.png)

Hook the end of the line on the triangle, then click on the left button to finish.

![](../images/cad/tangente_03.png)

>Note: It may be easier to "feel" the magnetization to the curve/triangle if you disable grid snapping

# Flat Spiral

Draws a flat spiral.

The <property>Stepover </property>is the spacing between the turns in current drawing unit.

The <property>Direction </property>is the winding direction of the spiral, and can be clockwise (CW) or counter-clockwise (CCW)

After filling in these two informations, draw the spiral starting at the center and then defining the radius (as if to draw a circle)

![](../images/cad/flat_spiral.png)

# Spiral Path

Draws a spiral along a path.

First, select a shape that will serve as path, then choose the <span class="cmd">Spiral Path </span>command from the menu.

The same window as for a flat spiral will open. It allows to choose the <property>Stepover </property>and <property>Direction</property>, as before, but also to specify the <property>radius </property> of the spiral.

![](../images/cad/spiral_path.png)

# Trochoidal Path

Same principle as above, but this time it is a succession of arcs that follow the path, and not a spiral.

Combined with an engraving operation, this path allows you to quickly remove material with less stress for the tools.

![](../images/cad/trocho.png)

# Ellipse

Allows drawing ellipses by drawing a bounding rectangle with the mouse.

The ellipse once drawn becomes a polyline, consisting of arcs and lines.

![](../images/cad/ellipse.png)
