---
title: Drawing (CAD)
---

## Drawing (CAD)

{:.subcontents}
- **[Entities](entities.md)**
- **[Script Entity](script-object.md)**
- **[Bitmaps](bitmap.md)**
- **[Layers](layers.md)**
- **[Transformations](transformations.md)**
- **[Operations](operations.md)**
- **[Edit Polyline](edit-polyline.md)**
- **[Edit Surface](edit-surface.md)**
- **[Edit Points](edit-point-lists.md)**
- **[Creating Surfaces](draw-surface.md)**
- **[Region Fill](region-fill.md)**
