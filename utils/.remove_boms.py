﻿"""Remove utf-8 BOM marker from files as Jekyll cannot handle this!
"""
import fnmatch
import os
import codecs

def process_file( srcfile, destfile ):
    print("processing " + srcfile)
    s = codecs.open(srcfile, mode='r', encoding='utf-8-sig').read()
    codecs.open(destfile, mode='w', encoding='utf-8').write(s)

filepattern = "*.md"
#filepattern = "*.html"

for root, dirnames, filenames in os.walk('..\en'):
    for filename in fnmatch.filter(filenames, filepattern):
        srcfile = os.path.join(root, filename)
        destfile = srcfile
        process_file( srcfile, destfile )
        