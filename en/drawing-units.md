---
title: Drawing Dimensions / Units
---
# Drawing Dimensions / Units

![](images/units.png){:align="right" .iright}

The current drawing's units can be changed from the drop down list on the toolbar.

After changing the drawing units, CamBam may prompt:  
>'Would you also like to change the default units for new drawings?'
{:style="width:50%;"}

If *Yes*{:.cmd} is clicked, then the selected units will become the default drawing units.  
If *No*{:.cmd} is clicked, the current drawing's units will change but the default settings will remain unchanged.

> **Note**: Changing the drawing units will not change the size of the drawing objects, only the units that the 
objects are measured in.  
To scale objects, use the [Transform - Resize](cad/transformations.md#Resizing){:.name} command.
{:.note}
