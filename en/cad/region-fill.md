---
title: Region Fill
---
# Region Fill

These methods are used to fill regions, polylines and other closed shapes with various line patterns.

These fill patterns are used by machining operations such as pockets and 3D waterline roughing to generate toolpaths, but
can be used independently to create interesting drawing effects.

Region fillers take the following parameters:

*Margin*{:.prop} : This is the distance away from source shapes to avoid filling lines.  In pocketing, this would be the same as the tool radius.

*Step Over*{:.prop} : This is the distance between fill lines.  In pocketing, this would be the same as the stepover distance.

## Filling Patterns

### Inside Offsets

{:align="center"}
![Inside Offset](../images/cad/inside_offset.png){:.icenter}  
Progressive offsets from holes outwards.

### Outside Offsets

{:align="center"}
![Outside Offset](../images/cad/outside_offset.png){:.icenter}  
Progressive offsets from outside shape inwards.

### Inside + Outside Offsets

{:align="center"}
![Inside + Outside Offset](../images/cad/inside_outside_offset.png){:.icenter}  
Progressive offsets from outside shape inwards unioned with offsets from holes outwards.

### Horizontal Hatch

{:align="center"}
![Horizontal Hatch](../images/cad/horizontal_hatch.png){:.icenter}  
Horizontal line fill style.

### Vertical Hatch

{:align="center"}
![Vertical Hatch](../images/cad/vertical_hatch.png){:.icenter}  
Vertical line fill style.
