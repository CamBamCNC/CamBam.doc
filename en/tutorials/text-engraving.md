---
title: Tutorial - Text Engraving
---
# Tutorial: Text Engraving

This tutorial describes inserting text into CamBam and generating an engraving operation from it.

## Inserting Text

To insert text into a drawing, use the <span class="cmd">Draw - Text</span> menu option or the ![Insert Text Tool](../images/basics/text.png) tool bar icon.

A multi-line text editor will be displayed.  Insert the text the press OK.  You will then be prompted for the location of the text
by clicking on the drawing.

> **Note:** By default, after entering a text item, the draw text command will be repeated and the text entry screen shown again.
Press the <span class="cmd">Cancel</span> button to end the text entry commands.
This behaviour can be turned off by setting the [Repeat Commands](configuration.md#RepeatCommands){:.prop} option in system configuration to <value>False</value>.
{:.note}

Text can be modified at a later stage by double clicking the text object in the drawing tree, or by clicking the ellipsis [...] button 
to the right of the Text object's <property>Text</property> property.

[![](../images/tutorial.text1-t.png){:.icenter}](../images/tutorial.text1.png)

Refer to the [Text drawing](../cad/entities.md#Text){:.name} section for details on the text drawing object.

## Creating Engraving GCode

To create some engraving gcode, select the text then select the <span class="cmd">Machining - Engrave</span> menu option or use the ![MOP Engrave](../images/basics/cam_engrave.png) toolbar icon.

For shallow engraving (0.3mm), try these properties:

*Depth Increment*{:.prop} = 0.3

*Tool Profile*{:.prop} = V-Cutter

> **Note:** The default CAM Style used an <i>Auto</i> setting for <property>Target Depth</property>. For engraving operations
using a V-Cutter, the target depth is automatically calculated to be one depth increment below the stock surface.
{:.note}

Sample Engraving...

[![](../images/tutorials/engraving/engraving-1-t.jpg){:.icenter}](../images/tutorials/engraving/engraving-1.jpg)

Not exactly high art, but the letters are quite small (3-6mm) and plywood isn't the best precision engraving material.

## Single Stroke (Stick) Fonts

To create thin engraving, ideally a 'stick' font should be used, that is a font with no thickness.  
Unfortunately, True Type Fonts (TTF) do not support open shaped fonts so engraving results with TTF fonts can be hit and miss.

[GeorgeRace](http://www.mykitairplane.com/) has created some excellent 'Stick Fonts' and has made them available
on the CamBam user forum [here](http://www.cambam.co.uk/forum/index.php?topic=1557.0).
