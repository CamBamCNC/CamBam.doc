---
title: Post Processor System
---
# Post Processor System

The format of generated gcode files can be controlled using post processor definitions. These definitions can be created,
copied and modified within the <span class="name">Post Processors</span> section of the [System](../basics-drawing-and-system.md#System){:.name} tab.

The post processor used for a specific drawing is set under the machining options.  Select the machining folder in the drawing tree and 
look in the Post Processor group of the machining properties.   If no post processor is specified, the default post processor will be used.

<span class="new">[New! 0.9.8N]</span>  
To set the default post processor, right click the definition in the <span class="name">Post Processors</span> section of the [System](../basics-drawing-and-system.md#System){:.name} tab, 
then select <span class="cmd">Set As Default</span>.  The default definition will be marked with a green arrow.

## Machining Properties

<table class="prop" cellspacing="0" width="100%">
<tr>
<td>
	<property>Post Processor</property>
</td>
<td>
	<p>This option is a drop down list that contains all the custom post processors defined in the system folder.</p>
	<p>Leave this blank to use the default post processor.</p>
</td>
</tr>

<tr>
<td>
	<property>Post Processor Macros</property>
</td>
<td>
	<p>This option is used to pass user defined macros from the drawing to the post processor.  
	This is a multi-line text field containing multiple macro definitions in the format $macro=value.</p>
    <p>
	Example:<br/>
	$o=1234<br/>
	$stock_height=0.4</p>
</td></tr>

</table>

## Post Processor Management

![](../images/post-processor.png){:align="right" .iright}

The list of available post processors is accessed from the <span class="name">Post Processor</span> folder of the system tab.
Here, post processor definitions can be created, modified, copied, renamed and deleted.

New post processors can be created via the context menu visible when right clicking the post processor folder.
Alternatively, existing definitions can be copied, pasted then modified.  This is a good way of creating variations
of a working post processor.

If post processor files are modified or new ones created outside of CamBam or in another CamBam instance, the post processor list should be refreshed 
using the <span class="cmd">Tools - Reload Post Processors</span> menu option.

Post processors are XML files with a .cbpp file extension, stored in the \post sub folder of the system folder.

<p style="clear:both;"></p>

## Install a post processor downloaded from the Internet

1. If necessary, unzip the file to obtain a ***xxx.cbpp*** file
2. In CamBam, menu <span class="cmd">Tools/Browse System Folder </span>to open the location of the "system" of CamBam.
3. Copy your .cbpp file into the <span class="name">post </span>folder of the "system" of camBam
4. Use the <span class="cmd">Tools/Reload Post Processors </span>menu to add it to the list of available post processors (or restart CamBam)



## Post Processor sections

The post processor definition contains a number of sections.  Each section can contain a mixture of literal text, which is output to the destination gcode file directly, and text macros of the format {$macro}.
The macro definitions are defined within other sections of the post processor, or by defining user macros in the <property>Post Processor Macros</property> property of drawing's machining options.
The macros are evaluated and the resulting text values are written to the gcode output.

> **Note:** If any of the following sections are not visible in the property editor, make sure the <span class="name">Advanced</span> property view button is selected.
{:.note}

## (Main) - Post File

This section defines the general structure of the gcode file.  It typically includes three macros that are evaluated internally based on rules defined in further sections of the post processor.

*{$header}* - This macro is evaluated using the <property>Header</property> section described below.  
*{$mops}* - This macro is evaluated to a list of blocks of text, one block per each machining operation.  Each block is formatted using the rules in the <property>MOP</property> section.  
*{$footer}* - This macro is evaluated using the <property>Footer</property> section described below.

**Example:**

~~~
%
O{$o}
( MY FANUC POST )
{$header}
G0 X10Y10Z0
{$mops}
{$footer}
%
~~~

> **Note:** The value of {$o} macro is passed to the post processor using the drawing's <property>Post Processor Macros</property> property which may contain a value such as '$o=1234'.
{:.note }

The % characters are output literally and would be omitted if not using an RS232 file transfer program.

## (Main) - Header

Defines the text rules used by the {$header} macro.

**Example:**

~~~
{$comment} {$cbfile.name} {$date} {$endcomment}
{$tooltable}
{$comment} CUTVIEWER {$endcomment}
{$comment} FROM/0,0,5 {$endcomment}
{$comment} TOOL/MILL,1,0,20.0,0 {$endcomment}
{$comment}STOCK/BLOCK,{$stock_width},{$stock_length},
    {$stock_height},{$stock_x},{$stock_y},{$stock_z} {$endcomment}
{$cbfile.header}
{$units} {$distancemode} {$velocitymode} {$cuttercomp(off)}
{$toolchange(first)}
G54 ( Use fixture 1 )
{$clearance}
~~~

Once again, the <property>Post Processor Macros</property> property is used to pass the {$stock_...}  macros to the post processor, which in this example may contain text such as:

~~~
$stock_length=150
$stock_width=150
$stock_height=12.7
$stock_x=75
$stock_y=75
$stock_z=12.7
~~~

## (Main) - Footer

Defines the text rules used by the {$footer} macro.

**Example:**

~~~
{$clearance}
G28 G91 Z0
G90 G53 X-15.0 Y0.0
M09
{$spindle(off)}
{$endrewind}
~~~

## (Main) - MOP

Defines how each item of the {$mops} macro is formatted.
This information will be repeated in the gcode output for each active machining operation.

**Example:**

~~~
{$comment} {$mop.name} {$endcomment}
{$toolchange}
{$velocitymode} {$workplane}
{$mop.header}
{$spindle} {$s}
{$blocks}
{$mop.footer}
~~~

## (Main) Start Cut

<span class="new">[New! 0.9.8L]</span>  
Macro to use when about to feed cut.  This may be used for plasma or laser cutters to power on the cutting tool.

The start of cutting is determined when a feed move is detected where Z is below the stock surface.

## (Main) End Cut

<span class="new">[New! 0.9.8L]</span>  
Macro to use when finished a feed cut.  This may be used for plasma or laser cutters to power off the cutting tool.

The end of cutting is determined when a rapid is detected or a feed move where Z is at or above the stock surface.

For example, to power off a laser to avoid holding tabs, use <b>square</b> holding tabs and set the holding tab height so that the 
top part of the tab move is above the stock surface.  The <property>Start Cut</property> macro will then be invoked when the feed move resumes
below the stock surface.

## (Main) Post Processor Macros

<span class="new">[New! 0.9.8N]</span>  
This property can be used to set default values for any custom macros used in the post processor.

Custom macro values will be overridden by the values set in the <value>Post Processor Macros</value> property of the machining options.

## Tools - Tool Table Item

Defines how each item of the {$tooltable} macro is produced.  Tool tables are typically inserted in the header of a file and 
contain commented text describing the list of tools that will be used in the gcode file.

**Example:**

~~~
{$comment} T{$tool.index} : {$tool.diameter} {$endcomment}
~~~

## Tools - Tool Change

Defines how the {$toolchange} macro is formatted.

**Example:**

~~~
{$clearance}
{$comment} T{$tool.index} : {$tool.diameter} {$endcomment}
{$comment} Tool Radius and Taper coming soon {$endcomment}
{$comment} TOOL/MILL, {$tool.diameter}, {$tool.radius}, 
    {$tool.length}, 0 {$endcomment}
T{$tool.index} M6
~~~

## G Codes - G0, G1, G2, G3, G81, G82, G83

These sections define how the commonly used gcode operators are output.

## G Codes - Arc Center Absolute

Used in the {$mop.header} macro to specify that ArcCenterMode is set to Absolute. Mach3 recognizes G90.1

## G Codes - Arc Center Incremental

Used in the {$mop.header} macro to specify that ArcCenterMode is set to Absolute. Mach3 recognizes G91.1

## G Codes - Canned Cycle Start

<span class="new">[New! 0.9.8h]</span>  
Code sequence used at the start of a group of canned cycle blocks.  Typically G98 for initial level return after canned cycles.

## G Codes - Canned Cycle End

<span class="new">[New! 0.9.8h]</span>  
Code sequence used at the end of a group of canned cycle blocks.  Typically G80.

## G Codes - Cutter Comp Off, Cutter Comp Left, Cutter Comp Right

<span class="new">[New! 0.9.8h]</span>  
Used in the {$cuttercomp(off|L|R)} macros.  Typically Off=G40, Left=G41, Right=G42.

## G Codes - Distance Absolute, Distance Incremental

<span class="new">[New! 0.9.8h]</span>  
Typically absolute=G90, incremental=G91. <b>NOTE!</b> Incremental distance mode is not currently supported.

## G Codes - Units (Inches), Units (Metric)

<span class="new">[New! 0.9.8h]</span>  
Typically inches=G20, millimeters=G21.

## G Codes - Velocity Mode - ExactStop, Velocity Mode - Constant Velocity

<span class="new">[New! 0.9.8h]</span>  
Typically exact stop=G61, constant velocity=G64.

## G Codes - Workplane XY, Workplane XZ, Workplane YZ

<span class="new">[New! 0.9.8h]</span>  
Typically XY=G17, XZ=G18, YZ=G19.

## G Codes - X Mode Diameter

Used in the {$lathexmode} macro to specify that X values are in diameter mode. For example; EMC2 recognizes G7.

## G Codes - X Mode Radius

<span class="new">[New! 0.9.8h]</span>  
Used in the {$lathexmode} macro to specify that X values are in radius mode. For example; EMC2 recognizes G8.

## M Codes - End Rewind

Typically M30.

## M Codes - Repeat

<span class="new">[New! 0.9.8h]</span>  
Typically M47.

## M Codes - Spindle CW, Spindle CCW, Spindle Off

<span class="new">[New! 0.9.8h]</span>  
Typically CW=M3, CCW=M4, Off=M5.

## M Codes - Stop

<span class="new">[New! 0.9.8h]</span>  
Typically M0.

## Moves - Rapid, Feed Move, Arc CW, Arc CCW

These sections define how the commonly used gcode move instructions are formatted.

**Example:**

<property>Rapid</property> `{$g0} {$_f} {$_x} {$_y} {$_z} {$_a} {$_b} {$_c}`

<property>Feed Move</property> `{$_g1} {$_f} {$_x} {$_y} {$_z} {$_a} {$_b} {$_c}`

<property>Arc CW</property> `{$g2} {$_f} {$_x} {$_y} {$_z} {$i} {$j}`

<property>Arc CCW</property> `{$g3} {$_f} {$_x} {$_y} {$_z} {$i} {$j}`

<span class="note">Note:</span> The gcode operators {$g...} and their parameters can specified using an underscore ( _ ) prefix.  This is to show values that are modal (or sticky).
That is, they will only be output if the current value has changed.  Omitting the underscore will force the parameter to be always output.
{: .note }

## Canned Cycles - Drill, Drill Dwell, Drill Peck

These sections define how the commonly used canned cycle instructions are formatted.

<property>Drill</property> `{$g81} {$_x} {$_y} {$_z} {$_r} {$_f}`

<property>Drill Dwell</property> `{$g82} {$_x} {$_y} {$_z} {$p} {$_r} {$_f}`

<property>Drill Peck</property> `{$g83} {$_x} {$_y} {$_z} {$p} {$_q} {$_r} {$_f}`

## Lathe - Lathe Tool Radius Offset

If <value>False</value>, the toolpath at the center of the tool radius is output.

If <value>True</value>, an appropriate tool radius offset is applied.  The toolpath will be offset by a negative tool radius in the lathe X axis.  
The direction of the Z tool radius offset is determined by the cut direction. 
For right hand cuts the toolpath Z will be offset by a negative tool radius.  For left hand cuts, a positive tool radius Z offset is used.
            
![Tool radius offsets](../images/cam/tool-radius-offset.png)
            
In the diagram above, the red cross represents the toolpath reference point when <property>Lathe Tool Radius Offset</property> is set <value>True</value>.  If False, the dot
at the tool radius center will be the reference point.  The reference point is sometimes referred to as the 'Imaginary' or 'Virtual' tool point.

## Lathe - Lathe X Mode

For lathe operations, specifies whether X values are radius or diameter mode.

## Line Numbering

<span class="new">[New! 0.9.8N]</span>  
## Line Numbering - Add Line Numbers

If <value>True</value> then line numbers will be inserted at the beginning of g-code lines.

## Line Numbering - Line Number Format

Controls how the line number values are presented.  '0' characters denote a place holder that will contain either a significant digit or a 0.
A '#' character will output a significant digit or space character where there is no significant digit at that position.

## Line Numbering - Line Number Increment

Line numbers will be incremented by this amount each time a line number is added.

## Line Numbering - Line Number Prefix

This text (typically an 'N' character) will be written before the line number value.

## Line Numbering - Line Number Skip

Lines where the first non space character is in this list will not receive a line number.

## Line Numbering - Line Number Space After

If <value>True</value> then a space character will be inserted after the line number value.

## Line Numbering - Line Number Start

The initial for the first line number used.

## Options - Arc Output

Controls how arcs are output to gcode.  

If <value>Convert To Lines</value>, small line moves are used rather than arc commands.  
*Helix Convert To Lines*{:.value} is similar to <value>Convert To Lines</value>, but only for helical arcs (i.e. arcs with varying Z).

## Options - Arc Center Mode

<span class="new">[New! 0.9.8N]</span>  
This property controls whether the I and J parameters for arc moves (G2, G3) use absolute coordinates or incremental, relative to the arc end points.  
If this setting is different to the way the CNC controller interprets arc moves, the resulting toolpath may look a mess of random arcs in the controller.

*Default*{:.value} When default is set in the drawing's machining properties, the post processor Arc Center Mode will be used.
A default value in the post processor will use <value>Incremental (C-P1)</value>.

*Absolute*{:.value} I & J are absolute coordinates of the arc center point.

*Incremental (C-P1)*{:.value} I & J are coordinates of the arc center, offset from the first arc point.  This is the typical incremental mode.  
In previous versions this option was just called <value>Incremental</value>.

*Incremental (P1-C)*{:.value} I & J are offsets of the first arc point from the arc center.

*Incremental (C-P2)*{:.value} I & J are arc center offsets from the second arc point.

*Incremental (P2-C)*{:.value} I & J are offsets of the second arc point from the arc center.

## Options - Arc To Lines Tolerance

If <property>Arc Output</property>=<value>Convert To Lines</value> is used, this value controls the maximum allowed error when converting arcs to lines.  Smaller tolerances will result in smoother curves but larger files.

## Options - Clearance Plane Axis

Used to specify which direction clearance moves are made.  Usually Z, but may be set to X or Z for lathe operations.

## Options - Comment, End Comment

Defines the text to be used at the beginning and end of a comment.

**Example 1:**

*Comment*{:.prop}: (  
*End Comment*{:.prop}: )  

**Example 2:**

*Comment*{:.prop}: ;  
*End Comment*{:.prop}:

## Options - End Of Line

<span class="new">[New! 0.9.8h]</span>  
Character sequence used at the end of a line.  Escape code \r and \n can be used.

## Options - Invert Arcs

Controls the behaviour of XZ (G18) arcs only.

For milling operations, this should be set <value>False</value>.  The direction of the g-code arcs will then be relative to the positive Y axis (using 
a right hand coordinate system).

For lathe operations, this should usually be set <value>True</value>.  Lathe arc directions are typically relative to the 'Up' direction.
This would imply a positive Y axis using a left hand coordinate system.  CamBam's drawing view is a right hand coordinate system
so XZ arcs would need to be inverted when written to g-code.

Note: Coordinate handedness can be determined by pointing your thumb in the direction of positive X, second finger in the positive Y axis
and third (middle) finger in the positive Z axis direction.

## Options - Minimum Arc Length

A numerical value that controls the precision limits used for outputting arc moves (G2, G3).  If the length of an arc is less than the <property>Minimum Arc Length</property> value then a straight move (G1) is used instead.  
This is useful for TurboCNC users where very small arcs can cause glitches that may appear as dimples in the toolpath.

**Example:**  
*Minimum Arc Length*{:.prop}: 1e-4

## Options - Maximum Arc Radius

A numerical value that controls the maximum radius allowed for arc moves.  If an arcs radius exceeds this value, a straight line move (G1) is used.

**Example:**  
*Maximum Arc Radius*{:.prop}: 1e6

## Options - Number Format

This is a string formatting pattern that controls how floating point numbers are displayed.  

A hash character (#) denotes an optional digit place holder and a 0 character denotes a digit that is always displayed, adding padding zeros if required.

It can also change the gcode instructions that are required.  For example, if a toolpath contains a move from X=1.234 to X=1.233 and a number format of #.#0 is used, no move instruction will be written to the gcode as the coordinates are identical when formatted to 2 decimal places.

## Options - Rapid Down To Clearance

<span class="new">[New! 0.9.8i]</span>  
If set <value>True</value>, and Z is above the clearance plane a rapid down to the clearance plane is used.  
If <value>False</value> the current Z is maintained.

## Options - Suppress Parser Errors

<span class="new">[New! 0.9.8L]</span>  
The post processor will parse gcode as it is created to update internal values such as registers.  This can produce error messages for post processors that produce
non-standard gcode.  In many cases the gcode will still be correctly generated and the error messages can be ignored.

Setting <property>Suppress Parser Errors</property> to <value>True</value> will prevent the gcode parsing errors being displayed, which may otherwise hide genuine error messages.

## Options - Upper Case

If set to <value>True</value>, the post processor converts all text written to the gcode file to upper case.  This is particularly useful for Fanuc posts that do not support lower case characters.

## Post Build - Post-Build Command and Post-Build Command Args

<span class="new">[New! 0.9.8j]</span>  
*Post-Build Command*{:.prop} can be used to specify an external application to modify the gcode produced from the post processor.

*Post-Build Command Args*{:.prop} contains any arguments to pass to the application.  
The following macros are recognised:  
{$outfile} is the filename of the raw gcode output.  
{$cbfile.name} is the short name of the current CamBam document.

**Note:** Double quotes should be used in command arguments to avoid problems with spaces in filenames.

**Example:**  
*Post-Build Command*{:.prop}: `C:\bin\gcodelinenums.exe`

*Post-Build Command Args*{:.prop}: `"{$outfile}" "{$outfile}.out"`

## Rotary
<span class="new">[New! 1.0]</span>  

## Rotary - Axis Of Spin

The axis about which the stock is rotated.

## Rotary - Rotary Axis

The rotary (4th) axis letter.

## Rotary - Rotary Wrap

If **True**, allow Gcode to be 'wrapped' about a rotary axis.

 If Rotary Wrap is set to **True**, the post processor will convert all toolpaths to lines only, then wrap all the toolpaths selected around the rotary axis.

The radius of rotation is taken from each machining operations *Stock Surface* property. It is up to the user to make sure the toolpath is within a width of **2 x PI x stock surface**.

>**Note:** The post-processor should also be modified so as not to output the axis registers for the non rotational axis. For example, if rotating about the Y axis, the X{$x} move parameters should not be output.

see post processors *RotaryX.cbpp* and *RotaryY.cbpp* in the CamBam post folder

## Post Processor Macros

<table class="prop" cellspacing="0" width="100%">
<tr>
<td>
	<property>$arc.i</property><br>
	<property>$arc.j</property><br>
	<property>$arc.k</property><br>
	<span class="new">[New! 0.9.8L]</span>
</td>
<td>
	<p>Outputs the I, J or K register value of the current arc move.</p>
	<p>The register 'I', 'J' or 'K' prefix is not output.</p>
</td>
</tr>
<tr>
<td>
	<property>$arc.radius</property><br>
	<span class="new">[New! 0.9.8L]</span>
</td>
<td>
	<p>Outputs the radius of the current arc move.</p>
	<p>Arcs that sweep 0 to 180 degrees will have a positive radius and 
	arc sweeps &gt; 180 to 360 degrees will output a negative radius.</p>
</td>
</tr>
<tr>
<td>
	<property>$arc.start</property><br>
	<property>$arc.sweep</property><br>
	<property>$arc.end</property><br>
	<span class="new">[New! 0.9.8N]</span>
</td>
<td>
	<p>Outputs the start, end or sweep angle of the current arc move.</p>
	<p>Angles are measured in degrees, with 0 degrees along +X axis.</p>
	<p>CCW arcs will have a positive sweep angle. CW arcs will have a negative sweep angle.</p>
</td>
</tr>
<td>
	<property>$arccentermode</property><br>
</td>
<td>
	<p>Outputs <i>Arc Center Absolute</i> (typically <b>G90.1</b>) or <i>Arc Center Incremental</i> (typically <b>G91.1</b>)  depending of the <i>Arc Center Mode</i> selected</p>
</td>

<tr>
<td>
	<property>$blocks</property>
</td>
<td>
	This macro is generated internally and contains all the move instructions required by the current machine operation (MOP)'s.
</td></tr>
<tr>
<td>
	<property>$comment</property>
</td>
<td>
	Inserts the text defined in the <property>Comment</property> section of the post processor.
</td>
</tr>
<tr>
<td>
	<property>$cbfile.footer</property>
</td>
<td>
	Inserts the drawing’s <property>Custom File Footer</property> Machining option.
</td>
</tr>
<tr>
<td>
	<property>$cbfile.header</property>
</td>
<td>
	Inserts the drawing’s <property>Custom File Header</property> Machining option.
</td>
</tr>
<tr>
<td>
	<property>$cbfile.name</property>
</td>
<td>
	Inserts the drawing’s <property>Name</property> property.
</td>
</tr>
<tr>
<td>
	<property>$check(x,y,z)</property>
</td>
<td>
	Generated internally, this macro checks the x,y,z coordinate parameters against the current tool location.  
	If different, a sequence of moves will be inserted to move to the new position, using the clearance plane and plunge feed rates where necessary.
</td>
</tr>
<tr>
<td>
	<property>$clearance</property>
</td>
<td>
	Rapids to the clearance plane.
</td>
</tr>
<tr>
<td>
	<property>$cuttercomp(off|L|R)</property>
</td>
<td>
	<p>Cutter radius compensation.  <b>Note:</b> CamBam does not currently calculate radius compensation codes for toolpaths.</p>
	<p>Inserts the text defined in the <property>Cutter Comp Off</property>, <property>Cutter Comp Left</property> or <property>Cutter Comp Right</property> sections of the post processor.</p>
	<p>Typically Off=G40, L=G41, R=G42</p>
</td>
</tr>
<tr>
<td>
	<property>$date</property>
</td>
<td>
	Inserts the current date time stamp
</td>
</tr>

<tr>
<td>
	<property>$distancemode</property>
</td>
<td>
	<p>Inserts the distance mode in use.  The values are defined in the <property>Distance Absolute</property> and <property>Distance Incremental</property> sections of the post processor.</p>
	<p>Currently this always equates to <property>Distance Absolute</property> (typically G90).</p>
</td>
</tr>
<tr>
<td>
	<property>$endcomment</property>
</td>
<td>
	Inserts the text defined in the <property>End Comment</property> section of the post processor.
</td>
</tr>
<tr>
<td>
	<property>$endrewind</property>
</td>
<td>
	<p>Inserts the text defined in the <property>End Rewind</property> section of the post processor.</p>
	<p>Typically M30.</p>
</td>
</tr>
<tr>
<td>
	<property>$footer</property>
</td>
<td>
	Evaluates the text in the <property>Footer</property> section of the post processor.
</td>
</tr>
<tr>
<td>
	<property>$g0, $g1, $g2, $g3</property><br />
	<property>$g81, $g82, $g83</property><br />
	<property>$_g0, $_g1, $_g2, $_g3</property><br />
	<property>$_g81, $_g82, $_g83</property>
</td>
<td>
	<p>These Gcode macros control how the gcodes are output.  The format of each code is taken from the <property>G...</property> definitions in the post processor.
	This may be useful to control zero padding (eg G1 vs G01), or to use alternative G codes.</p>
    <p>If the underscore (_) prefix is used, these instructions are assumed to be modal (or sticky).  That is; the first occurrence of the code will 
	be written but omitted if following blocks use the same instruction.</p>
</td>
</tr>
<tr>
<td>
	<property>$header</property>
</td>
<td>
	Evaluates the text in the <property>Header</property> section of the post processor.
</td>
</tr>
<tr>
<td>
	<property>$mop.clearanceplane</property><br>
	<span class="new">[New! 0.9.8L]</span>
</td>
<td>
	Outputs the <property>Clearance Plane</property> value of the current machining operation.
</td>
</tr>
<tr>
<td>
	<property>$mop.cutfeedrate</property><br>
	<span class="new">[New! 0.9.8L]</span>
</td>
<td>
	<p>Outputs the <property>Cut Feedrate</property> value of the current machining operation.</p>
	<p>The 'F' register code prefix is not output.</p>
</td>
</tr>
<tr>
<td>
	<property>$mop.depthincrement</property><br>
	<span class="new">[New! 0.9.8L]</span>
</td>
<td>
	Outputs the <property>Depth Increment</property> value of the current machining operation.
</td>
</tr>
<tr>
<td>
	<property>$mop.dwell</property><br>
	<span class="new">[New! 0.9.8L]</span>
</td>
<td>
	Outputs the <property>Dwell</property> value of the current drilling operation.
</td>
</tr>
<tr>
<td>
	<property>$mop.first.x</property><br>
	<property>$mop.first.y</property><br>
	<property>$mop.first.z</property><br>
	<span class="new">[New! 0.9.8N]</span>
</td>
<td>
	<p>Insert the X, Y or Z coordinate of the first toolpath point of the current machining operation.</p>
    <p>This macro may be useful after a tool change command, to move to the next machining X, Y coordinate
	at the tool change height, before plunging to the clearance plane.</p>
</td>
</tr>
<tr>
<td>
	<property>$mop.footer</property>
</td>
<td>
	Inserts the current machining operation's <property>Custom MOP Footer</property> property.
</td>
</tr>
<tr>
<td>
	<property>$mop.header</property>
</td>
<td>
	Inserts the current machining operation's <property>Custom MOP Header</property> property.
</td>
</tr>
<tr>
<td>
	<property>$mop.holediameter</property><br>
	<span class="new">[New! 0.9.8L]</span>
</td>
<td>
	Outputs the <property>Hole Diameter</property> value of the current drilling operation.
</td>
</tr>
<tr>
<td>
	<property>$mop.name</property>
</td>
<td>
	Inserts the current machining operation's <property>Name</property> property.
</td>
</tr>
<tr>
<td>
	<property>$mop.peckdistance</property><br>
	<span class="new">[New! 0.9.8L]</span>
</td>
<td>
	Outputs the <property>Peck Distance</property> value of the current drilling operation.
</td>
</tr>
<tr>
<td>
	<property>$mop.plungefeedrate</property><br>
	<span class="new">[New! 0.9.8L]</span>
</td>
<td>
	<p>Outputs the <property>Plunge Feedrate</property> value of the current machining operation.</p>
	<p>The 'F' register code prefix is not output.</p>
</td>
</tr>
<tr>
<td>
	<property>$mop.retractheight</property><br>
	<span class="new">[New! 0.9.8L]</span>
</td>
<td>
	Outputs the <property>Retract Height</property> value of the current drilling operation.
</td>
</tr>
<tr>
<td>
	<property>$mop.stocksurface</property><br>
	<span class="new">[New! 0.9.8L]</span>
</td>
<td>
	Outputs the <property>Stock Surface</property> value of the current machining operation.
</td>
</tr>

<tr>
<td>
	<property>$mop.tag</property><br>
	<span class="new">[New! 0.9.8L]</span>
</td>
<td>
	Outputs the <property>Tag</property> value of the current machining operation.
</td>
</tr>

<tr>
<td>
	<property>$mop.targetdepth</property><br>
	<span class="new">[New! 0.9.8L]</span>
</td>
<td>
	Outputs the <property>Target Depth</property> value of the current machining operation.
</td>
</tr>
<tr>
<td>
	<property>$move.x</property><br>
	<property>$move.y</property><br>
	<property>$move.z</property><br>
	<span class="new">[New! 0.9.8L]</span>
</td>
<td>
	<p>Outputs the X, Y or Z register value of the current move.</p>
	<p>The register code is not output.</p>
</td>
</tr>
<tr>
<td>
	<property>$mops</property>
</td>
<td>
	Inserts a list of objects, one item for each enabled machining operation.<br>
	Each list item is defined using the <property>MOP</property> section definition of the post processor.
</td>
</tr>
<tr>
<td>
	<property>$part.name</property>
</td>
<td>
	Inserts the name of the current part.
</td>
</tr>
<tr>
<td>
	<property>$post.toolchange</property><br>
	<span class="new">[New! 0.9.8N]</span>
</td>
<td>
	Inserts the post processor tool change macro.  This may be useful to include in the tool definition <property>Tool Change</property> property.
</td>
</tr>

<tr>
<td>
	<property>$repeat</property>
</td>
<td>
	<p>Inserts the text defined in the <property>Repeat</property> section of the post processor.</p>
	<p>Typically M47.</p>
</td>
</tr>
<tr>
<td>
	<property>$s</property>
</td>
<td>
	Inserts the current machining operation's <property>Spindle Speed</property> property.
</td>
</tr>
<tr>
<td>
	<property>$set(x|y|z|a|b|c|f|p|q|r,&lt;value&gt;)</property><br>
	<span class="new">[New! 0.9.8L]</span>
</td>
<td>
	<p>Sets the current value of the specified X, Y or Z register.  No gcode will be output.</p>
    <p>Example:</p>
    <p>
	<pre>$set(z,5.5)</pre>
    </p>
    <p>
	This may be useful after a custom, controller based tool change macro, to inform the post processor
	of the controller's new coordinates.
    </p>
    <p>
	The value NaN can also be used to set the register to an undefined state.
    </p>
</td>  
</tr>
<tr>
<td>
	<property>$spindle</property>
</td>
<td>
	<p>Inserts a macro depending on the current machine operation's <property>Spindle Direction</property> property.</p>
	<p>Nothing will be written to the gcode if the spindle is already in this state.</p>
</td>
</tr>
<tr>
<td>
	<property>$spindle(off|cw|ccw)</property>
</td>
<td>
	<p>Inserts the text defined in the <property>Spindle Off</property>, <property>Spindle CW</property> or <property>Spindle CCW</property> sections of the post processor.</p>
	<p>Typical values are cw=M3, ccw=M4, off=M5</p>
</td>
</tr>
<tr>
<td>
	<property>$stock.xsize</property><br>
    <property>$stock.width</property><br>
    <property>$stock_width</property>
</td>
<td>
	<p>The X size of the stock block defined in the Machining or Part object.</p>
	<p>Example: (For CutViewer STOCK definition)</p>
    <p>
	{$comment} STOCK/BLOCK,{$stock_width},{$stock_length},{$stock_height},{$stock_x},{$stock_y},{$stock_z} {$endcomment}
    </p>
</td>
</tr>
<tr>
<td>
	<property>$stock.ysize</property><br>
    <property>$stock.length</property><br>
    <property>$stock_length</property>
</td>
<td>
	The Y size of the stock block defined in the Machining or Part object.
</td>
</tr>
<tr>
<td>
	<property>$stock.zsize</property><br>
    <property>$stock.height</property><br>
    <property>$stock_height</property>
</td>
<td>
	The Z size of the stock block defined in the Machining or Part object.
</td>
</tr>
<tr>
<td>
	<property>$stock.xoffset</property><br>
</td>
<td>
	The X coordinate of the lower left corner of the stock block (relative to the machine's XY(0,0)), defined in the Machining or Part object.
</td>
</tr>
<tr>
<td>
	<property>$-stock.xoffset</property><br>
    <property>$stock_x</property>
</td>
<td>
	The <i>minus</i> X coordinate of the lower left corner of the stock block (relative to the machine's XY(0,0)), defined in the Machining or Part object.
</td>
</tr>
<tr>
<td>
	<property>$stock.yoffset</property>
</td>
<td>
	The Y coordinate of the lower left corner of the stock block (relative to the machine's XY(0,0)), defined in the Machining or Part object.
</td>
</tr>

<tr>
<td>
	<property>$-stock.yoffset</property><br>
    <property>$stock_y</property>
</td>
<td>
	The <i>minus</i> Y coordinate of the lower left corner of the stock block (relative to the machine's XY(0,0)), defined in the Machining or Part object.
</td>
</tr>
<tr>
<td>
	<property>$stock_z</property>
</td>
<td>
	The <i>minus</i> Z coordinate of the lower left corner of the stock block (relative to the machine's XY(0,0)), defined in the Machining or Part object.
</td>
</tr>
<tr>
<td>
	<property>$stop</property>
</td>
<td>
	<p>Inserts the text defined in the <property>Repeat</property> section of the post processor.</p>
	<p>Typically M0.</p>
</td>
</tr>
<tr>
<td>
	<property>$tool.comment</property>
	<br><span class="new">[New! 0.9.8N]</span>
</td>
<td>
	Inserts the <property>Comment</property> property from the tool library for the current tool.
</td>
</tr>
<tr>
<td>
	<property>$tool.diameter</property>
</td>
<td>
	<p>Inserts the current machining operation's <property>Tool Diameter</property> property.</p>
    <p><b>Note:</b> The $tool.diameter macro will not be defined until there has been a tool change command.
	If used in the header section, use a tool change such as <property>$toolchange(first)</property> before referring to <property>$tool.diameter</property>.</p>
</td>
</tr>
<tr>
<td>
	<property>$tool.index</property>
</td>
<td>
	Inserts the current machining operation's <property>Tool Number</property> property.
</td>
</tr>
<tr>
<td>
	<property>$tool.length</property>
</td>
<td>
	Inserts the tool length property from the tool definition in the tool library.
</td>
</tr>
<tr>
<td>
	<property>$tool.name</property><br>
	<span class="new">[New! 0.9.8N]</span>
</td>
<td>
	Inserts the current tool's <property>Name</property> property (from tool library) or T(tool number) if there is no tool library entry.
</td>
</tr>
<tr>
<td>
	<property>$tool.profile</property><br>
	<span class="new">[New! 0.9.8L]</span>
</td>
<td>
	Inserts the <property>Tool Profile</property> property of the current tool.
</td>
</tr>
<tr>
<td>
	<property>$tool.radius</property>
</td>
<td>
	Uses the current machining operation's <property>Tool Profile</property> property to determine a radius.  
	0 for end mills and Diameter / 2 for bullnose.
</td>
</tr>
<tr>
<td>
	<property>$tool.veeangle</property><br>
	<span class="new">[New! 0.9.8N]</span>
</td>
<td>
	Inserts the current tool's <property>Vee Angle</property> property (from tool library) or 0 if there is no tool library entry.
</td>
</tr>
<tr>
<td>
	<property>$toolchange</property>
</td>
<td>
	Inserts a tool change instruction, based on the <property>Tool Change</property> definition in the post processor.<br>
	If the tool number has not changed, no tool change code is inserted.
</td>
</tr>
<tr>
<td>
	<property>$toolchange(first)</property>
</td>
<td>
	Inserts a tool change instruction using the first tool in the current drawing's tool table.
</td>
</tr>
<tr>
<td>
	<property>$tooltable</property>
</td>
<td>
	Inserts a description for each tool that is referenced in the current drawing.<br>
	Each item in the list is formatted using the <property>Tool Table Item</property> definition in the post processor.
</td>
</tr>
<tr>
<td>
	<property>$units</property>
</td>
<td>
	<p>Outputs the drawing's <property>Units</property> property.</p>
	<p>The codes used are taken from the <property>Units (Inches)</property> or <property>Units (Metric)</property> sections of the post processor.</p>
	<p>Typically Inches = G20, Millimeters = G21.</p>
</td>
</tr>
<tr>
<td>
	<property>$velocitymode</property>
</td>
<td>
	<p>Inserts the current machining operation's <property>Velocity Mode</property> property.</p>
	<p>The codes used are taken from the <property>Velocity Mode - Constant Velocity</property> or <property>Velocity Mode - Exact Stop</property> sections of the post processor.</p>
	<p>For example: Mach3 uses Exact Stop=G61, Constant Velocity=G64.</p>
</td></tr>
<tr>
<td>
	<property>$workplane</property>
</td>
<td>
	<p>Inserts the current machining operation's <property>Work Plane</property> property.</p>
	<p>The codes used are taken from the <property>Workplane XY | XZ | YZ</property> sections of the post processor.</p>
    <p>Typically XY=G17, XZ=G18, YZ=G19.</p>
</td>
</tr>
<tr>
<td>
	<property>$x, $y, $z, $a, $b, $c</property><br>
	<property>$i, $j, $f, $r, $p, $q</property><br>
	<property>$_x, $_y, $_z, $_a, $_b</property><br>
	<property>$_c, $_i, $_j, $_f, $_r</property><br>
	<property>$_p, $_q</property>
</td>
<td>
	<p>These macros insert the parameters used in common Gcode move operations.<br>
	If an underscore (_) prefix is used, these parameters are treated as modal.<br>
	That is they will only be output if the current value has changed.<br>
	Omitting the underscore will force the parameter to be always output.</p>
	<p>These macros will include the register code as well as the value, for example $x = X1.23</p>
</td>
</tr>
<tr>
<td>
	<property>$xneg, $yneg, $zneg, ...</property><br>
	<span class="new">[New! 0.9.8h]</span>
</td>
<td>
	The same as the other register macros ($x, $_y etc), but with the value sign reversed.
</td>
</tr>
<tr>
<td>
	<property>$xabs, $yabs, $zabs, ...</property><br>
	<span class="new">[New! 0.9.8h]</span>
</td>
<td>
	The same as the other register macros ($x, $_y etc), but with the value always positive.
</td>
</tr>
</table>
