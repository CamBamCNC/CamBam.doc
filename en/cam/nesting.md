---
title: Nesting
---

# Nesting

CamBam's nesting provides a method to repeat a series of machining operations at different positions on the machine table.
In this way, it is possible to produce multiple copies of a workpiece from a single drawing and one set of machining operations.

Nesting operates on the contents of an entire [Part](part.md){:.name}. All machining operations contained in a part will be machined at the positions 
and order defined in the *Nesting*{:.name} property of that part.

## Implementation of a nesting 

First create one or more machining operations.

For this example, we will use a single operation; an external cut of a rectangle of 10 x 30 mm, with a tool of 2 mm diameter.

The following image shows the [Profile](profile.md){:.name} operation, with tabs and cut widths of the tool visible.

![](../images/cam/nesting/nest-en_01.png){:.icenter}


## Setting the nesting

All the properties defining the nesting are located at the [Part](part.md){:.name} object.

Select the Part containing the Machining Operations (MOP) that are to be nested.

![](../images/cam/nesting/nest-en_02.png){:.icenter}


In the part properties, click the + sign to the left of the *Nesting* property to display all options.

### Nest Method

The *Grid*{:.prop} and *ISO Grid*{:.prop} methods are used to distribute copies of machining operations in **rows** and **columns** automatically. 
The ISO Grid method additionally adds a horizontal / vertical shift of half the pitch at each row / column. 
The shift is performed on the lines or the columns depending on the value of *Grid Order*{:.prop}.

![](../images/cam/nesting/nest-en_03.png){:.icenter}

> **Note:** Copies are shown as a dotted outline of the same color as used for the display of the cut widths (from revsion N), 
> with a red triangle in each center as shown in the picture below.
> You can also activate the menu *View/ Show cut widths*{:.cmd}, allowing a better view of the copies as on the images above.
{:.note}

![](../images/cam/nesting/nest-en_04.png){:.icenter}

> **Note:** The Part must be selected in the drawing tree, for the copies to be visible.
{:.note}

To create the nesting of this example, select a type of grid, enter the number of *Rows*{:.prop} and *Columns*{:.prop} required, 
a *Spacing*{:.prop} value, and then generate the toolpaths of the *Part*{:.name} via its context menu. 

> **Important:** The *Spacing*{:.prop} value defines the distance between the outermost toolpaths of the active machining operation contained in the Part.
{:.note}

![](../images/cam/nesting/nest-en_05.png){:.icenter}

In the picture above, with *Show Cut Widths* enabled, a distance of 5 mm is visible, between the toolpaths, leaving 3mm of stock between the widths of the 2mm tool diameter cut.

### Grid order

*Grid Order*{:.name} controls the direction of the grid layout. For example *Right Up*{:.val} will make copies to the right of the original, 
then move up to the next row.

![](../images/cam/nesting/nest-en_06.png){:.icenter}

### Grid Alternate

If *Grid Alternate*{:.prop} is set to *True*{:.val}, the grid will alternate the direction of each row or column (depending on *Grid Order*{:.prop}). 
If *False*{:.val} then each row or column will proceed in the same direction, with a rapid back to the start of each.

![](../images/cam/nesting/nest-en_07.png){:.icenter}

## Moving the copies

Nested copies can also be positioned manually.

Click and hold the left button of the mouse over the red triangle of a copy, then drag it to the desired position. 
The *Nest Method*{:.prop} property will automatically change to *Manual*{:.val}.

Note that if you move the original copy, the original will always appear at the same position on the display (but without the red triangle),
but **will not be machined** as the image of the simulation below shows (the black arrow indicates the displacement of the original).
Only the nested copies will be output to the gcode.

![](../images/cam/nesting/nest-en_08.png){:.icenter}

You can also edit the location of copies, delete or add, modify the order in which they will be machined using the Nesting Item collection editor.

Set the *Nest Method*{:.prop} to *Manual*{:.val}, and then edit the *(collection)* object. The *Advanced* properties view needs to be enabled to access this.

![](../images/cam/nesting/nest-en_21.png){:.icenter}

The left side of the window allows you to add or remove copies using the *Add*{:.cmd} and *Delete*{:.cmd} buttons.

With the two vertical arrows to the right of the list you can change the order of the list, and thus the machining order of the copies, by selecting a copy then clicking on an arrow to move up or down.

The right part of the nesting collection editor allows you to edit the coordinates of each copy.

## Using a points list to define a nesting

It is possible to use a points list drawing object to define the positions of nesting copies, by using the *Nesting Method*{:.prop} set to *Point List*{:.val}, 
then assigning the *ID*{:.name} of the point list in the *Point List ID*{:.prop} property.

The default coordinates of the nesting system are at the drawing origin, (at the center of the X and Y axes),
unless an alternative *Machining Origin* has been set (shown by a small red cross), either on the [Part](part.md){:.name} or [Machining options](machining-options){:.name}.

The image below shows the points corresponding to different copies.

![](../images/cam/nesting/nest-en_09.png){:.icenter}

It is also possible to generate a list of corresponding points to an existing nesting. This is what I did to get the point list corresponding to the different copies of the image above.

Copies having been previously created with the automatic placement obtained with the Grid method.

For create this point list, open the context menu of the relevant **Part**, and use the menu item: **Nest to Point list**

![](../images/cam/nesting/nest-en_10.png){:.icenter}

### Why use a point list ?

There are some cases where the use of a Point list is required.

- If you want to assign a value from center to center distance between the copies rather than spacing between them, (or more precisely, between the outermost toolpaths).

- If a specific placement is needed, such as the result of a mathematical calculation.
The point list can be created in a spreadsheet and then imported into CamBam.

- If several different Parts need the same nesting placement.

In these cases the spacing between copies is not usable as it is based on the distance between the outermost toolpath of the Part, 
which can be different from one Part to another, depending on the machining operations that compose it.

If each Part uses the same, common point list object for their nesting, their nested copies will have identical positioning.

> **Note:** If some machining operations are disabled in the Part, they will not be taken into account when calculating the spacing between copies.
> Enabling and Disabling machining operations can alter the nest spacing when *Auto*{:.val} nesting methods are used, whenever the toolpaths are regenerated.
{:.note} 

The *Nest to Point list*{:.cmd} function on the Part context menu can be used to generate a suitable placement point list. 
The resulting point list can then be assigned to all other Parts that must use the same nesting pattern

In this image, the *Grid*{:.val} nesting method is used with the same parameters for both Part's nesting properties.
As can be seen, the operations of the second Part (the small rectangle) are not executed in the right location.

![](../images/cam/nesting/nest-en_11.png){:.icenter}

After generating a point list for *"Part1"*, and assigning this point list to the both Parts, and setting *Nest Method*{:.prop} to *Point list*{:.val}
everything is in order.

![](../images/cam/nesting/nest-en_12.png){:.icenter}

### Output order of machining operations

The *GCode Order*{:.prop} property is used to define the order in which multiple machining operations contained in a Part are executed. 

In the following example, the machining *"Pocket1"* (an island) and the cutting *"Profile1"* are machined with the same tool of 8mm diameter, 
the operation *"Drill1"* is performed with a tool of 2mm diameter, so there is a tool change at the beginning of the *"Drill1"* operation.

![](../images/cam/nesting/nest-en_16.png){:.icenter}

*GCode Order*{:.prop} = *Auto*{:.val} - All consecutive MOPs within the part *with the same tool number* will be posted, then repeated for each nest copy, 
before moving to the next MOP (which requires a tool change).

![](../images/cam/nesting/nest-en_14.png){:.icenter}

*GCode Order*{:.prop} = *Nest Each MOP*{:.val} - Each MOP is output at each nest location before moving to the next MOP.

![](../images/cam/nesting/nest-en_13.png){:.icenter}

*GCode Order*{:.prop} = *All MOPs Per Copy*{:.val} - All the MOPs in the part are posted before moving to the next nest location.

![](../images/cam/nesting/nest-en_15.png){:.icenter}

<a name="4axis"></a> 
## Use of the nesting with a 4th rotary axis 

To use the nesting system with a rotary axis to reproduce the same machinings at different angular positions, 
use a point list containing the same number of points as the number of copies to be made, **with each point's X and Y coordinates set to 0**.

The machining operations in the Part will then be repeated at the same place.
Add a Gcode command to the *Custom MOP Footer*{:.prop} of the last machining operation of the Part to rotate the 4th axis 
the desired angle before performing the following sequence of machining operations.

#### Example:

To machine 6 grooves distributed around a shaft of 50mm diameter.

Create a *Pocket*{:.name} operation with a *Stock surface*{:.prop} value equal to the radius of the workpiece to be machined, 
in this example *Stock surface*{:.prop} = 25 (Z=0 is set at the axis of the chuck on the CNC rotary axis).

![](../images/cam/nesting/nest-en_17.png){:.icenter}

Create a Point list with 6 points, all at (X,Y,Z) = 0,0,0.

![](../images/cam/nesting/nest-en_18.png){:.icenter}

Define a nesting with the *Nest method*{:.prop} = *Point list*{:.val} for this Part, then assign to it the point list created above.

The pocketing operation will be repeated six times in the same place.

To rotate the chuck at the end of the part's operations, the following Gcode sequence is added in the *Custom MOP Footer*{:.prop} property 
of the last machining operation in the Part.

![](../images/cam/nesting/nest-en_19.png){:.icenter}

Go back to the clearance plane:
~~~
G0 Z30
~~~

Use relative coordinates so as to give a rotation angle rather than an absolute position:
~~~
G91
~~~

Rotate the A axis of 60° i.e. 360/6:
~~~
G0 A60
~~~

Change back to absolute coordinates:
~~~
G90
~~~

You can also use a [NCFile](back-plotting.md) object to put the Gcode that produces the rotation instead of putting it into the <property>Custom MOP Footer</property> property.
In this case the GCode must be created in a separate text file.
You can create it with the CamBam text editor. (<span class="cmd">Scripts / New / VBscript</span> ; 
delete the existing text, enter your Gcode and save the file with a .txt, .nc, or .ngc extension ....)

The result cannot be seen in *CamBam* or *CutViewer*, but is visible in the Mach3 simulation window .

![](../images/cam/nesting/nest-en_20.png){:.icenter}

To see the list of properties of the nesting system, click [HERE](part.md)
