---
title: Drawing Surfaces
---
# Drawing Surfaces

The 3D surface and solid modelling functionality of CamBam is limited, but there are number of helpful 3D drawing routines.
These are available from the <span class="cmd">Draw - Surface</span> menu.

## From Mesh File

This inserts a 3D surface from an STL file.

## From Bitmap

Converts a bitmap image into a 3D surface by using the brightness levels to define the Z heights.

This is a similar process used by the [Heightmap Plugin](../tutorials/heightmap.md){:.name}, but where the plugin will only generate an engraving toolpath,
the <span class="cmd">draw surface from bitmap</span> routine will create a 3D mesh that can be used to create more sophisticated 3D machining
operations such as waterline and scanline roughing.

![](../images/frombitmap_win.png){:.icenter}

Click the ![](../images/frombitmap_button.png) button to open an image file.

*Heightmap Size*{:.prop}: 
Set the X and Y dimensions (in current drawing units) of the surface to be generated.  If the X or Y dimension is left at 0, this dimension
will be automatically calculated so as to preserve the aspect ratio of the original image.

*Grid Stepover*{:.prop}: Controls the size of each triangular facet that will be used to build the surface.  If this is set to 0, the 
size will be based on one image pixel size.

*Z Height range*{:.prop}: The minimum and maximum Z heights that correspond to the lightest and darkest parts of the image.

*Invert*{:.prop}: When unticked, dark areas represent low Z values and light areas higher Z areas.  If invert is ticked 
this is reversed.

Click <span class="cmd">Create surface</span> to generate the 3D mesh into the current drawing.

![](../images/frombitmap_result.png){:.icenter}

## From text file

Allows you to use a plain text file (ASCII) providing a list of coordinates representing the triangle faces of a 3D object.

Each line consists of nine coordinates, separated by a space, corresponding to the coordinates X, Y and Z of three vertices defining a triangle.

**Example**:
~~~
0 0 0 0 20 0 30 0 0
~~~

This defines 3 points : Point1 (x,y,z) = 0,0,0 Point2 = 0,20,0 and Point3 = 30,0,0

This file gives the following result:

![](../images/triangle_raw.png){:.icenter}

## Extrude

Extrude is used to create a 3D surface from a 2D line by projecting it in the Z direction.

This operation was originally added to create shapes for use as holding tabs or 'sprues' on 3D machining operations.

To create extrusions along other axis, the shape must first be extruded in Z, then rotate the extruded surface object to the required orientation.

The <span class="cmd">Extrude</span> operation will first prompt for an <property>Extrusion Height</property>.  This will be the Z height of the extruded surface.
A positive height will extend toward the positive Z axis (ie toward the viewer when drawing is in the normal orientation with the XY
plane parallel to the screen).  A negative height will extend the surface along the negative Z axis (ie into the screen).

The routine next prompts for the <property>Extrusion Steps</property>.  This controls the number of steps around the source shape to insert faces
on the extruded surface.  More steps will result in a smoother surface.

This following image of a circle extrusion shows steps of 10, 30 and 100.

![](../images/extrude_step.png){:.icenter}

Another example of an extruded polyline.

![](../images/extrude.png){:.icenter}

## Extrude solid <span class="new">New 1.0</span>

Like extrude, but the ends of the shape are closed in order to produce a "solid" object that can be exported to STL for printing.

A window will ask you to enter a thickness for the shape, then an <property>Arc Expand Tolerance </property>, as for simple extrusion.

![](../images/Extrudesolids.png){:.icenter}

## Fill <span class="new">New 1.0</span>

Fill the selection with a full 2D surface. (without thickness)

A window will ask you to enter an <property>Arc Expand Tolerance</property>, as for extrusion.

![](../images/surface_fill.png){:.icenter}
