---
title: Edit Polyline
---
# Edit Polyline

This section describes the operations available from the <span class="cmd">Edit - Polyline</span> menu.

## Edit

Modifies selected polylines by allowing the control points to be dragged interactively in the drawing view.

The polyline edit mode can also be entered by double clicking a polyline in the drawing view.

![](../images/edit_poly.png){:.icenter width="750" height="214"}

## Reverse

Reverses the order of the points within a polyline.

Reverse is useful in situations where there is a toolpath such as a profile, offset from an open polyline.  For
open polylines, the order of the points within the polyline will dictate which side of the polyline the toolpath will be offset from.
As an alternative to changing the profile machining operations <property>Inside / Outside</property> property, the source polyline can
be reversed.  This will change the side of the polyline where the toolpath is offset.

![](../images/ligne_reverse.png){:.icenter} 

## Clean

Removes duplicate points from a polyline.

## Break at points

![](../images/breakatpoints.png){:.icenter}  

Break a polyline at a set of points.

Select the polylines to cut, and also select the point lists that will define the cutting points and then select the <span class="cmd">Edit - Polyline - Break at Points</span> menu option.

## Set start point

Changes the first polyline control point, (for closed polylines only).

## Arc Fit

Arc fit will attempt to simplify a polyline by replacing a number of small segments with a single circular arc segment, that fits 
the polyline control points according to a specified tolerance.  In some cases this can dramatically reduce the number of segments
in the polyline, resulting in faster toolpath calculations and more compact gcode.  The use of large arc segments rather than
many small segments can also make the machining operations much smoother when cutting.

Arc fit will prompt for an <property>Arc Fit Tolerance</property>.  This is the maximum allowed deviation (in drawing units) from the fitting arc to the original segments.
A larger tolerance can result in polylines with fewer segments but the deviation from the original shape is potentially greater.

The following images show the effect of different tolerances on a polyline arc fit.
 

![](../images/arcfit.png){:.icenter width="750" height="469"} 

## Remove overlaps

Overlaps are polyline segments that back track along the polyline and then backtrack again, much like a compressed Z shape.
These can cause problems for some of the routines in CamBam, such as polyline joining and toolpath generation.

These problems are commonly found in drawings that have been converted from bitmaps using vectorization software.
These overlaps may be very small and hard to spot.

The <span class="cmd">Remove overlaps</span> operation will create a copy of the source polyline in the active layer, with any detected overlaps removed.
If the original polyline is used by a machining operation, the machining operation source objects will need to be reselected to choose
the cleaned polyline.

> **Note:** In the latest version of CamBam, the toolpath generation routines will automatically attempt to detect and fix any
back tracks in polylines before creating offset polylines from them.  In many cases the manual <span class="cmd">Remove overlaps</span> operation 
will not be needed.  The automatic checks can be disabled in the system configuration settings by setting 
[Offset Backtrack Check](../configuration.md#OffsetBacktrackCheck){:.prop} property to <value>False</value>.
{:.note}




 
