---
title: Machining Options
---
# Machining Options

Parameters that control how machining operation toolpaths are generated, as well as how gcode is produced, can be set by selecting the <span class="name">Machining</span> folder
in the drawing tree and inspecting the property window.

> **Note:** In earlier CamBam versions, settings that controlled how toolpaths were displayed were also found 
in the Machining options.  In version 0.9.8, these have been moved to the top level _Drawing_ object of the file tree and are also accessible from the _View_ menu.
{:.note }

## Properties

<table class="prop" cellspacing="0" width="100%">
<tr>
<td>
	<property>Arc Center Mode</property>
</td>
<td>
<p>This property controls whether the I and J parameters for arc moves (G2, G3) use absolute coordinates or incremental, relative to the arc end points.  
If this setting is different to the way the CNC controller interprets arc moves, the resulting toolpath may look a mess of random arcs in the controller.</p> 
<p><value>Default</value> When default is set in the drawing's machining properties, the post processor Arc Center Mode will be used.  
A default value in the post processor will use <value>Incremental (C-P1)</value>.</p>
<p><value>Absolute</value> I & J are absolute coordinates of the arc center point.</p>
<p><value>Incremental (C-P1)</value> I & J are coordinates of the arc center, offset from the first arc point.  This is the typical incremental mode.  
In previous versions this option was just called <value>Incremental</value>.</p>  
<p><value>Incremental (P1-C)</value> I & J are offsets of the first arc point from the arc center.</p>
<p><value>Incremental (C-P2)</value> I & J are arc center offsets from the second arc point.</p>
<p><value>Incremental (P2-C)</value> I & J are offsets of the second arc point from the arc center.</p>  
</td>
</tr>
<tr>
<td>
	<property>Custom File Footer</property>
</td>
<td>
	This text is inserted at the end of the gcode output.  It can contain multiple text lines or pipe characters '|' to denote new lines.  
	It can also contain $macros.  Common available macros are described in the post processor section.
</td>
</tr>
<tr>
<td>
	<property>Custom File Header</property>
</td>
<td>
	This text is inserted at the beginning of the gcode output.  It can contain multiple text lines or pipe characters '|' to denote new lines.  
	It can also contain $macros.  Common available macros are described in the post processor section.
</td>
</tr>

<tr>
<td>
	<a name="FastPlungeHeight"><property>Fast Plunge Height</property></a>
</td>
<td>
	<p>This value is used when moving down to the stock surface or next cutting level.</p>
	<p>If set to 0, the current machining operation's <property>Plunge Feedrate</property> is used (which can result in slow machining times).</p>
	<p>If a non zero <property>Fast Plunge Height</property> is specified, a rapid move is used (G0) to the specified height above the stock.  
	This can significantly improved cutting times in some files. A typical example might be 0.1 or Metric or 0.004 for Inches.
	The default value is (-1), which will use one minor grid unit as the fast plunge height.</p>
</td>
</tr>
<tr>
<td>
	<property>Holding Tabs:<br/>Inner Tab Scale, Outer Tab Scale</property>
	<br /><span class="new">New! [0.9.8i]</span>
</td>
<td>
	Adjusts the length of the holding tabs by scaling the length by these amounts.  <property>Outer Tab Scale</property> is the length
	toward the toolpath and <property>Inner Tab Scale</property> is the length away from the toolpath.
</td>
</tr>
<!-- $include file="../en/_prop/MachiningOrigin.html" -->
<tr>
<td>
	<property>Machining Origin</property>
</td>
<td>
<p>A drawing point that will be used as the machining origin (X=0,Y=0) point when gcode is created.</p>
<p markdown="1">The ellipsis button ![](../images/CONTENT.jpg) to the right of this property can be used to select a point in the drawing.</p>
<p>
An 'X' icon will be displayed on the drawing at the machining origin point.  This cross can be dragged to
a new location using the mouse.
</p>
<p><b>NOTE:</b> MachiningOrigin replaces the GCodeOrigin and GCodeOriginOffset properties of earlier releases.</p>
</td>
</tr>
<!-- $include.end -->
<tr>
<td>
	<property>Number Format</property>
</td>
<td>
	Controls how decimal numbers are output to the gcode file.  This property is overridden by the <property>Number Format</property> specified in the selected post processor.  
	See the Post Processor section for more information.
</td>
</tr>
<!-- $include file="../en/_prop/OutFile.html" -->
<tr>
<td>
	<property>Out File</property>
</td>
<td>
<p markdown="1">
This is the location of the destination gcode file.  Clicking the ![](../images/CONTENT.jpg) button to the right of
this property will open a file browser.
</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/PostProcessor.html" -->
<tr>
<td>
	<property>Post Processor</property>
</td>
<td>
    <p>
        A selection from a drop down list which contains a list of all the post processors available.  The post processor controls how the gcode files are formatted and are user configurable using XML based post processor files.
    </p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/PostProcessorMacros.html" -->
<tr>
<td>
	<property>Post Processor Macros</property>
</td>
<td>
    <p>
        This is a text field containing multiple macro definitions (one per line), of the format $macro=value.
        These macros can be used by the selected post processor and are a handy way of passing parameters from the drawing to the post processor.
    </p>
</td>
</tr>
<!-- $include.end -->

<tr>
<td>
	<property>Rebuild Toolpath Before Post</property>
</td>
<td>
	<p>Controls whether to regenerate toolpaths before creating gcode post.</p>
	<ul>
	<li><value>Always</value> - Toolpaths will automatically be regenerated before posting the gcode.</li>
	<li><value>Prompt</value> - Prompts whether or not to regenerate toolpaths before posting.</li>
	<li><value>If Needed</value> - Toolpaths will be regenerated if machining properties or drawing objects change.</li>
	</ul>
	<p><value>Prompt</value> or <value>If Needed</value> are useful when the toolpaths take a long time to generate such as with some 3D operations.</p>
</td>
</tr>

<tr>
<td>
	<property>Show Cut Widths</property>
	<br /><span class="new">[0.9.8] moved from machining to first item in the drawing tree.</span>
</td>
<td>
	<p><value>True</value> | <value>False</value>.</p>
	<p>Show cut widths will shade the areas that will be cut. This feature currently only works when the drawing view has not been rotated. It should be easy to spot any areas that are not shaded and will therefore have stock remaining.</p>
</td></tr>

<tr>
<td>
	<property>Show Direction Vector</property>
	<br /><span class="new">[0.9.8] moved from machining to first item in the drawing tree.</span>
</td>
<td>
	<p><value>True</value> | <value>False</value>.</p>
	<p>Controls the visibility of a small arrow at the start point of each toolpath that indications the direction of machining.</p>
</td>
</tr>
<tr>
<td>
	<property>Show Rapids</property>
	<br /><span class="new">[0.9.8] moved from machining to first item in the drawing tree.</span>
</td>
<td>
	<p><value>True</value> | <value>False</value>.</p>
	<p>Controls the visibility of a dashed line that indicates rapid moves from one toolpath to the next.
	NOTE: Rapids are currently only displayed within each machining operation.  Rapids from one machining operation to the next are not yet shown but should be in the next release.</p>
</td>
</tr>

<tr>
<td>
	<property>Show Toolpaths</property>
	<br /><span class="new">[0.9.8] moved from machining to first item in the drawing tree.</span>
</td>
<td>
	<p><value>True</value> | <value>False</value>.</p>
	<p>Shows or hides the toolpaths.  This is the same as using the <span class="cmd">View - Show Toolpaths</span> menu option.</p>
</td>
</tr>
<!-- $include file="../en/_prop/Stock.html" -->
<tr>
<td>
	<a name="Stock"><property>Stock</property></a>
</td>
<td>
<p>
    The stock object is used to define the dimensions of a block of material from which the part will be cut.
</p>
<p>
    The properties of the stock object can be used to automatically determine some machining properties.
</p>
<ul>
<li>If a machining operation or style's <property>Stock Surface</property> property is set to <value>Auto</value>, the stock's stock surface value will be used.</li>
<li>If a machining operation or style's <property>Target Depth</property>  property is set to <value>Auto</value>, the stock's stock surface and Z size will be used
to determine the target depth, so a machining operation will by default machine all the way through the stock.</li>
</ul>
<p>Stock properties:</p>
<p>
    <property>Material</property>: Informational text that describes the stock material.<br>
    <property>Stock Offset</property>: X and Y offset of the lower left corner of the stock block. For example, a stock offset of -10,-20
    would position the stock 10 units to the left of the Y axis (X=0) and 20 units below the X axis (Y=0).<br>
    <property>Stock Surface</property>: The Z location of the top of the stock block.<br>
    <property>Stock Size</property>: The X, Y and Z dimensions of the stock block.<br>
    <property>Color</property>: Color to use when displaying this stock object.
</p>
<p>
    Stock is undefined if the X,Y and Z sizes are all zero. Stock can be defined at the part or machining level.
    Stock defined at the part level will override and machining level stock definitions and will be used for all operations within the part.
</p>
<p>
    The stock object dimensions can also be passed to simulators such as CutViewer when post processors with appropriate stock macros are included, such as the
    <i>Mach3-CutViewer</i> post processor.
</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/Style.html" -->
<tr>
<td>
	<property>Style</property>
</td>
<td>
<p markdown="1">Select a default [CAM Style](cam-style.md){:.name} for this part.</p>
<p>All machining operations in the part will use this style unless set otherwise in the machining operation's Style property.</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/StyleLibrary.html" -->
<tr>
<td>
	<property>Style Library</property>
</td>
<td>
    <p>This property is used to locate the style definitions used in the Part or machining operations.</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.ToolDiameter.html" -->
<tr>
<td>
	<property>Tool Diameter</property>
</td>
<td>
    <p>This is the diameter of the current tool in drawing units.</p>
    <p>
        If the tool diameter is 0, the diameter from the tool information stored in the tool library
        for the given tool number will be used.
    </p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/ToolLibrary.html" -->
<tr>
<td>
	<property>Tool Library</property>
</td>
<td>
    <p>
        If left blank, the default tool library will be used (Default-{$Units}), otherwise the specified library will be used when
        looking up tool numbers.
    </p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.ToolNumber.html" -->
<tr>
<td>
	<property>Tool Number</property>
</td>
<td class="desc">
    <p>The ToolNumber is used to identify the current tool.</p>
    <p>
        If ToolNumber changes between successive machine ops a toolchange instruction is created in gcode.
        ToolNumber=0 is a special case which will not issue a toolchange.
    </p>
    <p>The tool number is also used to look up tool information in the current tool library.  The tool library is specified
	in the containing Part, or if this is not present in the Machining folder level.  If no tool library is defined the
	Default-(units) tool library is assumed.</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.ToolProfile.html" -->
<tr>
<td>
	<property>Tool Profile</property>
</td>
<td>
    <p>The shape of the cutter</p>
    <p>
        If the tool profile is Unspecified, the profile from the tool information stored in the tool library
        for the given tool number will be used.
    </p>
    <p>
        <value>EndMill</value> | <value>BullNose</value> | <value>BallNose</value> | <value>Vcutter</value> | <value>Drill</value> | <value>Lathe</value>
    </p>
</td>
</tr>
<!-- $include.end -->

<tr>
<td>
	<property>Toolpath Visibility</property>
	<br /><span class="new">[0.9.8] moved from machining to first item in the drawing tree.</span>
</td>
<td>
	<p><value>All</value> | <value>Selected Only</value></p>
    <p>
	When there are a lot of machining operations, it can get visually confusing as to which toolpath belongs to which machining operation.  
	By setting <property>Toolpath Visibility</property> to <value>Selected Only</value>, only the toolpaths for the machining operation selected in the drawing tree are visible.</p>
</td>
</tr>
<tr>
<td>
	<property>Velocity Mode</property>
</td>
<td>
	<p><value>Constant Velocity</value> | <value>Default</value> | <value>Exact Stop</value></p>
	<p>Controls the use of G61 and G64 commands in gcode output.</p>
	<p>This global velocity mode setting can be overridden by individual machine operations.  For example it may be useful to have a global value of <value>Constant Velocity</value> set for the drawing 
	and use <value>Exact Stop</value> for finishing machine operations.</p>
	<p>If <value>Default</value> is used, no velocity mode gcode is written (or the global velocity mode is used for machining operations).</p>
	<p><value>Constant Velocity</value>, sometimes referred to as 'Look Ahead', is a useful feature implemented in some CNC controllers so that motion is 
	smoothed between control points.  This is particularly useful with geometry that involves a sequence of many small movements, often trying to 
	approximate a natural shape.  The downside is a potential loss of accuracy.</p>
</td>
</tr>
</table>
