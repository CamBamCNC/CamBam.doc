---
title: Introduction
---

# Welcome to CamBam

*CamBam*{:.name} is an application to create CAM files (gcode) from CAD source files or its own internal geometry editor.  
CamBam has many users worldwide, from CNC hobbyists to professional machinists and engineers. 

![](images/cambam-plus-0.9.1_s.png){:align="right" .iright}

CamBam supports the following: 

- Reading from and writing to 2D DXF files.
- 2.5D profiling machine operations with auto-tab support
- 2.5D pocketing operations with auto island detection
- Drilling (Normal,Peck,Spiral Milling and Custom Scripts)
- Engraving
- True Type Font (TTF) text manipulation and outline (glyph) extraction.
- Conversion of bitmaps to heightmaps
- 3D geometry import from STL, 3DS and RAW files
- 3D surfacing operations
- Extensible through user written plugins and scripts

## [CamBam V1.0 documentation](contents.md)