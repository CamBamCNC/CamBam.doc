---
title: CamBam V1.0 documentation
---

# [CamBam V1.0 documentation](index.md)
<span class="refinfo" >Rev 1.23, {{ site.time | date: "%Y-%m-%d" }}</span>


## [Basics](basics.md)

{:.subcontents}
- **[User Interface](user-interface.md)**
- **[Drawing and System tabs](basics-drawing-and-system.md)**
- **[Rotating and panning](basics-rotating-and-panning.md)**
- **[Selecting objects](basics-selecting-objects.md)**
- **[Toolpaths and gcode](toolpaths-and-gcode.md)**
- **[Drawing Units](drawing-units.md)**
- **[File Menu](file-menu.md)**
- **[View Menu](view-menu.md)**
- **[Tools Menu](tools-menu.md)**
- **[Simple Example](simple-example.md)**
- **[Keyboard Shortcuts](keyboard-shortcuts.md)**

## [Machining (CAM)](cam/index.md)

{:.subcontents}
- **[Machining Basics](cam/basics.md)**
- **[Profile](cam/profile.md)**
- **[Pocket](cam/pocket.md)**
- **[Drill](cam/drill.md)**
- **[Engrave](cam/engrave.md)**
- **[3D Profile](cam/3d.md)**
- **[Lathe](cam/lathe.md)**
- **[Creating GCode](cam/creating-gcode.md)**
- **[Machining Options](cam/machining-options.md)**
- **[Edit Gcode](cam/gcode-editor.md)**
- **[CAM Part](cam/part.md)**
- **[CAM Styles](cam/cam-style.md)**
- **[Lead Moves](cam/lead-moves.md)**
- **[Holding Tabs](cam/holding-tabs.md)**
- **[Side Profiles](cam/side-profile.md)**
- **[Post Processor](cam/post-processor.md)**
- **[Nesting](cam/nesting.md)**
- **[Back Plotting](cam/back-plotting.md)**
- **[Tool Libraries](cam/tool-libraries.md)**
- **[Speeds and Feeds Calculator](cam/speeds-and-feeds.md)**

## [Drawing (CAD)](cad/index.md)

{:.subcontents}
- **[Entities](cad/entities.md)**
- **[Script Entity](cad/script-object.md)**
- **[Bitmaps](cad/bitmap.md)**
- **[Layers](cad/layers.md)**
- **[Transformations](cad/transformations.md)**
- **[Operations](cad/operations.md)**
- **[Edit Polyline](cad/edit-polyline.md)**
- **[Edit Surface](cad/edit-surface.md)**
- **[Edit Points](cad/edit-point-lists.md)**
- **[Creating Surfaces](cad/draw-surface.md)**
- **[Region Fill](cad/region-fill.md)**

## [Tutorials](tutorials/index.md)

{:.subcontents}
- **[Profile](tutorials/profile.md)**
- **[Pocketing](tutorials/pocket.md)**
- **[Drilling](tutorials/drill.md)**
- **[Bitmap Heightmaps](tutorials/heightmap.md)**
- **[Text Engraving](tutorials/text-engraving.md)**
- **[3D Profile](tutorials/3d-profile.md)**
- **[3D Profile - Back face](tutorials/3d-profile-back-face.md)**

## [Automation](automation.md)

## [Configuration](configuration.md)

## [Appendix](appendix.md)

{:.subcontents}
- **[What's New?"](whats-new.md)**
