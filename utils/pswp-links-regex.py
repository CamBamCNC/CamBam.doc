﻿"""
Convert image links of form...

<p><a href="../images/tutorial.profile-1.png"><img src="../images/tutorial.profile-1-t.png" alt="Step 1 - Insert pulley profile" class="icenter" /></a></p>

to

<div style="text-align:center;">
<figure><a href="../images/tutorial.profile-1.png" title="Step 1 - Insert pulley profile" data-size="1024x768" data-index="0">
<img src="../images/tutorial.profile-1-t.png"/>
</a></figure></div>


"""
import fnmatch
import os
import io
import re
import getimageinfo

def GetImageSize(img_file):
    # image_url = url
    # if not image_url.endswith("/"): image_url += "/"
    # image_url += href
    # response2 = urllib.request.urlopen(image_url)
    with open(img_file, 'rb') as fimg:
        image_type,width,height = getimageinfo.getImageInfo(fimg)
    return str(width) + "x" + str(height);



def process_file( srcfile, destfile ):
    print( "processing " + srcfile )

    # {$include file="prop/3d.Additive.txt"}        
    # pattern = re.compile(r"{\$include file=\"([^\"]*)\"}", re.IGNORECASE)
    # <!-- $include file="prop/mop.FinalDepthIncrement.html" -->
    # <!-- $include.end -->

    # \<p\>\<a(.*)href=(\"[^\"]*\")\>\<img src=(\"[^\"]*\")

    # link in paragraph
    # \<p\>(\<a(.*)\<\/a\>)\<\/p>
    # image link in paragraph
    # \<p\>(\<a(.*)(\<img([^\>]*)\>)\<\/a\>)\<\/p>

    img_index = 0

    re_imglink = re.compile(r"\<p\s*(?:[^\>]*?)\>(\<a(.*?)(\<img([^\>]*?)\>)\<\/a\>)(.*)\<\/p>", re.IGNORECASE)
    re_href = re.compile(r"href=\"([^\"]*)\"", re.IGNORECASE)
    re_src = re.compile(r"src=(\"[^\"]*\")", re.IGNORECASE)
    re_alt = re.compile(r"alt=(\"[^\"]*\")", re.IGNORECASE)
    re_imgclass = re.compile(r"class=(\"[^\"]*\")", re.IGNORECASE)
    
    ftext = ""
    
    with io.open(srcfile, 'r', encoding="utf-8") as f:
        for line in f:
            match = re.search(re_imglink,line)
            if match:
                ftext += "<!-- Change this.... -->\n"
                
                href = ""
                src = ""
                title = ""
                img_class = ""
                img_size = ""

                match_href = re.search(re_href,match.group(1))
                if match_href:
                    href = match_href.group(1)
                match_src = re.search(re_src,match.group(3))
                if match_src:
                    src = match_src.group(1)
                match_alt = re.search(re_alt,match.group(3))
                if match_alt:
                    title = match_alt.group(1)
                match_imgclass = re.search(re_imgclass,match.group(3))
                if match_imgclass:
                    img_class = match_imgclass.group(1)

                # TODO: get image size
                img_size = GetImageSize(os.path.join(os.path.dirname(srcfile),href))

                if "icenter" in img_class:
                    ftext += "<div style=\"text-align:center;\">"
                else:
                    ftext += "<div>"
                ftext += "<figure>"
                ftext += "<a href=\"" + href + "\" title=" + title + " data-size=\"" + img_size + "\" data-index=\"" + str(img_index) + "\">"
                ftext += "<img src=" + src + ">"
                ftext += "</a></figure>"
                match.group(1)
                ftext += "</div>"

                img_index += 1

            else:
                ftext += line
 
    with io.open(destfile, "w", encoding="utf-8") as f:
        f.write(ftext)

#  filepattern = "profile.html"
filepattern = "*.html"

for root, dirnames, filenames in os.walk('..\\_site\\tutorials\\'):
    for filename in fnmatch.filter(filenames, filepattern):
        # matches.append(os.path.join(root, filename))
        srcfile = os.path.join(root, filename)
        # destfile = os.path.join("C:\\dump\\pytest\\", filename)
        destfile = srcfile.replace(".html","-pswp.html")
        process_file( srcfile, destfile )
        