---
title: CAM Styles
---
# CAM Styles

CAM Styles are a way of grouping machining operation parameters into reusable objects to help simplify common machining tasks.
	    
![](../images/style1.png){:align="right" .iright}

Each machining operation has a <property>Style</property> property.  This refers to a style definition, stored in a system library, which is available to all drawings.
The value of machining operation properties that are marked as <value>Default</value>, will be taken from the style associated with the operation.
In this way, any changes to a CAM style will immediately affect all operations that refer to it.

If no style is selected, a default style will be selected automatically.

> **Note:** Styles replace a system of <span class="name">Templates</span> that were used in previous versions of CamBam 
and provided a similar purpose.
{:.note style="width:60%;"}

## Default, Value and Auto properties

Machining operation and Style properties can have multiple states, indicated by the icon to the right of the property name.

![](../images/basics/cbv_default.png) <span class="name">Default</span> - The value of the property will be taken from the CAM style associated with the machining operation.
Default property values will be displayed in grey italics and will show the default value that will be used.

![](../images/basics/cbv_auto.png) <span class="name">Auto</span> - Indicates the property value is to be calculated internally by CamBam, often based on other settings.<br/>
For example if the <property>Target Depth</property> property is set to <value>Auto</value>, the depth will be calculated to cut the full thickness of the stock object.

![](../images/basics/cbv_value.png) <span class="name">Value</span> - The property value has been entered explicitly.  In this way, the property is overridden from the value
stored in its parent style.

![](../images/value-context-menu.png){:align="right" .iright}

Clicking the value icon to the right of the property name, or right clicking the property, will display a menu where the property value state
can be selected.  This context menu also contains an <span class="cmd">Inherited style</span> command. <span class="cmd">Inherited style</span> invokes a message box showing
where the value of the selected property will be taken from.

## Property cache conflict alert

![](../images/property_cache.png){:align="right" .iright}

If the result of a <value>Default</value> property has changed from the previous value used, a <span class="name">Property Cache Conflict</span> message may be shown.
This may occur if the value stored in the parent style has changed, or if the style uses an <value>Auto</value> value and parameters
that affect the result of the automatic calculation have changed.

The warning message provides the following options:

- <span class="cmd">Use new value</span> - the newly calculated default value will be used.
- <span class="cmd">Use existing value</span> - the old value will continue to be used.  This will change the property from a <value>Default</value> to an explicit <value>Value</value>.
- <span class="cmd">Cancel current action</span> - the old value will continue to be used and left as a <value>Default</value>, but the current action will be canceled.

If <b>Use same action for all conflicts</b> is checked, the same response will be used whenever a new conflict is detected.
This remains in effect until the file is closed.  The next time the file is opened, changed default properties will once
again be reported.

The cache conflict alert was added to prevent inadvertent changes to a drawing resulting from changing a style or
other dependent system library.  In this way, if a drawing is transferred to another computer or sent to another person, 
it is not necessary to also provide the dependent style definitions, as all the required information will be preserved within the file.

## Machine operation, Part and Machining CAM styles

CAM Styles can be specified at the <span class="name">Part</span> level and in the top level <span class="name">Machining</span> folder.

If the <property>Style</property> property is left blank for a machining operation, the Style specified in the containing <span class="name">Part</span> object will be used. 
If the <span class="name">Part</span> does not have a defined Style, the Style set against the <span class="name">Machining</span> object will be used. 
In the event that no style is defined at any of these levels, the default style will be used for the source of the <i>Default</i> values.

![](../images/default_style(en).png){:align="right" .iright}

The default style is the style with an empty name in the style library.

> **Warning:** The default styles are very important for CamBam to function correctly and should not
be renamed or removed.
{:.warning style="width:45%"}

## Style definitions and style libraries

Style definitions can be maintained from the <span class="name">CAM Styles</span> section of the [System](../basics-drawing-and-system.md#System){:.name} tab.

CAM Styles contain a <property>Parent Style</property> property, so that styles can be based upon other styles.  
If the parent style parameter is not set, the default (blank name) top level style will be used to resolve default properties.

> **Hint:** If the properties in the default CAM style are set as close as possible to the values used by 
the majority of machining operations encountered, then in many cases, extra CAM styles may not need to be defined at all.
{:.note }

Styles are grouped into style libraries.  A style library may be used to group parameters for machining particular materials or different drawing units.

The <span class="name">Machining</span> and <span class="name">Part</span> objects contain a <property>Style Library</property> property.  This can be used to determine the correct style to use
when the same Style name is present in multiple libraries.

The style library property can contain the following macros:

**{$Material}** This will be expanded to the name of the material used in the stock object.

**{$Units}** This will be expanded to the drawing units abbreviation (e.g. `'mm'` for Millimeters and `'in'` for Inches).

If no style library is specified, libraries will be searched in the following order:

1. {$Material}-{$Units} (if a stock material is defined)
1. Standard-{$Units}

![](../images/style3.png){:align="right" .iright}

Styles and style libraries can be cut, copied, pasted, deleted and renamed within the System tree.  A right click context menu gives access to many of these commands.

Styles can also be moved from one style library to another by clicking and dragging them within the system tree.

If a style library has been modified externally or by another instance of CamBam, the <span class="cmd">Reload</span> operation will load the latest changes into the current program instance.

{:style="clear:both;"}
![](../images/style4.png){:align="right" .iright}

The context menu shown when right clicking a style also contains a <span class="cmd">New CAM style variant</span> option.  This will create a new style that inherits its default
parameters from the selected 'parent' style.

It is also possible to copy settings from machining operations into a style by copying the machining operation to the clipboard, selecting the
system tab, right clicking a destination CAM style then selecting <span class="cmd">Paste format</span> from the context menu.
This provides a similar functionality to the <span class="cmd">Copy MOP to Template</span> operation of previous CamBam versions.

