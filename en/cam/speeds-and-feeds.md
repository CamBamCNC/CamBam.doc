---
title: Speeds and Feeds Calculator
---
# Speeds and Feeds Calculator

The speeds and feeds calculator is accessed from the context menu shown when right clicking a machining operation.
This can be used to calculate feed rates, spindle rpms and other machining parameters.

> The speeds and feeds calculator is rather basic at the moment.  It requires an understanding of the theory of 
the calculations involved.  It will also require information from external references, such as tooling data sheets from cutter manufacturers,
and machinist's lookup tables.  
<br>
Speed and feed formulas should also be considered as a rough guideline and are no substitute for practical experience gained, working with specific machines, cutters and materials.
The formulas are often based on reference data which assumes optimal cutting conditions, coolant, rigid machines, and are often targeted towards industrial applications 
to optimise productivity and not necessarily tool life.  
<br>
Many other factors will also need to be taken into account when judging appropriate speeds and feeds, such as: 
Machine rigidity and backlash, Spindle power, Sharpness of tooling, Depth of cut, Finishing or Roughing operations etc.
{:.warning}

Some information (such as <property>Tool Diameter</property> and <property>Cut Feedrate</property>) may be taken from the machining operation selected, or from the tool libraries
(<property>Num Flutes</property>).
No information is currently fed back into the machining operation, so the results of any calculations will need to be manually copy and pasted
into the appropriate parameters.

![](../images/speed_calc_1.png){:.icenter}

<table> 
<tr>
	<td><property>Num flutes</property></td>
	<td>Number of teeth</td>
</tr><tr>
	<td><property>Diameter</property></td>
	<td>The diameter of the tool.</td>
</tr><tr>
	<td><property>Tooth Loading</property></td>
	<td>Feed per tooth in (inches or mm).  This information will need to be looked up from cutter manufacturer data or machinist
	reference tables.</td>
</tr><tr>
	<td><property>Surface Speed</property></td>
	<td>Cutting speed in m/min or inch/min.  Also will need to be looked up from cutter manufacturer data or machinist
	reference tables.</td>
</tr><tr>
	<td><property>Feedrate</property></td>
	<td>Feed rate in  mm/min or inch/min.</td>
</tr><tr>
	<td><property>RPM</property></td>
	<td>Rotation speed of the spindle in revs per min.</td>
</tr>
</table> 

### Usage

The general working method is to start with the <property>Number of flutes</property> and <property>Diameter</property> properties, which should remain fixed.
Then enter the recommended <property>Tooth Loading</property> and <property>Surface Speed</property> values suggested for the cutter / stock material combination,
taken from reference or manufacturer data.

The aim is to find suitable <property>Feedrate</property> and <property>RPM</property> values, which can then be fed back into the machining operation.

The values of <property>Feedrate</property> and <property>RPM</property>, suggested by the formulas, may not be possible given the limitations of the CNC machine.
In these cases, the machine's limits will be fed back into the calculation to determine the effect this will have
on the tooth loads and surface speeds.

In the example image, the values ​​of <property>RPM</property> and <property>Feedrate</property> have been set to 0 to highlight that these values ​will be calculated
from the other parameters.  In this case, a 6mm diameter cutter with 3 teeth, a tooth load of 0.01mm per tooth and a surface speed of 150m/min.

The buttons numbered 1 through 5 on the image are used to calculate a parameter based on other variables.
The formula, and dependent variables used, are shown to the right of the calculate buttons.

1) Clicking button 5, will calculate the spindle speed (RPM) from the surface speed and tool diameter.  
In this example, we get 7958 rev/min.

2) Clicking button 3 will then calculate the feed rate from the spindle rpms calculated in step 1, the tooth load and number of flutes.
In this example the result is 238.74 (m / min).

> **Warning:** When the speed and feeds calculator is opened, it will contain feedrate and
spindle speed information from the selected machining operation.  These values may need to be recalculated to attain accurate values
given the current tooth load and surface speeds.
{:.warning }

### Adjust the calculations based on the limitations of hardware

It is not always possible to use the ideal values calculated.
The spindle may not turn fast enough, or, conversely slow enough.  The machine may also not be able to achieve the required feed rate.
In these situations, it will be necessary to compromise and change the values to suitable limiting values.

Buttons 1, 2 and 4 are used to calculate the value of their associated parameters if the feedrate or RPMs need to be
manually modified to limiting values. 
The rotational speed (RPM) has 2 buttons because it can be calculated either taking into account the <property>Feedrate</property> and <property>Tooth Load</property> 
![](../images/pt_violet.png), or the <property>Surface Speed</property> ![](../images/pt_bleu.png).

These adjustments should be made only after completing steps 1) and 2) above.

![](../images/speed_calc_2.png){:.icenter} 

**Example 1**

Suppose our spindle does not drop below 10,000 rev / min, we can calculate the other parameters according to this speed. Enter 10000 for <property>RPM</property>.

Next, calculate the other values ​to reflect the new spindle speed. In this case, the <property>Feedrate</property> and <property>Surface Speed</property> values.
Click button 3 to calculate the new <property>Feedrate</property> based on the revised <property>RPM</property> value.
The result in this example is 300 mm / min. Clicking button 2 will recalculate the <property>Surface Speed</property>, also based on the revised <property>RPM</property> value.
In this example the revised <property>Surface Speed</property> is 188.5 m / min.
If this is outside the range of recommended cutting speeds, extra care should be taken and the machining strategy may need to be revised.
 
**Example 2**

The cutting parameters selected for this second example are: <property>Tool Diameter</property> 6mm, 4 teeth, feed 0.1 mm / tooth, cutting speed 150 m / min

![](../images/speed_calc_3.png){:.icenter}  

The button 5) calculation provides a spindle speed of <b>7958 rev/min</b> and button 3), a feedrate of <b>3183.2 mm/min</b>.

Suppose our machine is limited to a maximum speed of 2000 mm / min, we will enter that value as the feed rate (instead of 3183.2).
We can then try different possibilities for other suitable values. 
In this case, we can recalculate the spindle speed, for example (depending on the feed rate) by clicking on button 4.
This will give us a spindle speed of 5000 rev / min.

As with the previous example, we recalculate the <property>Surface Speed</property> (button 2) to verify that we are still within an acceptable range. 
In this case we get 94.25 m / min.

If we had wanted to keep the same RPM speed (7958) for this feed rate of 2000 mm / min (and thus keep the recommended surface speed), 
we could use button 1. to calculate a new <property>Tooth Loading</property>.  This would give a value of 0.0628 mm/tooth.

![](../images/speed_calc_4.png){:.icenter}  
 
