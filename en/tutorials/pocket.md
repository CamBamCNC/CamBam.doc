---
title: Tutorial - Pocketing and Island Pocketing
---
# Tutorial: Pocketing and Island Pocketing

This tutorial will describe using the [Pocket](../cam/pocket.md){:.name} machining operation and 
will also cover - Loading DXF files, CAD drawing, object transformations and Automatic Island Pockets.

[Download  the files used in this tutorial](../images/tutorials/pocket/heart-pocket.zip)

## Step 1 - Load a DXF file

The sample file above, includes a heart shape DXF file.  If you are married and fanatical about CNC, this shape can come in very handy indeed!

This shape is a nice and clean, closed polyline.  If your DXF files contain many small segments or uses non polyline objects 
you should tidy the drawing before creating any machining operations.

To convert objects to polylines, select them, then select <span class="cmd">Convert To - Polyline</span> from the drawing's context menu, 
or when the drawing window has focus, use the <span class="key">CTRL+P</span> shortcut key.

[![Step 1 - Load a DXF file](../images/tutorial.pocket-1-t.png){:.icenter}](../images/tutorial.pocket-1.png)

## Step 2- Free hand CAD drawing

Use the polyline drawing tool ![Draw Polyline](../images/basics/poly.png) to draw a random shape around the heart.
This will form the outer boundaries of an island pocket.
For the last point, press the <span class="key">C</span> key to close the shape, or click on the first polyline point
(the cursor should snap to it), then press <span class="key">ENTER</span> or click the middle mouse button.

[![Step 2 - Free hand CAD drawing](../images/tutorial.pocket-2-t.png){:.icenter}](../images/tutorial.pocket-2.png)

If the polyline does not sit evenly around the heart, you can free hand move objects by selecting them, then hold the <span class="key">SHIFT</span> key
and drag objects with the left mouse button.  To position objects more accurately, use the <span class="cmd">Transform - Translate</span> drawing context menu.
This will translate an object given an origin and destination point.

To make the shape a bit rounded create an offset shape.
Select the polyline, then click <span class="cmd">Edit - Offset</span> from the drawing context menu.
This will prompt for a distance to draw an offset polyline.
Positive offsets will be outside the polyline shape and negative offsets will be inside.

To rotate a shape, select it then use the <span class="cmd">Edit - Transform - Rotate</span> menu option or <span class="key">CTRL+R</span> keyboard short cut
to enter rotation mode.  Select a center of rotation point, a starting angle then move the mouse around the start point
to the desired position.

[![Step 2 - Offsets, translations and rotations](../images/tutorial.pocket-3-t.png){:.icenter}](../images/tutorial.pocket-3.png)

## Step 3 - Pocket the heart

Select the heart shape then insert a pocket machine operation using the pocket tool ![CAM Pocket MOP](../images/basics/cam_pocket.png).
For pocketing basics, please see the [Stepper Mount tutorial](../simple-example.md){:.name} in Getting Started.

The important thing to remember is that the <property>Target Depth</property> should be lower than <property>Stock Surface</property>.
If stock surface is at zero then the <strong>target depth should be negative</strong>.

CamBam can cut deep pockets by generating toolpaths at progressively deeper cutting levels.
The distance between each cut level is specified in the <property>Depth Increment</property> property.

To ensure a final light finishing pass at the lowest level cut, enter a small value in the <property>Final Depth Increment</property> property (0.1mm , 0.004").
This will be the depth of stock removed at the bottom pass of the pocket.

Another useful parameter is <property>Roughing Clearance</property>.  Enter a small value here to specify how much stock to leave remaining between the walls of 
the pocket and the target geometry.  This stock can then be removed using a later finishing profile.

If a <b>negative</b> <property>Roughing Clearance</property> value is specified, the geometry will be over cut by this amount.
This is very useful when making inlays or die cutting.
The <property>Roughing Clearance</property> can be adjusted so the positive and negative shapes fit very closely.
Roughing clearance can be adjusted while the stock is still mounted in the CNC machine. The pockets can then be tested with a previously cut inlay for a good fit.

[![Step 3 - Inner pocket](../images/tutorial.pocket-4-t.png){:.icenter}](../images/tutorial.pocket-4.png)

## Step 4 - Creating an Island Pocket

Island pockets can be created automatically by selecting inner and outer polylines then inserting a pocket as usual.
Shapes within other shapes will be detected and toolpaths will be excluded from these inner 'hole' shapes. 

If there are 3 concentric shapes selected for a pocket, the pocketing routines will interpret this as a pocket within an island within a pocket.
In this tutorial we could have used just 1 pocket from all 3 polylines, but for clarity 2 separate pockets were used.

With the 2 outer polylines selected, insert another pocket ![CAM Pocket MOP](../images/basics/cam_pocket.png).

To save entering in all the pocketing parameters for the second pocket, right click the first pocket operation in the drawing tree
then select <span class="cmd">Copy</span> from the context menu, then right click the second pocket operation and select
<span class="cmd">Paste Format</span> from the context menu.  This will copy all the properties from the source operation into the destination,
apart from information such as the operation's name and lists of source objects.

Generate the toolpaths again.  If all is well, the routines should detect that you intend to do a island pocket and will generate toolpaths in between the 2 curves.

[![Step 4 - Island Pocket](../images/tutorial.pocket-5-t.png){:.icenter}](../images/tutorial.pocket-5.png)

## Step 5 - Show Cut Widths

Before we continue, we will turn on the <span class="cmd">Show Cut Widths</span> option to indicate the areas that will be machined.
From the main <span class="cmd">View</span> menu, or the <span class="cmd">View</span> sub menu of the context menu shown when right clicking the drawing,
tick the <span class="cmd">Show cut widths</span> option, if it is not already ticked.

Show cut widths will shade the areas that will be cut.  It should be easy to spot any areas that are not shaded and will therefore have stock remaining.

In the image below there will be uncut stock in the inside corner shapes where the tool radius cannot reach without overcutting.

![Show cut widths](../images/tutorial.pocket-6.png){:.icenter}

## Step 6 - Machine operation renaming.

The drawing is basically complete and ready to save and generate gcode, but first we will do some cosmetic changes to help manage the drawing.

Machine operations can be given a more meaningful name, to help with readability and debugging.
To rename a machine operation, select it in the drawing tree and press <span class="key">F2</span>, or click the name a second time.
Avoid using special characters in the name such as parenthesis as could cause problems due to nested comments.

To change the order of machine operations, click and drag them to the desired position within the drawing tree.

Create the gcode as normal.  The new machine operation names will be present in comments within the gcode file.
This is very useful for diagnostic purposes.

[![Step 6 - Renaming MOPs](../images/tutorial.pocket-7-t.png){:.icenter}](../images/tutorial.pocket-7.png)

The following image shows the pocket simulated using CutViewer Mill

[![Simulating with CutViewer Mill](../images/tutorial.pocket-8-t.png){:.icenter}](../images/tutorial.pocket-8.png)
