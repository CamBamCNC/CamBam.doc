---
title: Tutorial - Drilling
---
# Tutorial: Drilling

Creating drilling patterns is very easy. Here a Wingding font character 'N' is used to create a drilling pattern for an external hard disk enclosure.

[Download the files used in this tutorial](../images/tutorials/drilling/jolly-roger.zip)

## Step 1 - Insert text 

Drilling machine operations are based on point lists or circle centers.
There are a number of routines in CamBam to generate point lists that can give interesting effects.

In a new CamBam drawing, insert a text object ![Insert Text Tool](../images/basics/text.png).
The Wingdings capital 'N' character happens to be a natty jolly roger.

Set the text <property>Height</property> to something large like 200 (I am working in mm here), and change the <property>Font</property> property to <value>Wingdings</value>.

[![Step 1 - Inserting Wingdings N character](../images/tutorial.drill-1-t.png){:.icenter}](../images/tutorial.drill-1.png)

## Step 2 - Fill text object with points

Select the text object then chose <span class="cmd">Draw - Points List - Offset Fill Geometry</span> from the drawing context menu.
This will prompt you for a step distance. Enter 2 here and press enter.

You should now have created a set of points which fill the selected geometry (excluding any holes in regions and text).

[![Step 2 - Filling text object with points](../images/tutorial.drill-2-t.png){:.icenter}](../images/tutorial.drill-2.png)

## Step 3 - Insert a drilling machine operation 

With the point lists selected, insert a drilling machine operation ![Drilling machine operation](../images/basics/cam_drill.png).

Under the drill mop properties, set <property>Tool Diameter</property> to 1.5 and the <property>Target Depth</property> to -3.

That's pretty much it! To make things clearer, you can right click on the Default layer in the file tree and select 'hide'.

You should now just see a bunch of circles indicating the drilling hole sizes.

[![Step 3 - Viewing drill holes](../images/tutorial.drill-3-t.png){:.icenter}](../images/tutorial.drill-3.png)

Right click on machining in the file tree to generate the gcode.

Here's one I prepared earlier.

This is the aluminium cover off an ICY BOX external USB hdd enclosure. Should look neat with some LEDS behind it.

[![CNC Drilled Hard Disk Enclosure](../images/tutorials/drilling/jolly-roger-s.jpg){:.icenter}](../images/tutorials/drilling/jolly-roger.jpg)  

Most geometry can be used to generate point lists. Try experimenting with the other <span class="cmd">Draw - Point List</span> options.
