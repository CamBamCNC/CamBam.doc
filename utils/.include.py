﻿"""Includes files into other files...
"""
import fnmatch
import os
import re

def process_file( srcfile, destfile ):
    print "processing " + srcfile    

    # {$include file="prop/3d.Additive.txt"}        
    # pattern = re.compile(r"{\$include file=\"([^\"]*)\"}", re.IGNORECASE)
    # <!-- $include file="prop/mop.FinalDepthIncrement.html" -->
    # <!-- $include.end -->
    start_pattern = re.compile(r"\<!-- \$include file=\"([^\"]*)\" --\>", re.IGNORECASE)
    end_pattern = re.compile(r"\<!-- \$include.end --\>", re.IGNORECASE)
    in_include = False
    
    ftext = ""
    
    with open(srcfile, 'U') as f:
        for line in f:
            if in_include:
                match = re.search(end_pattern,line)
                if match:
                    in_include = False
            else:
                match = re.search(start_pattern,line)
                if match:
                    in_include = True
                    include_file = match.group(1)
                    if include_file.startswith('prop/'):
                        include_file = '../en/_' + include_file
                    print "include: " + include_file
                
                    ftext += "<!-- $include file=\"" + include_file + "\" -->\n"
                    with open(include_file, 'U') as f2:
                        insert = f2.read()
                        ftext += insert
                    ftext += "<!-- $include.end -->\n"
                else:
                    ftext += line
 
    with open(destfile, "w") as f:
        f.write(ftext)

filepattern = "*.md"
# filepattern = "profile.md"
# filepattern = "3d.md"

for root, dirnames, filenames in os.walk('..\\en\\cad\\'):
    for filename in fnmatch.filter(filenames, filepattern):
        # matches.append(os.path.join(root, filename))
        srcfile = os.path.join(root, filename)
        # destfile = os.path.join("C:\\dump\\pytest\\", filename)
        destfile = srcfile
        process_file( srcfile, destfile )
        