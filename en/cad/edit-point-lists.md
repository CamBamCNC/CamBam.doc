---
title: Editing Point Lists
---
# Editing Point Lists

## Move or add points

You can edit a point list object by double clicking any of its points in the drawing view to activate the edit mode.

![](../images/edit_ptlist-1.png){:.icenter}

![](../images/edit_ptlist-2.png){:.icenter}  

To move a point, click and drag the square point icons.

![](../images/edit_ptlist-3.png){:.icenter}

Click on empty areas to add new points to the point list.

![](../images/edit_ptlist-4.png){:.icenter}

Click the middle mouse button, or press <span class="key">Enter</span>, to accept the changes.  Press <span class="key">ESC</span> to cancel the point list edit.

## Deleting points or entering explicit coordinates.

The points in a point list can also be edited in a tabular format by clicking the [...] button to the right
of the <property>Points</property> collection property of the selected point list.

![](../images/edit_ptlist2.png){:.icenter width="750" height="441"}

Points can be deleted by highlighting a row and pressing the <span class="key">Delete</span> key.

Exact X, Y and Z coordinates can be entered directly into the table.

Entering coordinates into the bottom line marked with an '*' will insert a new point into the list.

It is also possible to cut, copy and paste the point list data from this table as tab delimited text.
This also allows cutting and pasting points to and from a spreadsheet such as Microsoft Excel.

## Explode Point Lists

The <span class="cmd">Edit - Explode</span> operation can be used to break a point list containing multiple points
into the individual points.

