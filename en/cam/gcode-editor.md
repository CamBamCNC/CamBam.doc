---
title: Viewing and editing gcode
---
# Viewing and editing gcode

CamBam can be used to view and edit the output gcode.  It is also possible to specify an external gcode editor for this purpose.

To invoke the gcode editor, use the <span class="cmd">Machining - Edit gcode</span> menu option, or from the context menu presented when right clicking the 
machining folder in the drawing tree.

<span class="cmd">Edit gcode</span> currently only edits the top level <span class="name">Machining</span> gcode file.  To edit gcode created from <span class="name">Parts</span> or individual machining operations
it is necessary to open these manually.

To find the location of the created gcode file, <span class="cmd">Browse gcode folder</span> from the Machining context menu can be used.  This will
open Windows Explorer to the gcode folder location.

The tool used to open gcode files can be set in the [GCode Editor](../configuration.md#GCodeEditor){:.name} property of the 
[system configuration settings](../configuration.md){:.name}.

In the following example, Windows notepad has been defined as the gcode editor.

![](../images/set_editor.png){:.icenter}

Gcode files can also be view and their toolpaths displayed (or back plotted) using the [NC File](back-plotting.md){:.name}.
Double clicking the <span class="name">NC File</span> object in the drawing tree will invoke the gcode editor on the source gcode of the <span class="name">NC File</span> operation.

See the [Back Plotting section](back-plotting.md){:.name} for more information.