---
title: ![icon](../img/icon/cam_ncfile32.png) Backplotting + NCFile object
---
# ![icon](../img/icon/cam_ncfile32.png) Backplotting + NCFile object

CamBam can be used to view toolpaths contained within many gcode files.

GCode files can be opened using <span class="cmd">File - Open</span>, or dragged onto the main drawing view from Windows Explorer.

The gcode file is associated with a special <span class="name">NCFile</span> machining operation that will appear in the machining tree view.
This operation contains properties that can change the way the gcode is interpreted and displayed.  If any options are changed,
the toolpaths should then be regenerated.

CamBam currently only supports basic gcode and does not recognise more complex gcode syntax such as subroutines.

<span class="new">New [0.9.8]</span><br />
As of version 0.9.8, the contents of the gcode file referenced in the NCFile object, will be written to
the gcode output of the parent drawing.  Also, by double clicking the NCFile machining operation in the drawing
tree, the gcode source file will be opened in the configured gcode editor.

Another useful feature of backplotting is the ability to convert the gcode toolpaths to drawing objects.  Right click the
NCFile object under the machining tree and select <span class="cmd">Toolpath To Geometry</span> from the context menu.
                                                                    
## Properties

<table class="prop" cellspacing="0" width="100%">
<tr>
<td>
	<property>Arc Center Mode</property>
</td>
<td>
	GCode distance mode (<value>Absolute</value> or <value>Relative</value>), used to determine I and J coordinates in G02 and G03 (arc) commands.
</td>
</tr>
<!-- $include file="../en/_prop/mop.CustomMOPFooter.html" -->
<tr>
<td>
	<property>Custom MOP Footer</property>
</td>
<td>
    <p>A multi-line gcode script that will be inserted into the gcode post after the current machining operation.</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.CustomMOPHeader.html" -->
<tr>
<td>
	<property>Custom MOP Header</property>
</td>
<td>
    <p>A multi-line gcode script that will be inserted into the gcode post before the current machining operation.</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.CutFeedrate.html" -->
<tr>
<td>
	<property>Cut Feedrate</property>
</td>
<td>
    <p>The feed rate to use when cutting.</p>
</td>
</tr>
<!-- $include.end -->

<tr>
<td>
	<property>Distance Mode</property>
</td>
<td>
	GCode distance mode (<value>Absolute</value> or <value>Relative</value>), used to determine X, Y and Z coordinates.
</td>
</tr>
<!-- $include file="../en/_prop/mop.Enabled.html" -->
<tr>
<td>
	<property>Enabled</property>
</td>
<td>
    <p>
        <value>True</value>: The toolpaths associated with this machining operation are displayed and included in the gcode output<br>
        <value>False</value>: The operation will be ignored and no gcode or tool paths will be produced for this operation.
    </p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.MaxCrossoverDistance.html" -->
<tr>
<td>
	<property>Max Crossover Distance</property>
</td>
<td>
<p>Maximum distance as a fraction (0-1) of the tool diameter to cut in horizontal transitions.</p>
<p>If the distance to the next toolpath exceeds MaxCrossoverDistance, a retract, rapid and plunge to the next position, via the clearance plane, is inserted.</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.Name.html" -->
<tr>
<td>
	<property>Name</property>
</td>
<td>
    <p>
        Each machine operation can be given a meaningful name or description.<br>
        This is output in the gcode as a comment and is useful for keeping track of the function of each machining operation.
    </p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.OptimisationMode.html" -->
<tr>
<td>
	<property>Optimisation Mode</property>
</td>
<td>
    <p>An option that controls how the toolpaths are ordered in gcode output.</p>
    <p>
        <value>New (0.9.8)</value> - A new, improved optimiser currently in testing.<br>
        <value>Legacy (0.9.7)</value> - Toolpaths are ordered using same logic as version 0.9.7.<br>
        <value>None</value> - Toolpaths are not optimised and are written in the order they were generated.
    </p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.PlungeFeedrate.html" -->
<tr>
<td>
	<property>Plunge Feedrate</property>
</td>
<td>
    <p>The feed rate to use when plunging.</p>
</td>
</tr>
<!-- $include.end -->
<tr>
<td>
	<property>Source File</property>
</td>
<td>
	The filename of the gcode file which will be read, back plotted and inserted into output gcode.
</td>
</tr>
<!-- $include file="../en/_prop/mop.StartPoint.html" -->
<tr>
<td>
	<property>Start Point</property>
</td>
<td>
    <p>
        Used to select a point, near to where the first toolpath should begin machining.<br />
        If a start point is defined, a small circle will be displayed at this point when the machining operation
        is selected.  The start point circle can be moved by clicking and dragging.
    </p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.Style.html" -->
<tr>
<td>
	<property>Style</property>
	<br /><span class="new">[New! 0.9.8]</span>
</td>
<td>
<p markdown="1">
Select a [CAM Style](cam-style.md){:.name} for this machining operation.
All default parameters will be inherited from this style.
</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.Tag.html" -->
<tr>
<td>
	<property>Tag</property>
</td>
<td class="desc">
    <p>A general purpose, multi-line text field that can be used to store notes or parameter data.</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.ToolDiameter.html" -->
<tr>
<td>
	<property>Tool Diameter</property>
</td>
<td>
    <p>This is the diameter of the current tool in drawing units.</p>
    <p>
        If the tool diameter is 0, the diameter from the tool information stored in the tool library
        for the given tool number will be used.
    </p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.ToolNumber.html" -->
<tr>
<td>
	<property>Tool Number</property>
</td>
<td class="desc">
    <p>The ToolNumber is used to identify the current tool.</p>
    <p>
        If ToolNumber changes between successive machine ops a toolchange instruction is created in gcode.
        ToolNumber=0 is a special case which will not issue a toolchange.
    </p>
    <p>The tool number is also used to look up tool information in the current tool library.  The tool library is specified
	in the containing Part, or if this is not present in the Machining folder level.  If no tool library is defined the
	Default-(units) tool library is assumed.</p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.ToolProfile.html" -->
<tr>
<td>
	<property>Tool Profile</property>
</td>
<td>
    <p>The shape of the cutter</p>
    <p>
        If the tool profile is Unspecified, the profile from the tool information stored in the tool library
        for the given tool number will be used.
    </p>
    <p>
        <value>EndMill</value> | <value>BullNose</value> | <value>BallNose</value> | <value>Vcutter</value> | <value>Drill</value> | <value>Lathe</value>
    </p>
</td>
</tr>
<!-- $include.end -->
<!-- $include file="../en/_prop/mop.WorkPlane.html" -->
<tr>
<td>
	<property>Work Plane</property>
</td>
<td>
    <p>
        Used to define the gcode workplane.  Arc moves are defined within this plane.<br>
        Options are <value>XY</value> | <value>XZ</value> | <value>YZ</value>
    </p>
</td>
</tr>
<!-- $include.end -->

</table>
