---
title: Machining Operations
---
# Machining Operations

A machining operation is an object that will generate toolpaths and machining instructions used by a CNC machine.
Typically these operations will be based on one or more drawing objects. 

CamBam provides the following machining operation types:

### ![](../images/basics/cam_profile.png) [Profile](profile.md){:.name}

This is a versatile 2D machining operation, typically used to cut around the inside or outside of a shape.<br/>
Profiles support [holding tabs](holding-tabs.md){:.prop} (sometime called bridges), which will hold parts in place once the full depth of the stock is cut through.<br/>
[Lead in](lead-moves.md){:.prop} and [Lead out](lead-moves.md){:.prop} moves can be added to reduce the stresses on parts and tooling and 
the [Side Profile](side-profile.md){:.prop} property can be used to give 3D contours to the profile cut.

### ![](../images/basics/cam_pocket.png) [Pocket](pocket.md){:.name}

Pockets are used to clear out stock within selected shape outlines.  Pockets will detect selected <i>islands</i>, or closed shapes within other shapes
to form more complex shapes.  This can be used to create raised lettering effects such as on a name plate.

### ![](../images/basics/cam_drill.png) [Drill](drill.md){:.name}

The drill operation is typically used to drill holes at selected point lists or circle centers using drill tooling.
End mills can also be used to spiral mill holes larger than the tool diameter and complicated operations can be 
achieved using custom drilling scripts.

### ![](../images/basics/cam_engrave.png) [Engrave](engrave.md){:.name}

Engraving operations are used to machine over selected lines.  As well as 2D geometry in the XY plane, they can also be used to follow 
3D lines with varying Z heights such as in bitmap heightmaps.

### ![](../images/basics/cam_3dsurface.png) [3D Profile](3d.md){:.name}

This operation is used to machine 3D shapes from surface mesh objects such as those imported from STL and 3DS files.<br/>
A number of different 3D methods are supported including waterline and scan-line methods with roughing and finishing options.
Front and back face operations are provided as well as creating inverted 3D machining operations for molds.

### ![](../images/basics/cam_turn.png) [Lathe](lathe.md){:.name}

The turning operation is a new, experimental featured introduced with CamBam version 0.9.8.  This can create roughing and finishing operations
based on 2D profile lines drawn in the XY plane, but machined in the conventional lathe XZ plane.

### ![](../images/basics/cam_ncfile.png) [NC File](back-plotting.md){:.name}

The NC File operation is different to the other operations in that it is not based upon drawing objects, but can be used
to include gcode from an external text file.  This operation can also be used to display a toolpath or back plot gcode files.
The contents of external gcode files will be included in the gcode output of the current CamBam drawing.

## Inserting a machining operation

To add a machining operation, select one or more drawing objects (2D or 3D depending on the type of operation to be inserted), then click on 
the toolbar icon that corresponds to the desired operation, or choose from the <span class="cmd">Machining</span> menu.

Machining operations can also be created by copying and pasting existing ones.
Copies can be made from machining operations in the current file or from another file loaded in a second running instance of CamBam.

The new operation will appear in the drawing tree, within the currently active [Part](part.md){:.name}, and its properties
will be available to modify in the property window below the tree view.

When machining operations are selected in the drawing tree, all visible drawing objects associated with the operation will be highlighted in the drawing view.

The list of unique IDs identifying the drawing objects associated with the operation can be found in the <property>Primitive IDs</property> property.

![](../images/primitive-ids.png){:.icenter}

> **Note:** The <property>Primitive IDs</property> property is only displayed in the <span class="name">Advanced</span> property view.
{:.note}

## Changing machining operation source objects.

It may be necessary to change the drawing objects associated with a machining operation if:

- Additional objects need to be added to the operation.
- A drawing object has been modified and its ID no longer matches that currently linked to the machining operation (for example,
 after converting a rectangle to a polyline for editing, its ID number will be changed).
- A machining operation has been created by copying an existing operation and new drawing objects need to be assigned.

To change the assignment of source objects of a machining operation:

![](../images/select-drawing-objects.png){:align="right" .iright}

Click the right mouse button on the operation concerned to display the context menu for that operation, then use the <span class="cmd">Select Drawing Objects</span> command.

The drawing window displays the objects already assigned to the operation in red. 
All the [object selection methods](../basics-selecting-objects.md){:.name} can be used to alter the current selection.
Holding the <span class="key">Ctrl</span> key and left clicking objects will add and remove objects  from the selection.

Clicking an empty area of the drawing will deselect all.

When finished, click the <i>middle mouse button</i> or press the <span class="key">Enter</span> key to apply the selection.

Press the <span class="key">Escape</span> key to abort the selection and revert to the original.

Pressing the `[...]` button to the right of the <property>Primitive IDs</property> property will also invoke object selection function.

![](../images/select-drawing-objects2.png)

The <property>Primitive IDs</property> property can also be edited directly in the property grid, entering the ID values separated by commas.

## Managing machining operations

Right clicking a machining operation invokes a context menu with the following options.

<span class="cmd">Enable / Disable MOP</span>: Activates or deactivates a machining operation. 
When disabled, the operation will appear greyed out, its toolpaths will be hidden and it will not be taken into account when creating the Gcode.

<span class="cmd">Set start point</span>: Sets the starting point of a machining operation by clicking on the drawing at the desired start point. 
The operation will start at the closest point possible to the select start point.

This starting point will be indicated by a red circle which can then be moved by dragging with the mouse. 
The coordinates of the chosen starting point will also be displayed, and can be edited directly, in the <property>Start Point</property> property of the machining operation.

![](../images/startpoint.png){:.icenter} 

Selecting the ![](../images/CONTENT.jpg) button to the right of the <property>Start Point</property> property will also invoke the interactive point selection function.

<span class="cmd">Cut</span> / <span class="cmd">Copy</span> / <span class="cmd">Paste</span>: Uses standard clipboard routines to manage the machining operations. 
These functions allow copies of the selected machining operations to be made in the current drawing, or in a different drawing loaded in another
running instance of CamBam.

![](../images/depl_souris.png){:align="right" .iright}

Machining operations may be reordered or moved between Parts by dragging them within the drawing tree. 
A horizontal bar indicates where the operation will be inserted.

<span class="cmd">Paste format</span>: This function copies most of the properties of a machining operation that has been copied
to the clipboard using the <span class="cmd">Copy</span> command, into the selected target machining operation.  The target's name
and source drawing objects are left intact.

Paste format can also be used to copy the contents of a machining operation into a [CAM Style](cam-style.md){:.name} object.

<span class="cmd">Delete</span>: Removes the selected machining operation.

<span class="cmd">Rename</span>: Renames the selected machining operation.

![](../images/default_style(en).png){:align="right" .iright} 

<span class="cmd">Reset to defaults</span>: All the properties of the machining operation will be set to <i>Default</i> so that they will inherit their values from the parent [CAM Style](cam-style.md){:.name}.

If no style is specified for the machining operation, the Style set in the containing Part object will be used. 
If the Part does not have a defined Style, the Style set against the Machining object will be used. 
In the event that no style is defined at any of these levels, the default style will be used for the source of the <i>Default</i> values.

> **Note:** The default style is the style with an empty name in the style library.
{:.note style="width:45%;"}

> **Warning:** The default styles are very important for CamBam to function correctly and should not
be renamed or removed.
{:.warning style="width:45%;"}

Refer to the [section on styles](cam-style.md){:.name} for more information.
 
<span class="cmd">Generate toolpaths</span>: Calculate and display the tool paths for the selected machining operation only.

<span class="cmd">Produce Gcode</span>: Creates the Gcode for this operation only, the suggested file name will be composed as follows.

~~~
Drawing name.part name.[machining operation].nc
~~~

See [creating gcode section](creating-gcode.md){:.name} for more information.

<span class="cmd">Toolpaths to geometry</span>: This feature allows you to create drawing objects from machining operation tool paths. 
These polylines can then be edited, used to create other toolpaths or exported as DXF.
