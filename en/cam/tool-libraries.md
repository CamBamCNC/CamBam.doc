---
title: Tool Libraries
---
# Tool Libraries

Libraries of tools can be maintained in the system tab's <span class="name">Tools</span> folder.

Multiple libraries can be defined.  This may be useful to group tools for specific purposes, materials or drawing units.
It may also be convenient to create a master library of all tools, then smaller libraries or 'palettes', customised to 
specific jobs, into which tools from the master library can be copied.

Tool libraries can be specified in CamBam drawings in the <span class="name">Machining</span> options or <span class="name">Part</span> objects.  Libraries
specified at the <span class="name">Part</span> level will take precedence over any set at the <span class="name">Machining</span> level.

Each machining operation can specify a <property>Tool Number</property>.  This number is used to look up information about that particular tool
in the relevant tool library.

![](../images/toollib1.png){:align="right" .iright}

If no tool library is specified in the drawing, the default libraries will be searched for entries of this tool number.
The default libraries are labeled 'Default-in' and 'Default-mm', where the units of the current drawing will be used 
to choose the correct library according to the '-in' or '-mm' suffix.

Tool numbers can also be set at the <span class="name">Machining</span> and <span class="name">Part</span> levels.
If a tool number is set at the machining level, this will be the default tool, used by all parts and machining operations,
unless explicitly set in the part or machining operation.  The tool selected for the part will override any default
machining tools and will be used for all operations within the part, unless they contain non zero tool numbers.

The tool definitions in the tool library contain information such as tool diameters and profiles, which can be used
in the referring machining operation.  If the tool diameter or profile is set explicitly in the machining operation
then this will take precedence over the information from the tool library.

It is possible to use tool numbers without having any matching entries in the tool libraries.  In these cases
the tool diameter and profile must be defined in the machining operation.

## Managing tools

![](../images/toollib4.png){:align="right" .iright}

Like the other system libraries, new tools and tool libraries can be create from the context menus presented
when right clicking the system tab's <span class="name">Tools</span> folder and tool library sub folders.

## Tool Properties

Tool libraries and definitions are a relatively new addition to CamBam.  Some of the properties available in the tool definitions
are intended for future functionality, but for the current release can be considered for informational use only.

<table class="prop" cellspacing="0" width="100%">
<tr>
<td>
	<property>Axial Depth Of Cut</property><br>
	<i>Informational</i>
</td>
<td>
	The maximum (Z) depth of cut for this tool.
</td>
</tr>

<tr>
<td>
	<property>Coating</property><br>
	<i>Informational</i>
</td>
<td>
</td>
</tr>
<tr>
<td>
	<property>Comment</property><br>
	<span class="new">[New! 0.9.8N]</span>
</td>
<td>
	This is a text value that can be included by the post processor when using the {$tool.comment} macro from with the ToolChange post processor section.
</td>
</tr>
<tr>
<td>
	<property>Diameter</property>
</td>
<td>
	The diameter of the cutting part of the tool.  This will be used to calculate toolpath offsets.  For V cutters, this should be set to the diameter
	of the cut at typical depths of cut.
</td>
</tr>
<tr>
<td>
	<property>Flute Length</property><br>
	<i>Informational</i>
</td>
<td>
	The length of the cutting part of the tool.
</td>
</tr>
<tr>
<td>
	<property>Flutes</property>
	<br><i>Informational</i>
</td>
<td>
	The number of cutting teeth or flutes.
</td>
</tr>
<tr>
<td>
	<property>Helix Angle</property>
	<br><i>Informational</i>
</td>
<td>
	The helix angle for spiral type cutters.
</td>
</tr>
<tr>
<td>
	<property>Index</property>
</td>
<td>
	<p>The tool number that uniquely identifies the tool within the library.  The tool index will be used when looking up tool numbers
	referenced with the CamBam drawings.</p>
	<p>The tool index will also be used within gcode when signaling tool changes etc.  This should be set to match 
	any corresponding tool tables used by the controller which may contain tool height offsets etc.</p>
</td>
</tr>
<tr>
<td>
	<property>Length</property><br>
	<i>Informational</i>
</td>
<td>
	The total length of the tool that typically extends from the collet.
</td>
</tr>
<tr>
<td>
	<property>Material</property><br>
	<i>Informational</i>
</td>
<td>
	Material from which the cutter is made.
</td>
</tr>
<tr>
<td>
	<property>Max Ramp Angle</property>
	<br><i>Informational</i>
</td>
<td>
	The maximum ramp angle.  To be used for lead move calculations in future releases.
</td>
</tr>
<tr>
<td>
	<property>Name</property>
</td>
<td>
	<p>The descriptive name of the tool which will be used in drop down tool selection lists within the drawing.</p>
	<p>The name can be automatically calculated from tool diameter, profile and other parameters
	by using the <property>Tool Name Format</property> property of the parent tool library.</p>
</td>
</tr>
<tr>
<td>
	<property>Notes</property><br>
	<i>Informational</i>
</td>
<td>
	Free format text notes relating to the tool.
</td>
</tr>
<tr>
<td>
	<property>Part Code</property><br>
	<i>Informational</i>
</td>
<td>
	A general identifier that may be useful to relate the tool to an external library or tooling catalogue.
</td>
</tr>
<tr>
<td>
	<property>Radial Depth Of Cut</property><br>
	<i>Informational</i>
</td>
<td>
	The maximum 'stepover' to be used by this cutter for crossover cuts.
</td>
</tr>
<tr>
<td>
	<property>Shank Diameter</property><br>
	<i>Informational</i>
</td>
<td>
	The diameter of the non cutting shank of the tool.
</td>
</tr>
<tr>
<td>
	<property>Tool Change</property>
</td>
<td>
	<p>The tool change property may contain text that will be used in the post processor when a tool change condition occurs.</p>
	<p>The code in this property will be output to the gcode file and will be used in preference to the default tool change
	definitions specified in the post processor (for this tool only).</p>
</td>
</tr>
<tr>
<td>
	<property>Tool Profile</property>
</td>
<td>
	The shape of the tool profile:<br>
	<value>End Mill</value><br>
	<value>Bull Nose</value><br>
	<value>Ball Nose</value><br>
	<value>V-Cutter</value><br>
	<value>Drill</value><br>
	<value>Lathe</value><br>
</td>
</tr>
<tr>
<td>
	<property>Tooth Load</property>
	<br><i>Informational</i>
</td>
<td>
	Feed per tooth.  Intended for use in automatic speed and feed calculations in future releases.
</td>
</tr>
<tr>
<td>
	<property>Vee Angle</property>
	<br><i>Informational</i>
</td>
<td>
	The angle of the V cutter.
</td>
</tr>
</table>

## Tool numbering and naming

Tools can be re-numbered by simply changing their index number in the property grid. If the number entered already exists, the numbers of the following tools will be staggered.


![](../images/tool_stringformat.png){:align="right" .iright}

It is also possible to automatically rename using an expression entered into the tool libraries <property>Tool Name Format</property> property.  
The expression can contain the following macros:

**{$diameter}** = Tool diameter  
**{$flutes}** = Number of teeth / flutes  
**{$index}** = Tool number  
**{$length}** = Tool length  
**{$partcode}** = Part code  
**{$profile}** = Tool shape / profile  
**{$veeangle}** = Tool Vee angle

The tool name will be recalculated whenever the tool properties have changed.  It is also possible to rename all the tools if the format expression has changed
by using the <span class="cmd">Rename all tools</span> from the tool library context menu.

The following image shows the tools renamed using the following format expression.

~~~
{$diameter} mm - {$profile} - z={$flutes}
~~~

![](../images/tool_name.png){:.icenter} 

## Managing Libraries 

Like for tools, libraries can be copied, pasted, deleted or moved with all their content.

These operations works the same as for tools, but using the context menu of a library folder.

**Create a new library:** Two possibilities ; either through the tool libraries main menu with <span class="cmd">New Library</span> or by copy / paste or cut / paste from an existing library.

![](../images/toollib2.png){:align="right" .iright}

As long as you have not saved the libraries, you can restore the values saved on the hard disk by the <span class="cmd">Refresh</span> command.

A <span class="cmd">Paste</span> command is also available in this menu. It is identical to the <span class="cmd">Paste</span> command in a library folder.

**Copy and paste a library:** You can cut, copy and paste a library from the context menu of this library with the <span class="cmd">Cut/Copy/Paste</span> commands.

**Delete Library:** You can delete a library with the <span class="cmd">Delete</span> command of the Library context menu, or by pressing the <span class="key">Del</span> key on the keyboard when this library is selected.

![](../images/toollib3.png){:align="right" .iright}

<span class="cmd">Reload</span>: Reloads the version of the library that is on the hard disk.

<span class="cmd">Save to XML</span>: Saves the library to the hard disk. Libraries will also be saved by using the CamBam <span class="cmd">Tools/Save settings</span> menu command.
This backup will be automatic if the menu option <span class="cmd">Tools/Save settings on exit</span> is checked

All tool libraries are saved as an .xml file (text files) in the tools folder of the CamBam system folder. You can quickly access this folder through the menu command <span class="cmd">Tools/Browse System Folder</span>.

